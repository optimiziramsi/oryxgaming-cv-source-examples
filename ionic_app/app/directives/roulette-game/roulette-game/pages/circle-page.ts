import "gsap";

// *******************  entities  *******************
import { RouletteGame_Page as Page } from '../entities/page';
import { RouletteGame_Layer as Layer } from '../entities/layer';
import { RouletteGame_Config as Config } from '../entities/config';

import {
	RouletteGame_RenderObject as RenderObject,
	RouletteGame_ImageRenderObject as ImageRO,
	RouletteGame_ActiveImageRenderObject as ActiveImageRO,
	RouletteGame_TextRenderObject as TextRO,
	RouletteGame_TileRenderObject as TileRO,
	RouletteGame_ButtonRenderObject as ButtonRO,
	RouletteGame_GridChipRenderObject as GridChipRO,
	} from '../entities/render-object';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from '../interfaces/i-ui-manager';

import { RouletteGame_iPage as iPage } from '../interfaces/i-page';
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iActiveImageRenderObject as iActiveImageRO } from '../interfaces/i-render-object';
import { RouletteGame_iGridKeys as iGridKeys } from '../interfaces/i-grid-keys';
import { RouletteGame_iGridKey as iGridKey } from '../interfaces/i-grid-key';
import { RouletteGame_iGridChipRenderObject as iGridChipRO } from '../interfaces/i-render-object';
import { RouletteGame_iCircleRotations as iCircleRotations } from '../interfaces/i-circle-rotations';

/*******************  enums  *******************/
import { RouletteGame_ChipState as ChipState } from '../enums/chip-state';

export class RouletteGame_CirclePage extends Page {

	private grid_l:iLayer;
	private grid_chips_l:iLayer;
	private circle_l:iLayer;
	private ball_l:iLayer;

	private _grid_keys:iGridKeys;
	private _circle_numbers_rotations:iCircleRotations;
	private _ball_win_number_rotation:number = 0;
	private _ball_win_number_synced:boolean = false;
	private _syncing_ball_with_circle:boolean = false;
	private _syncing_ball_rotation_distance:number = 360;

	// *******************  render objects  *******************
	private _grid:iRenderObject;
	private _grid_chips_render_objects:{ [grid_kex_index:number]:iGridChipRO } = {};
	private _circle_inner:iRenderObject;
	private _circle_outter:iRenderObject;
	private _ball:iRenderObject;

	constructor(){
		super('circle');

		// *******************  create needed layers  *******************
		this.grid_l = new Layer( 'c_grid' );
		this.grid_chips_l = new Layer( 'c_grid_chips' );
		this.circle_l = new Layer( 'c_circle' );
		this.ball_l = new Layer( 'c_circle_ball' );

		// *******************  add them to this page  *******************
		this.addLayer( this.grid_l );
		this.addLayer( this.grid_chips_l );
		this.addLayer( this.circle_l );
		this.addLayer( this.ball_l );
	}

	public init(){

		this._grid_keys = this.am.get('betting_field_portrait_keys');
		this._circle_numbers_rotations = this.am.get("numbers_rotations");

		this.createRenderObjects();

		this.addGameManagerListeners();
	}



	// ******************************************************************
	// *
	// *       PRIVATE METHODS
	// *
	// ******************************************************************
	
	private createRenderObjects(){

		// *******************  grid  *******************
		this._grid = new ImageRO( this.am.get( 'betting_field_portrait' ) );
		this._grid.scale = ( Config.stage_height - 200 ) / this._grid.height;
		this._grid.x = Config.stage_width - this._grid.width - 100;
		this._grid.y = 100;

		this.grid_l.addRenderObject( this._grid );

		// circle / boben
		let circleCenterX:number = 540;
		let circleCenterY:number = 540;

		this._circle_outter = new ImageRO( this.am.get('wheel_outter') );
		this._circle_outter.centerX = circleCenterX;
		this._circle_outter.centerY = circleCenterY;
		this.circle_l.addRenderObject( this._circle_outter );

		this._circle_inner = new ImageRO( this.am.get('wheel_inner') );
		this._circle_inner.centerX = circleCenterX;
		this._circle_inner.centerY = circleCenterY;
		this._circle_inner.rotationOffsetX = this._circle_inner.width / 2;
		this._circle_inner.rotationOffsetY = this._circle_inner.height / 2;
		this.circle_l.addRenderObject( this._circle_inner );

		// ball
		this._ball = new ImageRO( this.am.get("ball") );
		this._ball.centerX = circleCenterX;
		this._ball.centerY = circleCenterY - this._circle_inner.height / 2;
		this._ball.rotationOffsetX = this._ball.width / 2;
		this._ball.rotationOffsetY = this._circle_inner.height / 2;
		this.circle_l.addRenderObject( this._ball );
		this._ball.visible = false;

	}

	private addGameManagerListeners(){
		this.gm.onBetConfirm.add( this.onBetConfirm );
		this.gm.onBetRemoveConfirmed.add( this.onBetConfirm );
		this.gm.onBallShot.add( this.onBallShot );
		this.gm.onWinNumber.add( this.onWinNumber );
		this.gm.onClearBets.add( this.onClearBets );
	}

	private onBetConfirm = ( grid_chip:iGridChip ) => {
		let ro:iActiveImageRO = this._grid_chips_render_objects[ grid_chip.grid_key_index ];
		if( ro ){
			this.grid_chips_l.removeRenderObject( ro );
			delete this._grid_chips_render_objects[ grid_chip.grid_key_index ];
		}

		this.placeGridChips();
	};

	private placeGridChips(){
		for (var i = this.gm.grid_chips.length - 1; i >= 0; i--) {
			let grid_chip:iGridChip = this.gm.grid_chips[i];

			// ne prikazujemo chip-ov, ki so označeni za brisanje ali nimajo potrjenega statusa / potrjene stave
			if( grid_chip.delete || ChipState.CONFIRMED != grid_chip.state ){
				continue;
			}

			// preskočimo, če že prikazujemo to stavo
			if( this._grid_chips_render_objects[ grid_chip.grid_key_index ] ){
				continue;
			}

			// prikažemo na grid-u
			let ro:iGridChipRO = new GridChipRO( this.am.get('grid_chip_off_'+grid_chip.betting_chip_index), this.am.get('grid_chip_on_'+grid_chip.betting_chip_index) );
				ro.value = grid_chip.value;
				ro.scale = this._grid.scale;

			let grid_key:iGridKey = this._grid_keys[ grid_chip.grid_key_index ];
			if( grid_key ){
				ro.visible = true;
				ro.centerX = this._grid.x + grid_key.centerX * this._grid.scale;
				ro.centerY = this._grid.y + grid_key.centerY * this._grid.scale;
			} else {
				ro.visible = false;
			}
			ro.active = ( grid_chip.state == ChipState.CONFIRMED );

			this._grid_chips_render_objects[ grid_chip.grid_key_index ] = ro;

			this.grid_chips_l.addRenderObject( ro );
		}
	}

	private onBallShot = () => {
		let spinning_duration:number = Config.ball_shot_animation_duration + 10;
		let spins_per_second = Config.wheel_rotations_per_second;
		let num_spins = Math.round( spinning_duration * spins_per_second );

		this._circle_inner.rotation = 0;
		TweenLite.to( this._circle_inner, spinning_duration, { rotation: 360 * num_spins, onComplete: () => { this._syncing_ball_with_circle = false; } } );


		this._ball.rotation = 0;
		this._ball.y = this._circle_inner.centerY - Config.ball_rotation_outer_offset;
		this._ball.rotationOffsetY = Config.ball_rotation_outer_offset;
		this._ball.visible = true;

		TweenLite.to( this._ball, Config.ball_shot_animation_duration + 5, {
			rotation: - 360 * num_spins * 2,
			y: this._circle_inner.centerY - Config.ball_rotation_inner_offset,
			rotationOffsetY: Config.ball_rotation_inner_offset, 
		} );

	};

	private onWinNumber = ( winning_number:number ) => {

		this.setBallWinNumberRotation( winning_number );
		this._syncing_ball_with_circle = true;
		this.syncBallWithCircle();
	};

	private animateWinningNumber() {

		let winning_number = this.gm.getWinNumber();

		let grid_chips:iGridChip[] = this.gm.grid_chips.filter( ( grid_chip:iGridChip ) => { return ! grid_chip.delete && grid_chip.state == ChipState.CONFIRMED; } );

		let grid_keys_checked:number[] = [];

		for (var i = grid_chips.length - 1; i >= 0; i--) {
			let grid_chip:iGridChip = grid_chips[i];
			let grid_key_index = grid_chip.grid_key_index;

			if( -1 != grid_keys_checked.indexOf( grid_chip.grid_key_index ) ){
				// smo že obdelali, naslednji
				continue;
			}

			// če obstaja render object, ga animirano
			let ro:iActiveImageRO = this._grid_chips_render_objects[ grid_chip.grid_key_index ];
			if( ro ){
				// se gre za zmagovalno stavo
				if( winning_number in grid_chip.payouts && grid_chip.payouts[ winning_number ] > 0 ){
					TweenLite.to( ro, 1, { y: -Config.stage_height * 0.2, delay: 2 } );
				}
				// se gre za izgubljeno stavo
				else {
					TweenLite.to( ro, 1, { y: Config.stage_height * 1.2 } );
				}
			}
		}

		setTimeout( () => {

			this.gm.onClearBets.trigger();

			this.um.switchPage( this.lm.getPage('bet') );

		}, 4000 );
	}

	private syncBallWithCircle = () => {

		// *******************  če moramo žogico še posinkati z rotacijo bobna  *******************
		if( ! this._ball_win_number_synced ){
			let ball_rotation = this._ball.rotation % 360;
			let circle_number_rotation = ( this._circle_inner.rotation + this._ball_win_number_rotation ) % 360;
			let rotation_distance =  360 - Math.abs( circle_number_rotation - ball_rotation ) % 360;

			if( rotation_distance < this._syncing_ball_rotation_distance ){
				// se še usklajujemo z želenim mestom
				this._syncing_ball_rotation_distance = rotation_distance;
			} else {
				// smo prišli na svoje mesto
				TweenLite.killTweensOf( this._ball );
				this._ball_win_number_synced = true;
				setTimeout( () => this.animateWinningNumber(), 2000 );
			}
		}

		// *******************  žogica je že posinkana, animiramo jo z bobnom  *******************
		if( this._ball_win_number_synced ){
			this._ball.rotation = this._circle_inner.rotation + this._ball_win_number_rotation;
		}

		if( this._syncing_ball_with_circle ){
			window.requestAnimationFrame( this.syncBallWithCircle );	
		}
	};

	private setBallWinNumberRotation( win_number:number ){
		this._ball_win_number_rotation = this._circle_numbers_rotations[ win_number ];
		this._syncing_ball_rotation_distance = 360;
		this._ball_win_number_synced = false;
	}

	private onClearBets = () => {
		this.grid_chips_l.removeAllRenderObjects();
		this._grid_chips_render_objects = {};
	};

}