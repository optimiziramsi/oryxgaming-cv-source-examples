// *******************  interfaces  *******************
import { RouletteGame_iLayer as iLayer } from './i-layer';

export interface RouletteGame_iLayers {
	[layer_id:string] : iLayer	// seznam / object like array, kjer je ključ id layerja
}