// *******************  interfaces  *******************
import { RouletteGame_iGame as iGame } from '../interfaces/i-game';
import { RouletteGame_iAsset as iAsset } from './i-asset';
import { RouletteGame_iSignal as iSignal } from './i-signal';
import { RouletteGame_iGridChips as iGridChips } from './i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from './i-grid-chip';
import { RouletteGame_iBettingChip as iBettingChip } from './i-betting-chip';

// *******************  enums  *******************
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';

/**
 * manager, ki skrbi za sinhronizacijo z igro
 * igra je lahko lokalna ali remote. Lahko bi rekli, da je to skupen interface
 */
export interface RouletteGame_iGameManager {
	
	// ******************************************************************
	// *
	// *       SIGNALS / EVENTS
	// *
	// ******************************************************************
	
	/**
	 * ko igra spremeni svoj status / odprta za stave / zaprta za stave
	 * @type {iSignal<GameState>}
	 */
	onGameState:iSignal<GameState>;

	/**
	 * ko je igra uzžrebala številko
	 * @type {iSignal<number>}
	 */
	onWinNumber:iSignal<number>;

	/**
	 * ko smo sprožili žogico v boben
	 * @type {iSignal<any>}
	 */
	onBallShot:iSignal<any>;

	/**
	 * ko se spremeni stanje / balance uporabnika
	 * @type {iSignal<number>}
	 */
	onUserCredits:iSignal<number>;

	/**
	 * ko izplačamo dobitek
	 * @type {iSignal<number>}
	 */
	onPayout:iSignal<number>;

	/**
	 * ko se spremeni vrednost stav na mizi
	 * @type {iSignal<number>}
	 */
	onStake:iSignal<number>;

	/**
	 * ko izgubimo ali dobimo povezavo z igro / to je predvsem za igre, ki niso lokalne. Parameter nam vrača stanje povezave
	 * @type {iSignal<GameConnectionStatus>}
	 */
	onConnection:iSignal<GameConnectionStatus>;

	/**
	 * če smo postavili bet, nam ga bo GAME potrdil ali zavrnil. To je signal / event, ki nam pove, da je nek bet potrjen
	 * @type {iSignal<iGameBet>}
	 */
	onBetConfirm:iSignal<iGridChip>;

	/**
	 * če smo postavili bet, nam ga bo GAME potrdil ali zavrnil. To je signal / event, ki nam pove, da je nek bet potrjen
	 * @type {iSignal<iGameBet>}
	 */
	onBetReject:iSignal<iGridChip>;

	/**
	 * če smo zahtevali bet remove, nam bo game potrdil ali zavrnil našo zahtevo
	 * @type {iSignal<iGridChip>}
	 */
	onBetRemoveConfirmed:iSignal<iGridChip>;

	/**
	 * če smo zahtevali bet remove, nam bo game potrdil ali zavrnil našo zahtevo
	 * @type {iSignal<iGridChip>}
	 */
	onBetRemoveRejected:iSignal<iGridChip>;

	/**
	 * ko že izžrebamo kuglo in izplačamo dobitek in vse, naredimo reset stav in takrat se sproži ta signal.
	 * @type {iSignal<any>}
	 */
	onClearBets:iSignal<any>;

	/**
	 * Če se spremeni, ali lahko igro / met kroglice, sprožimo sami
	 * @type {iSignal<boolean>}
	 */
	onManualBallShotEnabled:iSignal<boolean>;

	/**
	 * omogočimo dostop do grid-chips, ki so trenutno na mizi / stave
	 * @type {iGridChip[]}
	 */
	grid_chips:iGridChip[];

	
	// ******************************************************************
	// *
	// *       GAMEPLAY METHODS
	// *
	// ******************************************************************
	
	/**
	 * Če stavo sploh lahko podamo
	 * @param  {number}  grid_key_index id / modra barva iz colors slike za gord
	 * @param  {number}  value          vrednost stave
	 * @return {boolean}                če stavo lahko postavimo ali ne
	 */
	canMakeBet( grid_chip:iGridChip ) : boolean;

	/**
	 * če stavo lahko odstranimo
	 * @param  {iGridChip} grid_chip chip / stava, ki jo želimo odstraniti
	 * @return {boolean}             ali nam je dovoljeno odstraniti stavo
	 */
	canRemoveBet( grid_chip:iGridChip ) : boolean;

	/**
	 * oddamo zahtevo za stavo na polje
	 * @param {iGridChip} chip
	 */
	requestAddBet( chip:iGridChip );

	/**
	 * oddamo zahtevo, da odstranimo stavo
	 * @param {iGridChip} chip
	 */
	requestRemoveBet( chip:iGridChip );

	/**
	 * sprožimo met kugle / sprožimo začetek rulete. To pride v upoštev, če zunanji "game" ni določen
	 */
	makeBallShot() : void;



	/******************************************************************
	*
	*       STATES / DATA METHODS
	*
	******************************************************************/
	
	/**
	 * nastavimo igro, s katero bomo povezani / nam bo javljala stanja itd
	 * @param {iGame} game [description]
	 */
	setGame( game:iGame ) : void;

	/**
	 * ali smo povezani z igro ali ne
	 * @type {GameConnectionStatus}
	 */
	getConnectionStatus() : GameConnectionStatus;

	/**
	 * trenutno stanje igre / je odprta ali zaprta za stave
	 * @type {GameState}
	 */
	getGameState() : GameState;

	/**
	 * izžrebana številka
	 * @type {number}
	 */
	getWinNumber() : number;

	/**
	 * vrednost izplačila nagrade
	 * @type {number}
	 */
	getPayout() : number;

	/**
	 * stanje denarja trenutnega uporabnika
	 * @return {number} cifra 1,00€ se vrača kot 100, torej je potrebno zamakniti descimalno vejico za 2 mesti
	 */
	getUserCredits() : number;

	/**
	 * stanje / vrednost stav na mizi
	 * @return {number} vrednost stav na mizi za 1,00€ se vrača kot 100, torej je potrebno zamakniti decimalno vejico za 2 mesti
	 */
	getStake() : number;

	/**
	 * vrača maximalni možen dobiček, glede na stave, ki so na mizi
	 * @return {number}
	 */
	getMaxPossiblePayout() : number;

	/**
	 * vrne zgodovino stav za zadnjo igro
	 * @return {iGameBets}
	 */
	getPreviousBets() : iGridChip[];

	/**
	 * Informacija, če igra omogoča lastno sprožanje "ball shot"
	 * @return {boolean}
	 */
	getManualBallShotEnabled() : boolean;

	/**
	 * počisti vse parametre za seboj
	 */
	destroy();
}