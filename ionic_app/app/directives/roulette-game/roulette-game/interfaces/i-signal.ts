export interface RouletteGame_iSignal<T0> {
    add(handler: { (data0?: T0): void }) : void;
    remove(handler: { (data0?: T0): void }) : void;
    trigger(data?: T0) : void;
    clear() : void;
}