/*******************  interfaces  *******************/
import { RouletteGame_iPayoutsByGridNumber as iPayoutsByGridNumber } from './i-payouts-by-grid-number';

export interface RouletteGame_iPayoutsInfo {
	/**
	 * kolikšna je stava na stavnem mestu, na katerega bomo dodali stavo
	 * @type {number}
	 */
	bet:number;

	/**
	 * kolikšen je maximalen možen dobitek na gird-u, glede na trenutne stave na grid-u + stava, ki jo morda želimo dodati
	 * @type {number}
	 */
	max_payout:number;

	/**
	 * seštevek vrednosti stav na mizi
	 * @type {number}
	 */
	stake:number;

	/**
	 * izplačila glede na dobitno številko
	 * @type {iPayoutsByGridNumber}
	 */
	payouts_by_grid_number:iPayoutsByGridNumber;
}