// *******************  interfaces  *******************
import { RouletteGame_iAsset as iAsset } from './i-asset';

export interface RouletteGame_iAssets {
	[asset_id:string] : iAsset;	// seznam / array like object, kjer je ključ id asseta
}