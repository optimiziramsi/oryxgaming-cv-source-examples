// *******************  interfaces  *******************
import { RouletteGame_iAsset as iAsset } from './i-asset';
import { RouletteGame_iSignal as iSignal } from './i-signal';
import { RouletteGame_iGridChips as iGridChips } from './i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from './i-grid-chip';

// *******************  enums  *******************
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';


export interface RouletteGame_iGame {

	// ******************************************************************
	// *
	// *       SIGNALS / EVENTS
	// *
	// ******************************************************************
	
	/**
	 * ko igra spremeni svoj status / odprta za stave / zaprta za stave
	 * @type {iSignal<GameState>}
	 */
	onGameStateChange:iSignal<GameState>;

	/**
	 * ko je igra uzžrebala številko
	 * @type {iSignal<number>}
	 */
	onNumberSelected:iSignal<number>;

	/**
	 * ko smo sprožili žogico v boben
	 * @type {iSignal<any>}
	 */
	onBallShot:iSignal<any>;

	/**
	 * ko se spremeni stanje / balance uporabnika
	 * @type {iSignal<number>}
	 */
	onUserBalanceChange:iSignal<number>;

	/**
	 * ko izplačamo dobitek
	 * @type {iSignal<number>}
	 */
	onWinPayout:iSignal<number>;

	/**
	 * ko se spremeni vrednost stav na mizi
	 * @type {iSignal<number>}
	 */
	onBalanceOnTableChange:iSignal<number>;

	/**
	 * ko izgubimo ali dobimo povezavo z igro / to je predvsem za igre, ki niso lokalne. Parameter nam vrača stanje povezave
	 * @type {iSignal<GameConnectionStatus>}
	 */
	onConnectionChange:iSignal<GameConnectionStatus>;

	/**
	 * če smo postavili bet, nam ga bo GAME potrdil ali zavrnil. To je signal / event, ki nam pove, da je nek bet potrjen
	 * @type {iSignal<iGameBet>}
	 */
	onBetConfirm:iSignal<iGridChip>;

	/**
	 * če smo postavili bet, nam ga bo GAME potrdil ali zavrnil. To je signal / event, ki nam pove, da je nek bet potrjen
	 * @type {iSignal<iGameBet>}
	 */
	onBetReject:iSignal<iGridChip>;

	
	// ******************************************************************
	// *
	// *       METHODS
	// *
	// ******************************************************************
	
	/**
	 * sproži inicializacijo igre / se poveže z igro
	 */
	init();

	/**
	 * se igra zares začne / se povežemo / se začne lokalni igra
	 */
	start();

	/**
	 * ali smo povezani z igro ali ne
	 * @type {GameConnectionStatus}
	 */
	getConnectionStatus() : GameConnectionStatus;

	/**
	 * trenutno stanje igre / je odprta ali zaprta za stave
	 * @type {GameState}
	 */
	getGameState() : GameState;

	/**
	 * izžrebana številka
	 * @type {number}
	 */
	getSelectedNumber() : number;

	/**
	 * vrednost izplačila nagrade
	 * @type {number}
	 */
	getWinBalance() : number;

	/**
	 * stanje denarja trenutnega uporabnika
	 * @return {number} cifra 1,00€ se vrača kot 100, torej je potrebno zamakniti descimalno vejico za 2 mesti
	 */
	getUserBalance() : number;

	/**
	 * stanje / vrednost stav na mizi
	 * @return {number} vrednost stav na mizi za 1,00€ se vrača kot 100, torej je potrebno zamakniti decimalno vejico za 2 mesti
	 */
	getBalanceOnTable() : number;

	/**
	 * oddamo zahtevo za stavo na polje
	 * @param {iGridChip} chip
	 */
	addBet( chip:iGridChip );

	/**
	 * vrne zgodovino stav po zgodovini igranih iger / stav
	 * @return {iGameBets}
	 */
	getPreviousBets() : iGridChips[];

	/**
	 * počisti vse parametre za seboj
	 */
	destroy();
}