// *******************  interfaces  *******************
import { RouletteGame_iGridKey as iGridKey } from './i-grid-key';
import { RouletteGame_iRenderObject as iRenderObject } from './i-render-object';
import { RouletteGame_iPayoutsByGridNumber as iPayoutsByGridNumber } from './i-payouts-by-grid-number';

// *******************  enums  *******************
import { RouletteGame_ChipState as ChipState } from '../enums/chip-state';

export interface RouletteGame_iGridChip {

	/**
	 * kje na grid-u smo postavili ta žeton
	 * @type {number}
	 */
	grid_key_index:number,

	/**
	 * kater betting chip smo uporabili za stavo, preko česar lahko pripravimo primeren RenderObject za prikaz na grid-u
	 * @type {number}
	 */
	betting_chip_index:number,

	/**
	 * Vrednot žetona / stave
	 * @type {number}
	 */
	value:number,

	/**
	 * State je pomemben za zunanjo igro, kjer bo zunanja igra potrjevala stavo
	 * @type {ChipState}
	 */
	state:ChipState,

	/**
	 * ali imamo za to stavo zahtevan delete
	 * @type {boolean}
	 */
	delete:boolean;

	/**
	 * Imamo vklopljeno dvojno stavo / za izračun
	 * @type {boolean}
	 */
	double_bet:boolean;

	/**
	 * Katere so dobitne številke za to stavo
	 * @type {number[]}
	 */
	payouts:iPayoutsByGridNumber;
}