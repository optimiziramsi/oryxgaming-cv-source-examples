// *******************  interfaces  *******************
import { RouletteGame_iGridKey as iGridKey } from './i-grid-key';

export interface RouletteGame_iGridKeys {
	[key_id:number]:iGridKey
}