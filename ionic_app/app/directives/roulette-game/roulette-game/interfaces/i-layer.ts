// *******************  interfaces  *******************
import { RouletteGame_iRenderObject as iRenderObject } from './i-render-object';
import { RouletteGame_iPage as iPage } from './i-page';

export interface RouletteGame_iLayer {

	/**
	 * layer id, s katerim lažje dobimo želeni layer
	 * @type {string}
	 */
	id:string;

	/**
	 * html dom element / canvas
	 * @type {any}
	 */
	canvas:any;

	/**
	 * canvas 2d context
	 * @type {any}
	 */
	ctx:any;

	/**
	 * če ta layer prikazujemo / rišemo nanj
	 * @type {boolean}
	 */
	visible:boolean;

	/**
	 * če so bile spremembe na layerju, s čimer vemo, da je potreben nov render objektov
	 * @type {boolean}
	 */
	changed:boolean;

	/**
	 * kot imalo listenerje na RenderObject za klik, ga imamo tudi na layer.
	 * Uporablja se recimo ko imamo pojavno okno, in uporabnik klikne "nekam drugam", to ujamemo na layer-ju
	 * @type {[type]}
	 */
	onClick?:any;

	/**
	 * page, katerega del je ta layer
	 * @type {iPage}
	 */
	page:iPage;

	/**
	 * dobimo seznam vseh render objektov
	 * @type {iRenderObject[]}
	 */
	render_objects:iRenderObject[];

	/**
	 * Dodamo render object na trenutni layer
	 * @param {iRenderObject} render_object
	 */
	addRenderObject( render_object:iRenderObject ) : void;

	/**
	 * odstranimo render object / ga ne izrisujemo več
	 * @param {iRenderObject} render_object [description]
	 */
	removeRenderObject( render_object:iRenderObject ) : void;

	/**
	 * odstrani vse render objekte iz layer-ja
	 */
	removeAllRenderObjects() : void;
}