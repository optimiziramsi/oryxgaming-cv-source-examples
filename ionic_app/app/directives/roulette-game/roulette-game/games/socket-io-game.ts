import { Injectable } from '@angular/core';

// *******************  managers  *******************
import { RouletteGame_ConfigManager as ConfigManager } from '../managers/config-manager';

// *******************  enteties  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';

// *******************  interfaces  *******************
import { RouletteGame_iConfigManager as iConfigManager } from '../interfaces/i-config-manager';

import { RouletteGame_iGame as iGame } from '../interfaces/i-game';
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iGridChips as iGridChips } from '../interfaces/i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';

// *******************  enums  *******************
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';

@Injectable()
export class RouletteGame_SocketIoGame implements iGame {

	private _connection_status:GameConnectionStatus = GameConnectionStatus.LOST;
	private _game_state:GameState = GameState.CLOSED_FOR_BETS;
	private _selected_number:number = -1;
	private _win_payout:number = -1;
	private _user_balance:number = -1;

	public onGameStateChange:iSignal<GameState>;
	public onNumberSelected:iSignal<number>;
	public onBallShot:iSignal<any>;
	public onUserBalanceChange:iSignal<number>;
	public onWinPayout:iSignal<number>;
	public onBalanceOnTableChange:iSignal<number>;
	public onConnectionChange:iSignal<GameConnectionStatus>;
	public onBetConfirm:iSignal<iGridChip>;
	public onBetReject:iSignal<iGridChip>;

	constructor(){

	}

	// ******************************************************************
	// *
	// *       PUBLIC METHODS
	// *
	// ******************************************************************
	public init() {
		
	}

	public start(){
		
	}

	public getConnectionStatus() : GameConnectionStatus {
		return GameConnectionStatus.LOST;
	}

	public getGameState() : GameState {
		return GameState.CLOSED_FOR_BETS;
	}

	public getSelectedNumber() : number {
		return 1;
	}

	public getWinBalance() : number {
		return 1;
	}

	public getUserBalance() : number {
		return 1;
	}

	public getBalanceOnTable() : number {
		return 1;
	}

	public addBet( chip:iGridChip ) {}

	public getPreviousBets() : iGridChips[] {
		return [];
	}

	/**
	 * počisti vse parametre za seboj
	 */
	public destroy() {}

}