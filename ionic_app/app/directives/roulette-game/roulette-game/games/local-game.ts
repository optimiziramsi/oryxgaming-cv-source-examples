import { Injectable } from '@angular/core';

// *******************  managers  *******************
import { RouletteGame_ConfigManager as ConfigManager } from '../managers/config-manager';

// *******************  enteties  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';

// *******************  interfaces  *******************
import { RouletteGame_iConfigManager as iConfigManager } from '../interfaces/i-config-manager';

import { RouletteGame_iGame as iGame } from '../interfaces/i-game';
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iGridChips as iGridChips } from '../interfaces/i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';

// *******************  enums  *******************
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';

@Injectable()
export class RouletteGame_LocalGame implements iGame {

	private config:iConfigManager;

	private _connection_status:GameConnectionStatus = GameConnectionStatus.CONNECTED;
	private _game_state:GameState = GameState.CLOSED_FOR_BETS;
	private _winning_number:number = -1;
	private _win_payout:number = -1;
	private _user_balance:number = 10000000;
	private _bets:iGridChip[] = [];

	public onGameStateChange:Signal<GameState> = new Signal();
	public onNumberSelected:Signal<number> = new Signal();
	public onBallShot:Signal<any> = new Signal();
	public onUserBalanceChange:Signal<number> = new Signal();
	public onWinPayout:Signal<number> = new Signal();
	public onBalanceOnTableChange:Signal<number> = new Signal();
	public onConnectionChange:Signal<GameConnectionStatus> = new Signal();
	public onBetConfirm:Signal<iGridChip> = new Signal();
	public onBetReject:Signal<iGridChip> = new Signal();

	// *******************  local variables  *******************
	private timeouts:any[] = [];	// timeout instances
	private bets_open_duration:number;
	private bets_closed_duration:number;
	private ball_shot_interval:number;

	constructor( config:ConfigManager ){
		this.config = config;
	}

	// ******************************************************************
	// *
	// *       PUBLIC METHODS
	// *
	// ******************************************************************
	public init() {
		// this.bets_open_duration = this.config.local_game_betting_open_duration;
		// this.bets_closed_duration = this.config.local_game_betting_closed_duration;
		// this.ball_shot_interval = this.config.local_game_ball_shot_interval;

		this.bets_open_duration = 25;
		this.bets_closed_duration = 5;
		this.ball_shot_interval = 2;

		// console.log("GAME INIT", this.bets_open_duration, this.bets_closed_duration, this.ball_shot_interval);
	}

	public start(){
		this.openBets();
	}

	public addBet( chip:iGridChip ){
		this._user_balance -= chip.value;
		this.onUserBalanceChange.trigger( this._user_balance );

		this._bets.push( chip );
		this.onBalanceOnTableChange.trigger( this.getBalanceOnTable() );
	}

	public getConnectionStatus() : GameConnectionStatus {
		return GameConnectionStatus.LOST;
	}

	public getGameState() : GameState {
		return GameState.CLOSED_FOR_BETS;
	}

	public getSelectedNumber() : number {
		return 1;
	}

	public getWinBalance() : number {
		return 1;
	}

	public getUserBalance():number {
		return this._user_balance;
	}

	public getBalanceOnTable():number {
		let on_table_balance = 0;
		for (var i = 0; i < this._bets.length; ++i) {
			let bet:iGridChip = this._bets[ i ];
			on_table_balance += bet.value;
		}
		return on_table_balance;
	}

	public getPreviousBets() : iGridChips[] {
		return [];
	}

	public destroy() {
		this.clearTimeouts();
		// TODO - počisti še ostale stvari
	}

	// ******************************************************************
	// *
	// *       PRIVATE METHODS
	// *
	// ******************************************************************
	
	private openBets(){
		this.clearTimeouts();

		this._game_state = GameState.OPEN_FOR_BETS;
		this.onGameStateChange.trigger( this._game_state );
		this.setTimeout( () => this.closeBets(), this.bets_open_duration * 1000 );
		this.setTimeout( () => this.shootBall(), ( this.bets_open_duration - this.ball_shot_interval ) * 1000 );
	}

	private closeBets(){
		this.clearTimeouts();

		this._game_state = GameState.CLOSED_FOR_BETS;
		this.onGameStateChange.trigger( this._game_state );
		
		this.generateandSetWinningNumber();
		this.prepareWinPayout();
		this.onNumberSelected.trigger( this._winning_number );
		this.onWinPayout.trigger( this._win_payout );

		this.setTimeout( () => this.openBets(), this.bets_closed_duration * 1000 );
	}

	private shootBall(){
		this.onBallShot.trigger();
	}

	private generateandSetWinningNumber(){
		this._winning_number = this.getRandomInt(0, 39);
	}

	private prepareWinPayout(){
		this._win_payout = this.getRandomInt(0, this.getBalanceOnTable() * 10 );
	}

	private getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	private clearTimeouts(){
		for (var i = this.timeouts.length - 1; i >= 0; i--) {
			let timeout = this.timeouts[i];
			clearTimeout( timeout );
		}
		this.timeouts = [];
	}

	private setTimeout( handler:any, timeout:number ){
		let timeout_id = setTimeout( handler, timeout );
		this.timeouts.push( timeout_id );
	}

}