import { Injectable } from '@angular/core';

// *******************  entities  *******************
import { RouletteGame_RenderObject as RenderObject } from '../entities/render-object';
import { RouletteGame_Config as Config } from '../entities/config';
import { RouletteGame_Layer as Layer } from '../entities/layer';
import { RouletteGame_Utils as Utils } from '../entities/utils';

// *******************  interfaces  *******************
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iLayers as iLayers } from '../interfaces/i-layers';
import { RouletteGame_iPages as iPages } from '../interfaces/i-pages';
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iPage as iPage } from '../interfaces/i-page';
import { RouletteGame_iResizeParams as iResizeParams } from '../interfaces/i-resize-params';
import { RouletteGame_iMousePosition as iMousePosition } from '../interfaces/i-mouse-position';
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';

@Injectable()
export class RouletteGame_LayersManager implements iLayersManager {

	private _dom_holder_element:any;
	private _touch_layer:iLayer;
	private _layers:iLayers = {};
	private _pages:iPages = {};
	private _render_objects:iRenderObject[] = [];

	private _canvasWidth:number = 10;
	private _canvasHeight:number = 10;
	private _canvasRealWidth:number = 10;
	private _canvasRealHeight:number = 10;
	private _canvasStageScale:number = 1;

	private _resizeCanvasesTimeout:any;

	constructor(){

		// *******************  create touch listener layer  *******************
		
		let touch_layer = new Layer('touch');
		let touch_canvas = touch_layer.canvas;
		touch_canvas.addEventListener('click', this.onCanvasClick );
		this.addLayer( touch_layer );
	}

	public setHolderElement( dom_element:any ){
    	this._dom_holder_element = dom_element;

    	for( let layer_id in this._layers ){
    		this.addLayerCanvasToHolderDom( this._layers[ layer_id ]);
    	}
    }

    public addLayer( layer:iLayer ){
    	if( this._layers.hasOwnProperty( layer.id ) ){
			throw new Error('Error: Can not add layer. Layer with id "'+layer.id+'" already exist.');
		}
    	this._layers[ layer.id ] = layer;

    	this.addLayerCanvasToHolderDom( layer );

    	// ponovno dodamo touch layer, zato da je na koncu / čisto na vrhu
    	if( 'touch' != layer.id ){
    		let touch_layer:iLayer = this.getLayer('touch');
    		this.addLayerCanvasToHolderDom( touch_layer );
    	}

    	this.resizeCanvases();
    }

    public getLayer(layer_id:string):iLayer {
		if( ! this._layers.hasOwnProperty( layer_id ) ){
			throw new Error('Error: Can not get layer. Layer "'+layer_id+'" does not exist.');
		}
		return this._layers[ layer_id ];
	}

    public addPage( page:iPage ){
    	if( this._pages.hasOwnProperty( page.id ) ){
			throw new Error('Error: Can not add page. Page with id "'+page.id+'" already exist.');
		}
    	this._pages[ page.id ] = page;

    	for( let layer_id in page.layers ){
    		this.addLayer( page.layers[ layer_id ] );
    	}

    	page.onAddLayer.add( (layer:iLayer) => this.addLayer( layer ) );
    }

    public getPage(page_id:string):iPage {
		if( ! this._pages.hasOwnProperty( page_id ) ){
			throw new Error('Error: Can not get page. Page "'+page_id+'" does not exist.');
		}
		return this._pages[ page_id ];
	}

	public get pages():iPages {
		return this._pages;
	}

	/**
	 * neka zunanja komponenta nam javlja, na kakšno velikost moramo prilagoditi svoje layerje
	 */
	public resize( params:iResizeParams ){
		// shranimo parametre
		this._canvasRealWidth = params.stageWidth;
		this._canvasRealHeight = params.stageHeight;

		// sprožimo resize layerjey
		this.resizeCanvases();
	}

	/**
	 * zunanji klic, ki sproži rendanje objektov na layerjih / zgodi se ob vsakem loop frame-u
	 */
	public render(){
		// posredujemo naprej v privatno funkcijo, da tukaj ne mešamo štrenov
		this.processRender();
	}

	/**
	 * uničimo vse parametre / spraznimo cache, odstranimo DOM elmente / odstranimo listenerje / ...
	 */
	public destroy(){
		let touch_canvas = this._touch_layer.canvas;
		touch_canvas.removeEventListener('click', this.onCanvasClick );
	}



	// ******************************************************************
	// *
	// *       PRIVATNE METODE
	// *
	// ******************************************************************
	
	/**
	 * on canvas click
	 * @param {[type]} event
	 */
	private onCanvasClick = (event) => this.onClick(event);

	/**
	 * Layerje prilagodimo 100% glede na površino, ki nam je na voljo
	 */
	private resizeCanvases( now:boolean = false ){
		clearTimeout( this._resizeCanvasesTimeout );
		if( ! now ){
			this._resizeCanvasesTimeout = setTimeout( () => this.resizeCanvases( true ), 5 );
			return;
		}

		if( 0 == Object.keys( this._layers ).length ){
			return;
		}

		let first_layer_id:string = Object.keys( this._layers )[0];
		let first_layer:iLayer = this._layers[first_layer_id];
		let ctx = first_layer.ctx;

		let devicePixelRatio = window.devicePixelRatio || 2;
		if( devicePixelRatio > Config.render_max_devicePixelRatio ){
			devicePixelRatio = Config.render_max_devicePixelRatio;
		}
        let backingStoreRatio = ctx.webkitBackingStorePixelRatio ||
                            ctx.mozBackingStorePixelRatio ||
                            ctx.msBackingStorePixelRatio ||
                            ctx.oBackingStorePixelRatio ||
                            ctx.backingStorePixelRatio || 1;

        let ratio = devicePixelRatio / backingStoreRatio;

        let toWidth:number = this._canvasRealWidth;
        let toHeight:number = this._canvasRealHeight;
        let toScale:number = 1;

	    // upscale the canvas if the two ratios don't match
	    if ( devicePixelRatio !== backingStoreRatio ) {

	    	toWidth = ratio * this._canvasRealWidth;
	    	toHeight = ratio * this._canvasRealHeight;
	    	toScale = ratio;

	    }

	    Config.ctxWidth = toWidth;
	    Config.ctxHeight = toHeight;
	    Config.ctxTranslateX = 0;
	    Config.ctxTranslateY = 0;

	    let ctxSizeRatio = toWidth / toHeight;
	    let stageSizeRatio = Config.stage_width / Config.stage_height;
	    if( stageSizeRatio > ctxSizeRatio ){
	    	Config.ctxStageRatio = toWidth / Config.stage_width / toScale;
	    	Config.ctxTranslateY = ( toHeight / toScale - ( Config.ctxStageRatio * Config.stage_height ) ) / 2;
	    } else {
	    	Config.ctxStageRatio = toHeight / Config.stage_height / toScale;
	    	Config.ctxTranslateX = ( toWidth / toScale - ( Config.ctxStageRatio * Config.stage_width ) ) / 2;
	    }

	    // zaokrožimo parametre na lepše cifre
	    Config.ctxWidth = Utils.precise_round( Config.ctxWidth, 0 );
		Config.ctxHeight = Utils.precise_round( Config.ctxHeight, 0 );
		Config.ctxTranslateX = Utils.precise_round( Config.ctxTranslateX, 0 );
		Config.ctxTranslateY = Utils.precise_round( Config.ctxTranslateY, 0 );
		Config.ctxStageRatio = Utils.precise_round( Config.ctxStageRatio, 2 );

		// zaokrožimo in dodamo malenkost, da bo lepši / ostrejši izris
		// toWidth = Utils.precise_round( toWidth, 0 ) + 0.5;
		// toHeight = Utils.precise_round( toHeight, 0 ) + 0.5;

		for (let layer_id in this._layers) {
			let canvas = this._layers[ layer_id ].canvas;
			// nastavimo "pravo velikost canvasa"
			canvas.width = toWidth;
			canvas.height = toHeight;

			let ctx = this._layers[ layer_id ].ctx;
			// resetiramo context
			ctx.setTransform(1, 0, 0, 1, 0, 0);
			// nastavimo scale
			ctx.scale( toScale, toScale );

			// zapišemo, da smo imeli spremembe
			this._layers[ layer_id ].changed = true;			
		}

		// sprožimo, da so parametri za CTX spremenjeni / RenderObject elementi, bodo na novo izračunali svoje pozicije in se poravnali
		Config.onCtxParamsChange.trigger();
	}

	/**
	 * na layerju je bil sprožen klik
	 */
	private onClick( event ){
		event.preventDefault();

		// TODO - pravilno transliraj pozicijo miške
		let mouseX:number = ( event.offsetX - Config.ctxTranslateX ) / Config.ctxStageRatio;
		let mouseY:number = ( event.offsetY - Config.ctxTranslateY ) / Config.ctxStageRatio;

		let mouse_position:iMousePosition = {
			mouseX: mouseX,
			mouseY: mouseY
		};

		setTimeout( () => this.onClickHandle(mouse_position), 0 );
	}

	private onClickHandle( mouse_position:iMousePosition ){

		let mouseX:number = mouse_position.mouseX;
		let mouseY:number = mouse_position.mouseY;

		var layers_ids_reversed = Object.keys( this._layers ).reverse();	// top layers first
		for( var layer_id_index in layers_ids_reversed ){
			let layer_id:string = layers_ids_reversed[ layer_id_index ];
			let layer = this._layers[ layer_id ];

			if( layer.page && ! layer.page.visible ){
				continue;
			}

			if( ! layer.visible ){
				continue;
			}

			let pageOffsetX:number = 0;
			let pageOffsetY:number = 0;

			if( layer.page ){
				pageOffsetX = layer.page.x;
				pageOffsetY = layer.page.y;
			}

			let checkMouseX:number = mouseX - pageOffsetX;
			let checkMouseY:number = mouseY - pageOffsetY;

			// reversed loop for top objects first
			for (var i = layer.render_objects.length - 1; i >= 0; i--) {
				let render_object:iRenderObject = layer.render_objects[i]

				if(
					false !== render_object.onClick &&
					true == render_object.visible &&
					render_object.x <= checkMouseX &&
					render_object.x + render_object.width >= checkMouseX &&
					render_object.y <= checkMouseY &&
					render_object.y + render_object.height >= checkMouseY
					){

					mouse_position.mouseX = checkMouseX;
					mouse_position.mouseY = checkMouseY;

					setTimeout( () => render_object.onClick( mouse_position ), 0 );

					return;
				}
			}

			// check layer on click
			if( layer.onClick ){
				mouse_position.mouseX = checkMouseX;
				mouse_position.mouseY = checkMouseY;
				
				setTimeout( () => layer.onClick( mouse_position ), 0 );
				return;
			}
		}

	}

	/**
	 * Izris elementov na layerjih
	 */
	private processRender(){
		for( let layer_id in this._layers ){
			let layer = this._layers[ layer_id ];
			let ctx = layer.ctx;
			let pageChanged:boolean = ( layer.page && layer.page.changed );
			let pageVisible:boolean = ( ! layer.page || ( layer.page && layer.page.visible ) );

			let render_objects = layer.render_objects;
			let render_objects_changed:boolean = this.hasChangedRenderObjects( render_objects );

			let ctx_cleared:boolean = false;
			let renderOnEveryFrame:boolean = false;
			
			// preverimo, če slučajno ni potrebno na novo rendati
			if( ! renderOnEveryFrame && ! pageChanged && ! layer.changed && ! render_objects_changed ){
				continue;
			}

			// počistimo samo v primeru, da bomo na novo rendali ali page ni viden
			if( renderOnEveryFrame || ( pageChanged && ! pageVisible ) || layer.changed || render_objects_changed ){
				ctx_cleared = true;
				ctx.clearRect( 0, 0, Config.ctxWidth, Config.ctxHeight );
			}

			if( ! pageVisible || ! layer.visible ){
				// počistly smo layer, ampak ker "nismo vidni", ničesar ne izrisujemo
				continue;
			}

			layer.changed = false;

			// če imamo page, po potrebi premikamo layer-je
			if( layer.page && layer.page.changed ){
				layer.canvas.style.left = layer.page.ctxX + "%";
				layer.canvas.style.top = layer.page.ctxY + "%";
				// ctx.translate( layer.page.ctxX, layer.page.ctxY );
			}

			// če nismo počistili canvas-a, potem ni potrebe po ponovnem izrisovanju
			if( ! ctx_cleared ){
				continue;
			}

			for (let i = 0; i < render_objects.length; ++i) {
				let render_object:iRenderObject = render_objects[i];
				render_object.render( ctx );
			}

			if( layer.page ){
				// ctx.translate( - layer.page.ctxX, - layer.page.ctxY );
			}
		}

		for( let page_id in  this._pages ) {
			let page:iPage = this._pages[ page_id ];
			page.changed = false;
		}
	}

	private addLayerCanvasToHolderDom( layer:iLayer ){
		if( ! this._dom_holder_element ){
			return;
		}
    	this._dom_holder_element.appendChild( layer.canvas );
	}

	/**
	 * nam pove, če ima layer kaj render objektov, ki so bili spremenjeni / potrebujejo nov render
	 */
	private hasChangedRenderObjects( render_objects: iRenderObject[] ){
		for (let i = 0; i < render_objects.length; ++i) {
			let render_object:iRenderObject = render_objects[i];
			if( render_object.changed ){
				return true;
			}
		}
		return false;
	}


}