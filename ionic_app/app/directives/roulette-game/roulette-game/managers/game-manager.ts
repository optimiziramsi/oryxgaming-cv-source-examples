import { Injectable } from '@angular/core';

// *******************  entities  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';
import { RouletteGame_Config as Config } from '../entities/config';

// *******************  interfaces  *******************
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iGame as iGame } from '../interfaces/i-game';
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';
import { RouletteGame_iPayoutsByGridNumber as iPayoutsByGridNumber } from '../interfaces/i-payouts-by-grid-number';
import { RouletteGame_iPayoutsByGridKeyIndex as iPayoutsByGridKeyIndex } from '../interfaces/i-payouts-by-grid-key-index';
import { RouletteGame_iPayoutsInfo as iPayoutsInfo } from '../interfaces/i-payouts-info';
import { RouletteGame_iGridChips as iGridChips } from '../interfaces/i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';
import { RouletteGame_iBettingChip as iBettingChip } from '../interfaces/i-betting-chip';

// *******************  enums  *******************
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_ChipState as ChipState } from '../enums/chip-state';

@Injectable()
export class RouletteGame_GameManager implements iGameManager {

	private _game:iGame;

	public onGameState:iSignal<GameState> = new Signal();
	public onWinNumber:iSignal<number> = new Signal();
	public onBallShot:iSignal<any> = new Signal();
	public onUserCredits:iSignal<number> = new Signal();
	public onPayout:iSignal<number> = new Signal();
	public onStake:iSignal<number> = new Signal();
	public onConnection:iSignal<GameConnectionStatus> = new Signal();
	public onBetConfirm:iSignal<iGridChip> = new Signal();
	public onBetReject:iSignal<iGridChip> = new Signal();
	public onBetRemoveConfirmed:iSignal<iGridChip> = new Signal();
	public onBetRemoveRejected:iSignal<iGridChip> = new Signal();
	public onClearBets:iSignal<any> = new Signal();
	public onManualBallShotEnabled:iSignal<boolean> = new Signal();

	private _win_number:number;

	private _user_credits:number = 10000000;
	private _stake:number = 0;

	private _previous_bets:iGridChip[] = [];

	private _game_state:GameState = GameState.OPEN_FOR_BETS;

	private _grid_chips:iGridChip[] = [];
	private _grid_chips_by_grid_key:iGridChips = {};
	private _grid_chips_by_grid_number:iGridChips = {};
	private _payouts_by_grid_number:iPayoutsByGridNumber;
	private _bet_values_by_grid_key_index:iPayoutsByGridKeyIndex;
	private _connection:GameConnectionStatus = GameConnectionStatus.CONNECTED;

	constructor(){

		this._payouts_by_grid_number = this.getObjectWithGridNumbers();
		this._bet_values_by_grid_key_index = this.getObjectWithGridKexIndex();

		this.onClearBets.add( this.onClearBetsHandle );

		this.onClearBetsHandle();
	}

	public get grid_chips():iGridChip[] {
		return this._grid_chips;
	}

	public canMakeBet( grid_chip:iGridChip ){
		
		// preverimo za game state
		if( GameState.OPEN_FOR_BETS != this._game_state ){
			return false;
		}

		// preverimo, če ima uporabnik dovolj kreditov
		if( ! this.userHasCreditsFor( grid_chip ) ){
			return false;
		}

		// dobimo info / izračun možnih stav
		let payouts_info:iPayoutsInfo = this.getPayoutsInfo( grid_chip );

		// preverimo, če bi s stavo slučajno presegli max payout
		if( Config.game_max_payout > 0 && payouts_info.max_payout > Config.game_max_payout ){
			return false;
		}

		// preverimo, če izpolnjujemo max bet limit na stavno polje
		if( Config.game_max_bet > 0 && payouts_info.bet > Config.game_max_bet ){
			return false;
		}

		// preverimo, če izpolnjujemo pogoj za min bet value / minimalno stavo na stavno polje
		if( Config.game_min_bet > 0 && payouts_info.bet < Config.game_min_bet ){
			return false;
		}

		// preverimo, če izpolnjujemo pogoj za minimalno vrednost stav na mizi
		// TODO - to nekako nima smisla pri preverjanju, ali lahko stavimo, ker če zavrnemo stavo, nikoli ne bomo prišli
		// do zahtevane vrednosti stav na mizi
		if( false && Config.game_min_stake > 0 && payouts_info.stake < Config.game_min_stake ){
			return false;
		}

		// preverimo, če presegamo maksimalno vrednost stav na mizi
		if( Config.game_max_stake > 0 && payouts_info.stake > Config.game_max_stake ){
			return false;
		}

		// vsa preverjanja so bila uspešna
		return true;
	}

	public canRemoveBet( grid_chip:iGridChip ) : boolean {
		// preverimo za game state
		if( GameState.OPEN_FOR_BETS != this._game_state ){
			return false;
		}

		// vsa preverjanja so ok, zato dovolimo odstranitev stave
		return true;
	}

	public setGame( game:iGame ){
		this._game = game;

		// TODO - nastavimo parmetre, primerne igri
	}

	public getConnectionStatus() : GameConnectionStatus {
		return this._connection;
	}

	public getGameState() : GameState {
		return this._game_state;
	}

	public getWinNumber() : number {
		return this._win_number;
	}

	public getPayout() : number {
		return 1;
	}

	public getUserCredits() : number {
		return this._user_credits;
	}

	public getStake() : number {
		return this._stake;
	}

	public getMaxPossiblePayout() : number {
		return 1;
	}

	public requestAddBet( grid_chip:iGridChip ) {
		this.saveGridChip( grid_chip );

		setTimeout( () => this.addBetHandle( grid_chip ), 10 );
	}

	public requestRemoveBet( grid_chip:iGridChip ){

		setTimeout( () => this.removeBetHandle( grid_chip ), 10 );
	}

	public makeBallShot() {
		if( ! this.getManualBallShotEnabled() ){
			return;
		}
		// TODO - kako bi reševali tole
		setTimeout(() => {

			this._win_number = this.getRandomInt( 0, 37 );
			this.onWinNumber.trigger( this._win_number );
			
			this.handlePayouts();
		}, Config.ball_shot_animation_duration * 1000 );

		this.onBallShot.trigger();
	}

	public getPreviousBets() : iGridChip[] {
		return this._previous_bets;
	}

	public getManualBallShotEnabled():boolean {
		return true;
	}

	public destroy() {

	}


	
	// ******************************************************************
	// *
	// *       PRIVATE METHODS
	// *
	// ******************************************************************
	
	private addBetHandle( grid_chip:iGridChip ){
		// še enkrat preverimo skozi notranje preverjanje
		if( ! this.canMakeBet( grid_chip ) ){
			this.onBetReject.trigger( grid_chip );
			return;
		}

		// če imamo zunanjo igro, pošljemo zahtevek naprej igri
		if( this._game ){
			// TODO - pošljemo v zunanjo igro za preverbo
			return;
		}
		
		// *******************  kot kaže je s stavo vse ok, zato jo dodamo na mizo in to javimo  *******************
		this.confirmBet( grid_chip );
	}

	private confirmBet( grid_chip:iGridChip ){
		// shranimo chip še na drugo potrebno listo
		if( ! ( grid_chip.grid_key_index in this._grid_chips_by_grid_key ) ){
			this._grid_chips_by_grid_key[ grid_chip.grid_key_index ] = [];
		}
		this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].push( grid_chip );

		// dodamo možne payouts v nabor za payout-e
		for( let grid_number in grid_chip.payouts ){
			this._payouts_by_grid_number[ grid_number ] += grid_chip.payouts[ grid_number ];
		}

		// dobimo vrednost stave
		let chip_value:number = grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value;

		// odstranimo denar pri uporabniku
		this._user_credits -= chip_value;
		// povečamo stake na mizi
		this._stake += chip_value;
		// spremenimo status stave
		grid_chip.state = ChipState.CONFIRMED;

		// javimo, da je bila stava potrjena
		this.onBetConfirm.trigger( grid_chip );
		// javimo, da se je spremenilo stanje uporabnika
		this.onUserCredits.trigger( this._user_credits );
		// javimo, da se je spremenil stake / seštevek vseh stav na mizi
		this.onStake.trigger( this._stake );
	}

	private removeBetHandle( grid_chip:iGridChip ){
		if( ! this.canRemoveBet( grid_chip ) ){
			grid_chip.delete = false;
			this.onBetRemoveRejected.trigger( grid_chip );
		}

		// če imamo zunanjo igro, pošljemo zahtevek naprej igri
		if( this._game ){
			// TODO - pošljemo v zunanjo igro za preverbo
			return;
		}

		// *******************  kot kaže je vse ok in lahko stavo odstranimo  *******************
		this.confirmBetRemove( grid_chip );
	}

	private confirmBetRemove( grid_chip:iGridChip ){
		let chip_value:number = grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value;

		// odstranimo možne dobitke, ki bi bili, če ta stava ostane
		if( grid_chip.state = ChipState.CONFIRMED ){
			let grid_chip_payouts_info = this.getPayoutsInfo( grid_chip, true );
			let payouts_by_grid_number = grid_chip_payouts_info.payouts_by_grid_number;
			for( let grid_number in payouts_by_grid_number ){
				let grid_number_payput:number = payouts_by_grid_number[ grid_number ];
				this._payouts_by_grid_number[ grid_number ] -= grid_number_payput;
			}
		}

		this._user_credits += chip_value;
		this._stake -= chip_value;

		this.onBetRemoveConfirmed.trigger( grid_chip );
		this.onUserCredits.trigger( this._user_credits );
		this.onStake.trigger( this._stake );
		
		this.removeGridChip( grid_chip );
		if( grid_chip.grid_key_index in this._grid_chips_by_grid_key ){
			let grid_chip_index:number = this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].indexOf( grid_chip );
			this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].splice( grid_chip_index, 1 );
		}
	}

	private handlePayouts(){
		let grid_number = this._win_number;
		let payout:number = this._payouts_by_grid_number[ grid_number ];
		
		// dodamo novo stanje uporabniku
		this._user_credits += payout;
		// koliko je denarja na mizi
		this._stake = 0;

		// javimo informacije še ostalim
		this.onPayout.trigger( payout );
		this.onUserCredits.trigger( this._user_credits );
		this.onStake.trigger( this._stake );
	}

	private getPayoutsInfo( grid_chip:iGridChip, onlyThisBet:boolean = false ):iPayoutsInfo{
		if( ! grid_chip.payouts ){
			grid_chip.payouts = this.getPayouts( grid_chip );
		}

		let payouts = grid_chip.payouts;
		
		let payouts_info:iPayoutsInfo = {
			max_payout: 0,
			bet: 0,
			stake: 0,
			payouts_by_grid_number: payouts,
		};

		// dobimo maximalno možno izplačilo
		for( let grid_number in payouts ){
			let grid_number_payout:number = onlyThisBet ? payouts[ grid_number ] : this._payouts_by_grid_number[ grid_number ] + payouts[ grid_number ];
			if( payouts_info.max_payout < grid_number_payout ){
				payouts_info.max_payout = grid_number_payout;
			}
		}
		
		// dobimo bet na trenutnem stavnem mestu
		if( ! onlyThisBet && grid_chip.grid_key_index in this._grid_chips_by_grid_key ){
			let bet:number = 0;
			this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].forEach( ( tmp_gc:iGridChip ) => {
				if( ! tmp_gc.delete ){
					bet += tmp_gc.double_bet ? tmp_gc.value * 2 : tmp_gc.value;
				}
			} );
			payouts_info.bet = bet;
		}
		payouts_info.bet += grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value;

		// dobimo stake na celotni mizi
		payouts_info.stake = (onlyThisBet ? 0 : this._stake ) + ( grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value );

		return payouts_info;
	}

	private getPayouts( grid_chip:iGridChip ){

		let payouts:iPayoutsByGridNumber = this.getObjectWithGridNumbers();

		let is_double_zero_grid:boolean = true;	// TODO - tole bi bilo potrebno iz Config-a braz verjetno

		// definiramo grid key index, ki ga bomo uporabili za izračun
		let key:number = grid_chip.grid_key_index;
		let howmuch:number = grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value;

		// za lažjo predtavo imamo pred seboj ležeči grid
		// +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
		// |     |  3  |  6  |  9  |  12 |  15 |  18 |  21 | ...
		// |  00 +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
		// +-----+  2  |  5  |  8  |  11 |  14 |  17 |  20 | ...
		// |  0  +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
		// |     |  1  |  4  |  7  |  10 |  13 |  16 |  19 | ...
		// +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
		// 
		// 
		// DEL = sklop števil, razrezan po sklopih:
		// 		0 = [0, 00, 1-12], 1 = [ 13-24 ], 2 = [ 25-36 ]
		// VRSTA = vsi možni stolpci, ampak po sklopih, kot jih definira DEL
		// 		možni so rezultati 1-7
		// 		--- prvi sklop DEL, DEL = 0, številke 0, 00, 1-12
		// 		0 = [0, 00, 0:1, 0:1:2, 0:00:2, 00:2:3, 00:3]
		// 		1 = [1, 1:2, 2, 2:3, 3, 1:2:3 ]
		// 		2 = [1:4, 1:2:4:5, 2:5, 2:5:3:6, 3:6, 1:2:3:4:5:6 ]
		// 		3 = [4, 4:5, 5, 5:6, 6, 4:5:6 ]
		// 		...
		// 		--- drugi sklop DEL, DEL = 1, številke 13-24 + preklopi na prejšnji DEL s številkami 10, 11, 12
		// 		0 = [10:13, 10:13:11:14, 11:14, 11:14:12:15, 12:15, 10:11:12:13:14:15 ]
		// 		1 = [13, 13:14, 14, 14:15, 15, 13:14:15 ]
		// 	ST = vrstice, ki delujejo podobno kot VRSTA / stolpci, le da gremo po vodoravni liniji
		// 		0 = [ 1:2:3, 1:2:3:4:5:6, 4:5:6, ...]	 - vse zgornje vrstice, kjer stavimo na celoten stolpec in s tem zajamemo vse vrstice
		// 		1 = [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34] - prva vrstica številk
		// 		2 = [1:2, 4:5, 7:8, 10:11, 13:14, ...] - prva vmesna vrstica med števili
		// 		3 = [2, 5,8, 11, 14, ... ] - druga vrstica posameznih števil
		// 		4 = [2:3, 5:6, 8:9, 11:12, ...] vmesna števila med drugo in tretjo vrstico od spodaj
		// 		5 = [3, 6, 9, 12, 15, ... ] - tretja vrstica števil od spodaj
		// 		7 = [0, 00, ODD, EVEN, stranski "2-1", spodnji "1-12"|"13-24"|"25-36", spodnji "1-18"|"19-36"] - dodatna polja, ki niso številke

		// definiramo osnovne informacije za "membrano" na grid-u
		let del:number = Math.floor( key / 64 );
		let vrsta:number = Math.floor( ( key - ( del * 64 ) ) / 8 );
		let st = key - ( del * 64 + vrsta * 8);
		
		if( is_double_zero_grid && 23 == key ){	// key = 23 - ko žeton postavimo med 0 in 00
			payouts[0]+=howmuch*18;
			payouts[37]+=howmuch*18;
		}

		else {

			switch(st){
				case 7:
					//cel hudic
					switch( del ){
						case 0:
							switch( vrsta ){
								case 0:
									//nicla
									payouts[0]+=howmuch*36;
									break;
								case 1:
									//low 1-18
									for( let i = 1; i <=18; i++ ){
										payouts[i]+=howmuch*2;
									}
									break;
								case 4:
									//low 1-12
									for( let i = 1; i <=12; i++ ){
										payouts[i]+=howmuch*3;
									}
									break;
								case 5:
									//even
									for( let i = 2; i <= 36; i+=2){
										payouts[i]+=howmuch*2;
									}
									break;
							}
							break;
						case 1:
							switch(vrsta){
								case 1:
									//rdeca
									payouts[1]+=howmuch*2;
									payouts[3]+=howmuch*2;
									payouts[5]+=howmuch*2;
									payouts[7]+=howmuch*2;
									payouts[9]+=howmuch*2;
									payouts[12]+=howmuch*2;
									payouts[14]+=howmuch*2;
									payouts[16]+=howmuch*2;
									payouts[18]+=howmuch*2;
									payouts[19]+=howmuch*2;
									payouts[21]+=howmuch*2;
									payouts[23]+=howmuch*2;
									payouts[25]+=howmuch*2;
									payouts[27]+=howmuch*2;
									payouts[30]+=howmuch*2;
									payouts[32]+=howmuch*2;
									payouts[34]+=howmuch*2;
									payouts[36]+=howmuch*2;

									break;
								case 5:
									//crna
									payouts[2]+=howmuch*2;
									payouts[4]+=howmuch*2;
									payouts[6]+=howmuch*2;
									payouts[8]+=howmuch*2;
									payouts[10]+=howmuch*2;
									payouts[11]+=howmuch*2;
									payouts[13]+=howmuch*2;
									payouts[15]+=howmuch*2;
									payouts[17]+=howmuch*2;
									payouts[20]+=howmuch*2;
									payouts[22]+=howmuch*2;
									payouts[24]+=howmuch*2;
									payouts[26]+=howmuch*2;
									payouts[28]+=howmuch*2;
									payouts[29]+=howmuch*2;
									payouts[31]+=howmuch*2;
									payouts[33]+=howmuch*2;
									payouts[35]+=howmuch*2;

									break;
								case 6:
									//2nd
									for( let i = 13; i <= 24; i++){
										payouts[i]+=howmuch*3;
									}
									break;
							}
							break;
						case 2:
							switch( vrsta ){
								case 1:
									//odd
									for( let i = 1; i <= 36; i+=2) {
										payouts[i]+=howmuch*2;
									}
									break;
								case 4:
									//3rd
									for( let i = 25; i <= 36; i++){
										payouts[i]+=howmuch*3;
									}
									break;
								case 5:
									//high 19-36
									for( let i = 19; i <= 36; i++){
										payouts[i]+=howmuch*2;
									}
									break;
								case 7:
									//2to1 34
									for( let i = 1; i <= 36; i+=3 ){
										payouts[i]+=howmuch*3;
									}
									break;
								case 6:
									//2to1 35
									for( let i = 2; i <= 36; i+=3 ){
										payouts[i]+=howmuch*3;
									}
									break;
								case 3:
									//2to1 36
									for( let i = 3; i <= 36; i+=3 ){
										payouts[i]+=howmuch*3;
									}
									break;
							}
							break;
					}
					// end of special fields
					break;

				case 6:
				case 0: //stranska
					vrsta += del * 8;
					if( 0 == vrsta % 2 ){
						//soda vrsta
						if( 0 == vrsta ){
							//petica
							if( is_double_zero_grid ){
								if( 0 == st ){
									payouts[0]+=howmuch*7;
									payouts[1]+=howmuch*7;
									payouts[2]+=howmuch*7;
									payouts[3]+=howmuch*7;
									payouts[37]+=howmuch*7;


								}
								if( 6 == st ){
									payouts[37]+=howmuch*36;
								}
							}
							// ni 00 grid
							else {
								payouts[0]+=howmuch*9;
								payouts[1]+=howmuch*9;
								payouts[2]+=howmuch*9;
								payouts[3]+=howmuch*9;

							}
						}

						else {
							//sestica
							let tmp1:number = Math.floor( (vrsta-1) / 2 ) * 3 + 1;	// TODO - preveri, če je sigurno vse ok, lahko da bo potreben Math.floor()
							for( let i = tmp1; i <= tmp1 + 5; i++ ){
								payouts[i]+=howmuch*6;
							}
						}
					}
					// liha vrsta
					else {
						//liha vrsta
						let tmp1:number = Math.floor( ( vrsta-1 ) / 2 ) * 3 + 1;	// TODO - preveri, če je sigurno vse ok, lahko da bo potreben Math.floor()
						for( let i = tmp1; i <= tmp1 + 2; i++ ){
							payouts[i]+=howmuch*12;
						}
					}
					break;

				case 3:
				case 5:
				case 1: //celi
					vrsta += del * 8;
					if( 0 == vrsta % 2 ){
						//soda vrsta
						if(st==3){ st=2 };
						if(st==5){ st=3 };
						if(vrsta==0){
							//printf("ne vem za te 3\n");
							if( is_double_zero_grid ){
								switch(st){
									case 1:
										payouts[0]+=howmuch*18;
										payouts[1]+=howmuch*18;
										break;
									case 2:
										payouts[0]+=howmuch*12;
										payouts[2]+=howmuch*12;
										payouts[37]+=howmuch*12;
										

										break;
									case 3:
										payouts[3]+=howmuch*18;
										payouts[37]+=howmuch*18;

										break;
								}
							}

							else {
								payouts[0]+=howmuch*18;
								payouts[st]+=howmuch*18;

							}
						}

						else {
							let tmp1:number = Math.floor( ( vrsta-1 ) / 2 ) * 3 + st;
							payouts[tmp1] +=howmuch*18;
							payouts[tmp1+3]+=howmuch*18;

						}
					}
					else {
						//liha vrsta
						if(st==3){ st=2 }
						if(st==5){ st=3 }
						let tmp1:number = Math.floor( ( vrsta-1 ) / 2 ) * 3 + st;
						payouts[tmp1]+=howmuch*36;
					};
					break;
				case 4:
				case 2: //vmesni
					vrsta += del * 8;
					if( 0 == vrsta % 2 ){
						//soda vrsta
						if(0 == vrsta ){
							if( is_double_zero_grid ){
								switch(st){
									case 2:
										payouts[0]+=howmuch*12;
										payouts[1]+=howmuch*12;
										payouts[2]+=howmuch*12;

										break;
									case 4:
										payouts[2]+=howmuch*12;
										payouts[3]+=howmuch*12;
										payouts[37]+=howmuch*12;

										break;
								}
							}

							else {
								let tmp1 = Math.floor( st / 2 );
								payouts[0]+=howmuch*12;
								payouts[tmp1]+=howmuch*12;
								payouts[tmp1+1]+=howmuch*12;

							}
						}

						else {
							if(st==2){ st=1 }
							if(st==4){ st=2 }
							let tmp1 = Math.floor( ( vrsta-1 ) / 2 ) * 3 + st;
							payouts[tmp1]+=howmuch*9;
							payouts[tmp1+1]+=howmuch*9;
							payouts[tmp1+3]+=howmuch*9;
							payouts[tmp1+4]+=howmuch*9;

						}
					}
					else {
						//liha vrsta
						if(st==2){ st=1 }
						if(st==4){ st=2 }
						let tmp1 = Math.floor( ( vrsta-1 ) / 2 ) * 3 + st;
						payouts[tmp1]+=howmuch*18;
						payouts[tmp1+1]+=howmuch*18;

					}
					break;
			}

		}

		return payouts;
	}

	private onClearBetsHandle = () => {
		// shranimo prejšnje stave / ta grod stav
		this._previous_bets = this._grid_chips;

		this._grid_chips = [];
		this._grid_chips_by_grid_key = {};
		this._grid_chips_by_grid_number = {};
	};

	private getObjectWithGridNumbers():iPayoutsByGridNumber {
		let obj:iPayoutsByGridNumber = {};
		for (let i:number = 0; i <= 37; ++i) {
			obj[ i ] = 0;
		}
		return obj;
	}

	private getObjectWithGridKexIndex():iPayoutsByGridKeyIndex {
		let obj:iPayoutsByGridKeyIndex = {};
		for (let i:number = 0; i <= 200; ++i) {
			obj[ i ] = 0;
		}
		return obj;
	}


	/**
	 * povežemo se z igro in pridobimo vse potrebne podatke
	 */
	private initWithGame(){
		// TODO
		
		// if( ! this.game ){
		// 	throw new Error('UiManager::initWithGame() error. Param game not set.');
		// }

		// let g = this.game;

		// this.user = {
		// 	balance: g.getUserBalance(),
		// 	bets_history: [],
		// 	bets: [],	
		// };

		// assign listeners
		// g.onGameStateChange.add(() => { console.log('onGameStateChange') });
		// g.onNumberSelected.add(() => { console.log('onNumberSelected') });
		// g.onBallShot.add(() => { console.log('onBallShot') });
		// g.onUserBalanceChange.add(() => { console.log('onWinPayout') });
		// g.onWinPayout.add(() => { console.log('onWinPayout') });
		// g.onBalanceOnTableChange.add(() => { console.log('onBalanceOnTableChange') });
		// g.onConnectionChange.add(() => { console.log('onConnectionChange') });
		// g.onBetConfirm.add(() => { console.log('onBetConfirm') });
		// g.onBetReject.add(() => { console.log('onBetReject') });

		// g.onGameStateChange.add(() => this.setGameState());
		// g.onUserBalanceChange.add(() => this.setUserBalance());
		// g.onBalanceOnTableChange.add(() => this.setGridBalance());

		// this.game.start();

		// this.setUserBalance();
		// this.setGridBalance();
		// this.setGameState();
	}

	private setUserBalance(){
		// this.user.balance = this.game.getUserBalance();
		// this._user_balance_txt.text = "Balance: " + this.user.balance;
	}

	private setGridBalance(){
		// let balance:number = this.game.getBalanceOnTable();
		// this._grid_bets_balance_txt.text = "Bets: " + balance;
	}

	private setGameState(){
		// let state:GameState = this.game.game_state;
		// let state_lbl:string = "Unknown";
		// switch (state) {
		// 	case GameState.OPEN_FOR_BETS:
		// 		state_lbl = "Open for bets";
		// 		break;

		// 	case GameState.CLOSED_FOR_BETS:
		// 		state_lbl = "Closed for bets";
		// 		break;
		// }
		// this._game_state_txt.text = "GameState: " + state_lbl;
	}

	private userHasCreditsFor( grid_chip:iGridChip ):boolean {
		return this._user_credits >= ( grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value );
	}


	private isGameOpenForBetting():boolean {
		return true;
		// return ( this._game && this._game.game_state == GameState.OPEN_FOR_BETS );
	}

	private saveGridChip( grid_chip:iGridChip ){
		let chip_index = this._grid_chips.indexOf( grid_chip );
		// žeton že obstaja, zato ga ne dodajamo
		if( -1 != chip_index ){
			return;
		}

		this._grid_chips.push( grid_chip );
	}

	private removeGridChip( grid_chip:iGridChip ){
		let chip_index = this._grid_chips.indexOf( grid_chip );
		if( -1 != chip_index ){
			this._grid_chips.splice( chip_index, 1);
		}
	}

	// *************************************************
	// 		UTILS - START
	// *************************************************
	
	private uniqueid(){
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < 12; i++ ){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		return text;
	}

	private getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
	
	// *************************************************
	// 		UTILS - END
	// *************************************************


	
}