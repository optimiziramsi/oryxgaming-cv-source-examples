import { Injectable } from '@angular/core';
import "gsap";

// *******************  managers  *******************
import { RouletteGame_AssetsManager as AssetsManager } from './assets-manager';
import { RouletteGame_LayersManager as LayersManager } from './layers-manager';
import { RouletteGame_ConfigManager as ConfigManager } from './config-manager';
import { RouletteGame_GameManager as GameManager } from './game-manager';

// *******************  entities  *******************
import { RouletteGame_Config as Config } from '../entities/config';
import { RouletteGame_Layer as Layer } from '../entities/layer';
import {
	RouletteGame_RenderObject as RenderObject,
	RouletteGame_ImageRenderObject as ImageRO,
	RouletteGame_ActiveImageRenderObject as ActiveImageRO,
	RouletteGame_TextRenderObject as TextRO,
	RouletteGame_TileRenderObject as TileRO,
	RouletteGame_RectangleRenderObject as RectangleRO,
	} from '../entities/render-object';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from '../interfaces/i-ui-manager';
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iPage as iPage } from '../interfaces/i-page';

// *******************  pages  *******************
import { RouletteGame_BetPage as BetPage } from '../pages/bet-page';
import { RouletteGame_CirclePage as CirclePage } from '../pages/circle-page';

@Injectable()
export class RouletteGame_UiManager implements iUiManager {

	private am:iAssetsManager;
	private lm:iLayersManager;
	private gm:iGameManager;

	// *******************  pages  *******************
	private betPage:iPage;
	private circlePage:iPage;

	constructor( am:AssetsManager, lm:LayersManager, gm:GameManager ){
		this.am = am;
		this.lm = lm;
		this.gm = gm;
	}

	/**
	 * ko imamo zlovdane vse datoteke, slike, pripravljene layerje itd, pripravimo vse elemente
	 */
	public init(){

		// *******************  bacgkround  *******************
		let bg_l:iLayer = new Layer('background');
			let bg_color_rect = new RectangleRO( Config.stage_width, Config.stage_height );
				bg_color_rect.color = "#910000";
			bg_l.addRenderObject( bg_color_rect );

			let bg_alpha = new TileRO( this.am.get('bg_alpha') );
			bg_l.addRenderObject( bg_alpha );

		this.lm.addLayer( bg_l );

		// *******************  bet page  *******************
		this.betPage = new BetPage();
		// this.betPage.visible = false;
		this.betPage.am = this.am;
		this.betPage.lm = this.lm;
		this.betPage.gm = this.gm;
		this.betPage.um = this;
		this.betPage.x = 0;
		this.betPage.y = 0;
		this.betPage.init();	// TODO - timeout = 0
		this.lm.addPage( this.betPage );

		// *******************  circle page  *******************
		this.circlePage = new CirclePage();
		// this.circlePage.visible = false;
		this.circlePage.am = this.am;
		this.circlePage.lm = this.lm;
		this.circlePage.gm = this.gm;
		this.circlePage.um = this;
		this.betPage.x = - Config.stage_width * 1.1;
		this.betPage.y = 0;
		this.circlePage.init();		// TODO - timeout = 0
		this.lm.addPage( this.circlePage );

		// *******************  stats layer  *******************
		let stats_l:iLayer = new Layer('stats');
		this.lm.addLayer( stats_l );

		// *******************  game  *******************
		// this.initWithGameManager();	TODO - uncomment

		this.switchPage( this.betPage, true );
		// this.switchPage( this.circlePage, true );
	}

	public switchPage( page:iPage, noAnimation:boolean = false ){
		let duration:number = noAnimation ? 0 : 0.5;
		switch ( page ) {
			case this.betPage:
				TweenLite.to( this.betPage, duration, { x: 0 } );
				TweenLite.to( this.circlePage, duration, { x: - Config.stage_width * 1.1 } );
				break;

			case this.circlePage:
				TweenLite.to( this.betPage, duration, { x: Config.stage_width * 1.1 } );
				TweenLite.to( this.circlePage, duration, { x: 0 } );
				break;
		}
	}

	

	/**
	 * počisti vse za seboj
	 */
	public destroy(){
		// TODO
	}
	
}