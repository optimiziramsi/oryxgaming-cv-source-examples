
// *******************  entities  *******************
import { RouletteGame_Signal as Signal } from './signal';

// *******************  interfaces  *******************
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';
import { RouletteGame_iConfigBetChip as iConfigBetChip } from '../interfaces/i-config-bet-chip';

// *******************  enums  *******************
import { RouletteGame_GameType as GameType } from '../enums/game-type';

export class RouletteGame_Config {
	
	// ******************************************************************
	// *
	// *       Parametri, ki jih uporablja aplikacija za igro
	// *
	// ******************************************************************
	/**
	 * razmerje med realnimi dimenzijami izrisa in dimenzijami, za katere imamo narejen design
	 * @type {number}
	 */
	public static ctxStageRatio:number = 1;

	/**
	 * za koliko po X osi je zamaknjen naš "stage", v katrem izrisujemo igro
	 * @type {number}
	 */
	public static ctxTranslateX:number = 0;

	/**
	 * za koliko po Y osi je zamaknjen naš "stage", v katrem izrisujemo igro
	 * @type {number}
	 */
	public static ctxTranslateY:number = 0;

	/**
	 * realna površina canvasa za izris - uporablja se za "čiščenje" canvas-a
	 * @type {number}
	 */
	public static ctxWidth:number = 0;

	/**
	 * realna površina canvasa za izris - uporablja se za "čiščenje" canvas-a
	 * @type {number}
	 */
	public static ctxHeight:number = 0;
	
	/**
	 * signal / event, ko so se spremenili parametri za canvas context
	 * @type {iSignal<any>}
	 */
	public static onCtxParamsChange:iSignal<any> = new Signal();
	/**
	 * Koliko FPS bi radi dosegli. Za MAX (max kar podpira naprava je po navadi 60 FPS), nastavi 0 ali manj
	 * @type {number}
	 */
	public static use_fps:number = 30;


	// ******************************************************************
	// *
	// *       Parametri, ki jih dobimo iz configs.json / parametri za inicializacijo igre
	// *
	// ******************************************************************
	
	/**
	 * širina design-a / stage-a, po katerem bomo uporabljali koordinacijski sistem
	 * @type {number}
	 */
	public static stage_width:number = 1920;

	/**
	 * višina design-a / stage-a, po katerem bomo uporabljali koordinacijski sistem
	 * @type {number}
	 */
	public static stage_height:number = 1080;

	/**
	 * določa tip igre, ki jo bomo uporabljali v aplikaciji. Igra je lahko lokalna, zunanja preko Socket.io ali česa drugega
	 * @type {GameType}
	 */
	public static game_type:GameType = GameType.NONE;
	
	/**
	 * Ali naj prikazujemo statistiko za FramesPerSecond
	 * @type {boolean}
	 */
	public static stats_show_fps:boolean = false;

	/**
	 * Ali naj za rendanje uporabljamo cache
	 * @type {boolean}
	 */
	public static render_use_cache:boolean = false;

	/**
	 * maximalno razmerje / povečava za HiDPI zaslone.
	 * @type {number}
	 */
	public static render_max_devicePixelRatio:number = 2;

	/**
	 * pot do mape, kjer imamo potrebne datoteke za aplikacijo
	 * @type {string}
	 */
	public static assets_dir:string = "";

	/**
	 * seznam žetonov in podatki za posameznega, s katerimi lahko stavimo v igri
	 * @type {iConfigBetChip[]}
	 */
	public static bet_chips:iConfigBetChip[] = [];

	/**
	 * pasica, ki je prikazana nad grid-om
	 * @type {string}
	 */
	public static base_top_src:string = "";

	/**
	 * pasica, ki je prikazana pod grid-om
	 * @type {string}
	 */
	public static base_bottom_src:string = "";

	/**
	 * hitrost vrtenja wheel-a
	 * @type {number}
	 */
	public static wheel_rotations_per_second:number = 0.2;

	/**
	 * čas animacije kugle, definirana v sekundah
	 * @type {number}
	 */
	public static ball_shot_animation_duration:number = 10;

	/**
	 * ko žogico "vržemo", kakšen radij naj bo nastavljen na začetku
	 * @type {number}
	 */
	public static ball_rotation_outer_offset:number = 500;

	/**
	 * ko žogico vržemo, se počasi premika proti sredini in ta parameter definira, kakšen je notranji radij
	 * @type {number}
	 */
	public static ball_rotation_inner_offset:number = 275;

	/**
	 * parametri za grid-e
	 * @type {iConfigGrid[]}
	 */
	public static grid_square_src_img:string = "";
	public static grid_square_src_colors:string = "";
	public static grid_square_src_keys:string = "";

	public static grid_circle_src_img:string = "";
	public static grid_circle_src_colors:string = "";
	public static grid_circle_src_keys:string = "";

	public static grid_portrait_src_img:string = "";
	public static grid_portrait_src_keys:string = "";

	/**
	 * wheel source-i
	 * @type {string}
	 */
	public static wheel_inner_src:string = "";
	public static wheel_outter_src:string = "";
	public static wheel_numbers_rotations_src:string = "";
	public static wheel_ball_src:string = "";


	/******************************************************************
	*
	*       PARAMETRI ZA GAME LIMITS
	*
	******************************************************************/
	
	/**
	 * če želimo omejiti maximalno vrednost izplačila
	 * če je neomejeno, potem naj bo vrednost 0 ali manj
	 * @type {number}
	 */
	public static game_max_payout:number = 0;

	/**
	 * kolikšen je MAX bet na posameznem grid_key-ju (gird_key != grid_number )
	 * če ni omejitve, potem naj bo vrednost 0 ali manj
	 * @type {number}
	 */
	public static game_max_bet:number = 0;

	/**
	 * kolikšna je minimalna stava na stavno polje / grid_key ( grid_key != grid_number )
	 * če ni omejitve, naj bo vrednost 0 ali manj
	 * @type {number}
	 */
	public static game_min_bet:number = 0;

	/**
	 * minimalna vrednost vseh stav na mizi
	 * za neomejeno naj bo vrednost 0 ali manj
	 * @type {number}
	 */
	public static game_min_stake:number = 0;

	/**
	 * maksimalna vrednost stav na mizi
	 * za neomejeno, naj bo vrednost 0 ali manj
	 * @type {number}
	 */
	public static game_max_stake:number = 0;

}