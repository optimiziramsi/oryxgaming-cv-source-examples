// *******************  interfaces  *******************
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iPage as iPage } from '../interfaces/i-page';
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';


export class RouletteGame_Layer implements iLayer {

	public canvas:any;
	public ctx:any;
	public changed:boolean = false;
	private _visible:boolean = true;
	public onClick:any;
	public page:iPage;

	private _id:string;
	private _render_objects:iRenderObject[] = [];

	constructor( layer_id:string ){
		this._id = layer_id;

		this.canvas = document.createElement('canvas');
		this.ctx =  this.canvas.getContext('2d');
	}

	public get id() : string {
		return this._id;
	}

	public get visible():boolean { return this._visible; }
	public set visible( val:boolean ) { if( val != this._visible ){ this._visible = val; this.changed = true; } }

	public get render_objects() : iRenderObject[] {
		return this._render_objects;
	}

	public addRenderObject( ro:iRenderObject ){
		// check if render object already exists in requested layer
		let exsists_index = this._render_objects.indexOf( ro );
		if( exsists_index > -1 ){
			// already exists, put it on top
			let last_index = this._render_objects.length - 1;
			this._render_objects.splice( last_index, 0, this._render_objects.splice(exsists_index, 1 )[0]);
		} else {
			// render object še ne obstaja. zato ga dodamo v layer
			this._render_objects.push( ro );
		}

		// povemo tudi, da so bile spremembe na layerju, da se lahko izrišejo ob naslednjem loop-u
		this.changed = true;
	}

	public removeRenderObject( ro:iRenderObject ){
		let ro_index:number = this._render_objects.indexOf( ro );
		if( -1 != ro_index ){
			this._render_objects.splice(ro_index, 1);
			this.changed = true;
		}
	}

	public removeAllRenderObjects(){
		this._render_objects = [];
		this.changed = true;
	}

	



}