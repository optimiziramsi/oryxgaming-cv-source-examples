export class RouletteGame_Utils {

	public static precise_round( num:number, decimals:number ) {
		let decimals_multiplier:number = Math.pow( 10, decimals );
		return Math.round(num * decimals_multiplier) / decimals_multiplier;
	}

}