// *******************  entities  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';
import { RouletteGame_Config as Config } from '../entities/config';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from '../interfaces/i-ui-manager';

import { RouletteGame_iPage as iPage } from '../interfaces/i-page';
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iLayers as iLayers } from '../interfaces/i-layers';
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';

export class RouletteGame_Page implements iPage {

	public am:iAssetsManager;
	public lm:iLayersManager;
	public gm:iGameManager;
	public um:iUiManager;

	private _id:string;
	private _layers:iLayers = {};

	private _x:number = 0;
	private _y:number = 0;
	private _ctxX:number = 0;
	private _ctxY:number = 0;
	private _ctxStageRatio:number = 1;

	private _visible:boolean = true;
	public changed:boolean = false;

	onAddLayer:iSignal<iLayer> = new Signal();

	constructor( page_id:string ){
		this._id = page_id;
		Config.onCtxParamsChange.add( () => this.calculateCtxParams() );
		this.calculateCtxParams();
	}

	public get id() : string {
		return this._id;
	}

	public get layers() : iLayers {
		return this._layers;
	}

	public init(){

	}

	public set x( val:number ){ if( val != this._x ){ this._x = val; this.calculateCtxParams(true); }}
	public get x():number{ return this._x; }
	
	public set y( val:number ){ if( val != this._y ){ this._y = val; this.calculateCtxParams(true); }}
	public get y():number{ return this._y; }

	public get ctxX():number{ return this._ctxX; }
	public get ctxY():number{ return this._ctxY; }

	public get visible():boolean { return this._visible; }
	public set visible(val:boolean) { if( val != this._visible ) { this._visible = val; this.changed = true; } }

	public addLayer( layer:iLayer ){
		this._layers[ layer.id ] = layer;
		layer.page = this;
		this.onAddLayer.trigger( layer );
	}

	public destroy(){
		// TODO
	}


	private calculateCtxParams( force:boolean = false ){
		if( this._ctxStageRatio == Config.ctxStageRatio && ! force ){
			return;
		}

		// v resnici računamo % za pozicijo, ker prestavljamo canvas-e
		this._ctxX = Math.floor( this._x / Config.stage_width * 100 );
		this._ctxY = Math.floor( this._y / Config.stage_height * 100 );
		this.changed = true;
	}
}