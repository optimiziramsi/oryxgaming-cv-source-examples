import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';

export class RouletteGame_Signal<T0> implements iSignal<T0> {
    private _handlers: { (data?: T0): void; }[] = [];

    public add(handler: { (data?: T0): void } ) {
        this._handlers.push(handler);
    }

    public remove(handler: { (data?: T0): void }) {
        this._handlers = this._handlers.filter(h => h !== handler);
    }

    public trigger(data?: T0) {
        this._handlers.slice(0).forEach(h => h(data));
    }

    public clear(){
        this._handlers = [];
    }
}
