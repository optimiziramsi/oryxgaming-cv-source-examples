# RouletteGame
    stvari, ki se v igri izvedejo, še preden se igra začne
    - init - lovdanje konfiguracije
        => ko je zaključeno / uspešno
            - lovdanje slik
            - povezovanje v strežnik / možgane
            - layers / loop / rendering on stage
            - Game state
            => če je vse ok / nalovdano / povezano
                __IGRA SE LAHKO ZAČNE__
            => če pade povezava / imamo težavo z assets-i
                + mora uporabnik to vedeti
        => če je napaka
            + mora uporabnik to vedeti
    - start - starts the render loop
    - stop - stops the render loop
    - destroy - destroy everything we created
    - resize - sproži notranje klice za resize

# Game (local|socket) - ui-manager dela z interface-om
    vsa komunikacija poteka preko Signal-ov
    - javlja state
        - OPEN FOR BETS
        - CLOSED FOR BETS
    - javlja izžrebano številko
    - javi, da je bila žogica sprožena
    - javlja dobitek
    - si zapomni zadnji bet / vrača zadnji bet
    - potrjuje PLACE BET / preveri, če lahko položimo bet
    - ko izgubimo povezavo / dobimo povezavo
    - ima zapisano trenutno stanje uporabnika
    - ima informacijo, koliko je trenutna vrednost stav "na mizi"

# UiManager
    PUBLIC:
    - init - inicializira vse objekte, ki jih bomo prikazovali
    - resize - poravnava objekte
    - destroy
    DESCRIPTION
    - animira objekte
    - bere / komunicira z Game-om

# ostale knjižnice
    - LayersManager
    - AssetsManager
    - RenderObject (in podknjižnice)
    - interfaces
    - enums
    - signal


# vprašanja
    - kolikšne so vrednosti chip-ov ? Kje to vrednost dobim?

# plani za naprej / dogovor 5.10.2016 Bojan, Mitja
    - Domen v tej fazi še ne pride v upoštev, zato mora igra imeti možnost "priklopa" na več tipov iger
    - v konfiguraciji moramo imeti podatke za max bet
    - naredimo igro, kot je vidno v videu, ki ga je poslal Mitja 4.10.2016
    - Andraž (designer) pripravi grafike enkrat do 11.10.2016
    - do takrat sem prejel design-e za landscape, kjer imam nujno potrebne zadeve
    - zaenkrat delamo samo za landscape, resolucijo / razmerje 1920x1080
    - podpirali bomo lokalno igranje, socketio, kasneje center, ki ga pripravi Domen