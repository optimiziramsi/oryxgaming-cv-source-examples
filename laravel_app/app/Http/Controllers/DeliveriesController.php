<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use App\Country;
use App\Address;
use App\PackageType;
use App\Partner;
use App\DeliveryPackage;
use App\Nancy\Helper;
use Response;
use Storage;

class DeliveriesController extends Controller
{
	const DELIVERY_EDIT_SESSION = 'delivery_edit_data';

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(){
		return view('pages.deliveries')
			->with('pdf_url_base', route('delivery_pdf', ['delvery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']))
			->with('pdf_download_url_base', route('delivery_pdf_download', ['delvery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']))
			->with('tracking_base_url', route('delivery_tracking', ['delvery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']))
			// ->with('countries', Country::get(1000))
			;
	}

	public function json(Request $request){
		$criteria = "";
		$fts = null;
		$search = $request->input('search');
		if(!empty($search)){
			// želimo splošen search, zato na koncu vsake besede dodamo "*" za bolj splošno iskanje
			$search_words = explode(" ", $search);
			$search_words = array_filter($search_words);    // remove empty values
			foreach ($search_words as $sw_idx => $search_word) {
				if(strpos($search_word, '*') === false){
					$search_words[$sw_idx] .= '*';
				}
			}
			$fts = implode(" ", $search_words);
		}

		// TODO - ko Piki javi API klice za datumČas, uredi spodnjo zadevo
		if(!empty($request->input('period'))){
			$date_start = '';
			$date_end = '';
			$date_format = 'm/d/Y';
			if('current_month' == $request->input('period')){
				$date_start = date($date_format, mktime(0, 0, 0, date('m'), 1, date('Y')));
				$date_end = date($date_format, mktime(0, 0, 0, date('m')+1, 1, date('Y')));
			}
			elseif('previous_month' == $request->input('period')){
				$date_start = date($date_format, mktime(0, 0, 0, date('m')-1, 1, date('Y')));
				$date_end = date($date_format, mktime(0, 0, 0, date('m'), 1, date('Y')));
			}
			else{
				$date_split = explode("|", $request->input('period'), 2);
				if(2 == $date_split){
					$date_start = date($date_format, strtotime($date_split[0]));
					$date_end = date($date_format, strtotime($date_split[1]));
				}
			}

			if(!empty($date_start) && ! empty($date_end)){
				if(!empty($cirteria)){
					$criteria .= " AND ";
				}
				$criteria .= sprintf("(datum >= #%s# AND datum < #%s#)", $date_start, $date_end);
			}
		}

		// dd($criteria);		

		if($request->input('status')){
			$filter_statuses = [];
			$statuses_available = array_keys(Delivery::statuses());
			foreach (explode(',', $request->input('status')) as $status_code) {
				if(in_array($status_code, $statuses_available)){
					$filter_statuses[] = $status_code;
				}
			}

			if(count($filter_statuses) > 0){
				if(!empty($criteria))
					$criteria .= " AND ";

				$criteria .= sprintf("statusOznaka IN ('%s')", implode("','", $filter_statuses));
			}
		}

		if($request->input('partner') && ! empty($request->input('partner'))){
			$partners = explode(',', $request->input('partner'));
			$partners = array_filter($partners);
			if(count($partners) > 0){
				if(!empty($criteria))
					$criteria .= " AND ";

				$criteria .= sprintf("partnerKontaktId IN ('%s')", implode("','", $partners));
			}
		}

		if(empty($criteria)){
			$criteria = null;
		}
		
		$sort = null;
        $sort_field = Delivery::getNancyFieldsMap($request->input('sort_by'));
        if( $sort_field ){
            $sort = $sort_field;
            if('desc' == $request->input('sort_type')){
                $sort .= ' descending';
            }
        }

		$count = Delivery::count($criteria, $fts);

		$per_page = 50;

		$page = (int)$request->input('page');
		$num_pages = ceil($count / $per_page );
		$skip = 0;
		if($page >= 1 && $page <= $num_pages){
		   // page je že v potrebnem range-u 
		} else {
			$page = 1;
		}

		$skip = ($page - 1) * $per_page;

		$data = [
			'error' => '',

			'search' => $request->input('search'),
			'status' => $request->input('status'),
			'period' => $request->input('period'),
			'partner' => $request->input('partner'),

			'sort_by' => !is_null( $request->input('sort_by') ) ? $request->input('sort_by') : "",
            'sort_type' => !is_null( $request->input('sort_type') ) ? $request->input('sort_type') : "",

			'page' => $request->input('page'),
			'pages' => $num_pages,
			'deliveries' => Delivery::get($per_page, $skip, $criteria, $sort, $fts),
		];
		return response()
			->json($data);
	}

	public function from_address(Request $request, $address_id){
		$address = Address::firstOrNull(sprintf("Id='%s'", $address_id));
		$delivery = new Delivery();
		if($address){
			$copy_fields = Address::getNancyEditableFields();
			$delivery_fields = Delivery::getNancyEditableFields();
			foreach ($copy_fields as $copy_field) {
				if(in_array($copy_field, $delivery_fields)){
					$delivery->{$copy_field} = $address->{$copy_field};
				}
			}
		}
		session([ self::DELIVERY_EDIT_SESSION => $delivery->toArray() ]);
		return redirect()->route('delivery_new');
	}

	public function edit(Request $request, $delivery_id = 'new'){
		return view('pages.delivery_edit')
			->with('ajax_json', route('delivery_json', ['delivery_id' => $delivery_id]))
			->with('addresses_json', route('deliveries_addresses_json'))
			->with('ajax_post', route('delivery_post', ['delivery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']))
			->with('delivery_id', $delivery_id)
			->with('countries', Country::get(1000))
			->with('package_types', PackageType::get(1000))
			;
	}

	public function delivery_json(Request $request, $delivery_id){
		$data = [ "error" => "", "edit_enabled" => false];
		if('new' == $delivery_id){
			$session_data = $request->session()->has(self::DELIVERY_EDIT_SESSION) ? $request->session()->get(self::DELIVERY_EDIT_SESSION) : [];
			if( ! is_array($session_data) ){
				$session_data = [];
			}
			$delivery = new Delivery($session_data);
		} else {
			$delivery_nancy_field = Delivery::getNancyFieldsMap('id');
			$delivery = Delivery::firstOrNull( sprintf("%s='%s'", $delivery_nancy_field, $delivery_id) );
		}

		if(is_null($delivery->status_code) || (is_string($delivery->status_code) && strlen($delivery->status_code) >= 2 && $delivery->status_code[1] == '0')){
			$data['edit_enabled'] = true;
		}

		if(empty($delivery->type_id)){
			$delivery->type_id = '';
		}

		if($data['edit_enabled']){
			$delivery->fillPackagesData();
			$data['delivery'] = $delivery;
		}

		return response()->json($data);
	}

	public function delivery_pdf(Request $request, $delivery_id){
		$delivery_nancy_field = Delivery::getNancyFieldsMap('id');
		$delivery = Delivery::firstOrNull( sprintf("%s='%s'", $delivery_nancy_field, $delivery_id) );

		if( $delivery ){
			$pdf_content = $delivery->pdf();
			return Response::make( $pdf_content, 200, [
				'Content-Type' => 'application/pdf',
				'Content-Disposition' => sprintf('inline; filename="%s.pdf"', $delivery->code),
			]);
		} else {
			return "File not found.";
		}
	}

	public function delivery_pdf_download(Request $request, $delivery_id){
		$delivery_nancy_field = Delivery::getNancyFieldsMap('id');
		$delivery = Delivery::firstOrNull( sprintf("%s='%s'", $delivery_nancy_field, $delivery_id) );

		if( $delivery ){
			$pdf_content = $delivery->pdf();
			return Response::make( $pdf_content, 200, [
				'Content-Type' => 'application/octet-stream',
				'Content-Disposition' => sprintf('attachment; filename="%s.pdf"', $delivery->code),
			]);
		} else {
			return "File not found.";
		}
	}

	public function delivery_pdf_viewer($delivery_id){
		return view('pages.pdf_viewer')
			->with('pdf_url', route('delivery_pdf', ['delivery_id' => $delivery_id]))
			;
	}

	public function delivery_tracking(Request $request, $delivery_id){
		$data = [ 'error' => '' ];

		$delivery_nancy_field = Delivery::getNancyFieldsMap('id');
		$delivery = Delivery::firstOrNull( sprintf("%s='%s'", $delivery_nancy_field, $delivery_id) );

		if( $delivery ){
			$trackings = $delivery->trackings();
			$data['id'] = $delivery->id;
			$data['trackings'] = $trackings;
			$data['tracking_urls'] = $this->get_tracking_urls_for_delivery( $delivery );
			$data['tracking_courier_display_name'] = $delivery->tracking_courier_display_name;
			$data['tracking_awb'] = $delivery->tracking_awb;
		} else {
			$data['error'] = "Ne najdem izbranega naročila.";
		}

		return response()->json($data);
	}

	public function addresses_json(Request $request){
		$criteria = null;   // TODO - active only
		$sort = null;   // TODO - by name
		$search = $request->input('search');

		// želimo splošen search, zato na koncu vsake besede dodamo "*" za bolj splošno iskanje
		$search_words = explode(" ", $search);
		$search_words = array_filter($search_words);    // remove empty values
		foreach ($search_words as $sw_idx => $search_word) {
			if(strpos($search_word, '*') === false){
				$search_words[$sw_idx] .= '*';
			}
		}
		$fts = implode(" ", $search_words);

		$data = [
			"error" => "",
			"search" => $search,
			"addresses" => Address::get(5, 0, $criteria, $sort, $fts),
			"copy_fields" => Address::getNancyEditableFields(),
			];
		return response()->json($data);
	}

	public function update(Request $request, $delivery_id){
		$data = [ 'error' => 'Pošiljke ni bilo mogoče posodobiti.', ];

		$action = $request->input('action');
		$is_new = ('new' == $delivery_id) ? true : false;

		switch ($action) {
			case 'update_delivery':
				$delivery =
					$is_new
					?
					Delivery::newFromInput($request->input('delivery'), [ 'partner_id' => Partner::current()->id ])
					:
					Delivery::getFromInput($request->input('delivery'))
				;
				if( ! is_null($delivery) ){
					$saved = $is_new ? $delivery->create() : $delivery->update();
					if( $saved ){
						$data['error'] = '';
						$data['services'] = $delivery->services();
						if($is_new){
							$request->session()->forget(self::DELIVERY_EDIT_SESSION);
							$data['new_id'] = $delivery->id;
							$data['new_url'] = route('delivery_edit', ['delivery_id' => $delivery->id]);
						}
					} else {
						$data['error'] = 'Pri shranjevanju je prišlo do težave. Prosimo poskusite ponovno kasneje.';
					}
				} else {
					$data['error'] = 'Ne najdem izbrane pošiljke.';
				}
				break;
			case 'update_service':
				$delivery = Delivery::firstOrNull( sprintf("Id='%s'", $request->input('delivery_id') ) );
				if( ! is_null($delivery) ){
					$services = $delivery->services();
					$service_found = false;
					foreach ($services as $service) {
						// check if this is selected service
						if( $service->available && $service->id == $request->input('service.id') && $service->pricelist_id == $request->input('service.pricelist_id') && $service->route_id == $request->input('service.route_id') ){
							$service_found = true;
							if( $service->select() ){
								$data['error'] = '';
								$data['pdf_viewer_url'] = route('delivery_pdf_viewer', ['delivery_id' => $delivery->id]);
								$data['tracking_urls'] = $this->get_tracking_urls_for_delivery( $delivery );
								$data['pdf_url_download'] = $delivery->pdfUrlDownload();
							} else {
								$data['error'] = 'Pri izbiri storitve je prišlo do napake. Prosimo poskusite ponovno.';
							}
						}
					}
					// service not found
					if( ! $service_found ){
						$data['error'] = 'Ne najdem izbrane storitve.';
					}
				} else {
					$data['error'] = 'Ne najdem izbrane pošiljke.';
				}
				break;
		}

		return response()
			->json($data);
	}

	private function get_tracking_urls_for_delivery( Delivery $delivery ){
		$languages = [
			'en' => 'angleščina',
			'sl' => 'slovenščina'
		];

		$tracking_id = base64_encode( $delivery->code );

		$urls = [];
		foreach ( $languages as $lang_code => $lang_label ) {
			$urls[] = [
				'language' => $lang_label,
				'url' => route('tracking', [ 'lang' => $lang_code, 'tracking_id' => $tracking_id ] ),
			];
		}
		return $urls;
	}
}
