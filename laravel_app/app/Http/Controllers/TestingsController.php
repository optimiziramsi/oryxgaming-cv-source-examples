<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Country;
use App\Delivery;
use App\DeliveryPackage;
use App\Partner;
use App\Offer;

class TestingsController extends Controller
{
	public function __construct(){
		$this->middleware('auth');
	}

    public function addresses(){
        return response()->json([
                'count' => Address::count(),
                'list' => Address::get(10),
            ]);
    }
    
    public function countries(){
        return response()->json([
                'count' => Country::count(),
                'list' => Country::get(1000),
            ]);
    }
    
    public function deliveries(){
        return response()->json([
                'count' => Delivery::count(),
                'list' => Delivery::get(10),
            ]);
    }
    
    public function offers(){
        return response()->json([
                'count' => Offer::count(),
                'list' => Offer::get(10),
            ]);
    }

    public function delivery_create(){
        $delivery = new Delivery();
        $delivery->partner_id = "4d27eb42-c9e4-e611-81fc-3052cb29a8ca";
        $delivery->type_id = "7dac2c34-d1e3-e611-989f-902b3435e7c8";
        $delivery->addressee_1 = "Naslovnik Testni 1";
        $delivery->addressee_2 = "Naslovnik Testni 2";
        $delivery->street_1 = "Ulica Testna 1";
        $delivery->street_2 = "Ulica Testna 2";
        $delivery->post_number = "1234";
        $delivery->city = "Testno mesto";
        $delivery->contact_name = "Testni Janez";
        $delivery->contact_phone = "041 234 567";
        $delivery->country_id = "045fed38-cae3-e611-989f-902b3435e7c8";
        $delivery->comment = "Nek komentar\nki je zapisan v več vrsticah";

        $packages = [];
        for ($i=0; $i < 4; $i++) { 
            $packages[] = [
                'weight' => '2,5',
                'width' => '34,12',
                'height' => '33,12',
                'depth' => '32,12',
            ];
        }
        $delivery->packages = $packages;

        $created = $delivery->create();

        response()->json([
                'created' => $created
            ]); 
    }

    public function delivery_services( $delivery_id ){
        $delivery = Delivery::firstOrNull( sprintf("Id='%s'", $delivery_id ) );
        if( ! $delivery ){
            return response()->json([ 'error' => sprintf( 'delivery with id "%s" not found.', $delivery_id ), ]); 
        }

        $services = $delivery->services();
        
        return response()->json([ 'services' => $services, ]);
    }

    public function delivery_tracking( $delivery_id ){
        $delivery = Delivery::firstOrNull( sprintf("Id='%s'", $delivery_id ) );
        // dd($delivery);
        if( ! $delivery ){
            return response()->json([ 'error' => sprintf( 'delivery with id "%s" not found.', $delivery_id ), ]); 
        }

        $tracking = $delivery->tracking();
        
        return response()->json([ 'tracking' => $tracking, ]);
    }

    public function packages_new($delivery_id){
        for ($i=0; $i < 4; $i++) { 
            $package = new DeliveryPackage([
                'delivery_id' => $delivery_id,
                'weight' => '2,5',
                'width' => '34,12',
                'height' => '33,12',
                'depth' => '32,12',
            ]);

            return $package->create();
        }
    }
    
    public function partners(){
        return response()->json([
        		'count' => Partner::count(),
        		'list' => Partner::get(10),
        	]);
    }

    public function partners_update(){
        $partner = Partner::current();
        $partner->email = "oranzo@gmail.com";
        $partner->password = "Test13Test13";
        $updated = $partner->update();

        return response()->json([ 'updated' => $updated, ]);
    }
}
