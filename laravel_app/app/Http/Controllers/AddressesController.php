<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\Country;

class AddressesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	return view('pages.addresses')
            ->with('countries', Country::get(1000))
            ;
    }

    public function json(Request $request){
        $criteria = null;
        $fts = null;

        if( in_array($request->input('active'), array('0', '1'))){
            $nancy_fields_map = Address::getNancyFieldsMap();
            if( isset($nancy_fields_map['active']) ){
                $criteria = sprintf('%s=%s', $nancy_fields_map['active'], $request->input('active') ? 'true' : 'false');
            }
        }

        if( ! empty($request->input('search'))){
            $fts = $request->input('search');
        }

        $sort = null;
        $sort_field = Address::getNancyFieldsMap($request->input('sort_by'));
        if( $sort_field ){
            $sort = $sort_field;
            if('desc' == $request->input('sort_type')){
                $sort .= ' descending';
            }
        }

        $count = Address::count($criteria, $fts);

        $per_page = 50;

        $page = (int)$request->input('page');
        $num_pages = ceil($count / $per_page );
        $skip = 0;
        if($page >= 1 && $page <= $num_pages){
           // page je že v potrebnem range-u 
        } else {
            $page = 1;
        }

        $skip = ($page - 1) * $per_page;

        $data = [
            'error' => '',

            'search' => !is_null( $request->input('search') ) ? $request->input('search') : "",
            'active' => !is_null( $request->input('active') ) ? $request->input('active') : "",

            'sort_by' => !is_null( $request->input('sort_by') ) ? $request->input('sort_by') : "",
            'sort_type' => !is_null( $request->input('sort_type') ) ? $request->input('sort_type') : "",

            'page' => !is_null( $request->input('page') ) ? $request->input('page') : 1,
            'pages' => $num_pages,
            'addresses' => Address::get($per_page, $skip, $criteria, $sort, $fts),
        ];

        return response()
            ->json($data);
    }

    public function update(Request $request, $address_id){
        $data = [ 'error' => 'Naslova ni bilo mogoče posodobiti.', ];

        $address_update = Address::getFromInput($request->input('address'));
        if( ! is_null($address_update) ){
            if( $address_update->update() ){
                $data['error'] = '';
            } else {
                $data['error'] = 'Pri shranjevanju je prišlo do težave. Prosimo poskusite ponovno kasneje.';
            }
        } else {
            $data['error'] = 'Manjkajoča informacija za ID.';
        }

        return response()
            ->json($data);
    }
}
