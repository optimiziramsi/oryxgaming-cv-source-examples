<?php

namespace App\Http\Controllers;

class PickupsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function index(){
        return view('pages.pickups');
    }

    public function blank(){
    	return view('pages.pickups_new');
    }
}
