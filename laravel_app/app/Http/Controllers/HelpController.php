<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\News;
use App\Partner;

class HelpController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function help_index(){
		return view('pages.help');
	}

	public function help_ajax(Request $request){
		$data = ['error' => ''];
		$type = $request->input('type');
		$page = (int)$request->input('page');

		$confirm_only = false;
		if($request->has('confirm') && filter_var( $request->input('confirm'), FILTER_VALIDATE_BOOLEAN) ){
			$confirm_only = true;
		}

		if($page <= 0){
			$page = 1;
		}

		$per_page = 10;
		$skip = ($page -1) * $per_page;

		$qry = News::where('type', $type);
		if($confirm_only){
			$qry = $qry->where('requires_confirm', 1);
		}
		if( Partner::current()->isAdmin() && $request->has('with_unpublished') && filter_var( $request->input('with_unpublished'), FILTER_VALIDATE_BOOLEAN) ){
			
		} else {
			$qry = $qry->where('published', 1);
			$qry = $qry->where('publish_date', '<=', date('Y-m-d'));
		}
		if($request->has('unread')){
			$qry = $qry->where('id', '>', (int)Partner::current()->prop($type . '_read_id', 0));
		}
		$qry_count = clone $qry;
		$news = $qry->orderBy('publish_date', 'desc')->take($per_page)->skip($skip)->get();
		$news_count = $qry_count->count();

		$num_pages = ceil($news_count / $per_page);

		if(!Partner::current()->isAdmin()){
			foreach ($news as $news_idx => $news_item) {
				$news_item->content_src = '[]';
				$news[$news_idx] = $news_item;
			}
		}

		$data['type'] = $type;
		$data['items'] = $news;
		$data['page'] = $page;
		$data['pages'] = $num_pages;

		return response()
			->json($data);
	}

	public function help_ajax_update(Request $request){
		$data = ['error' => ''];

		if(!Partner::current()->isAdmin()){
			$data['error'] = "Nimate potrebnih pravic za to dejanje.";;
			return response()
			->json($data);
		}
		
		$delete_request = $request->input('delete') ? true : false;

		$news_model;
		$news_id = $request->input('news.id');
		if('new' == $news_id){
			$news_model = new News;
			$news_model->type = $request->input('news.type');
		} else {
			$news_model = News::find($news_id);
		}

		if($delete_request){
			News::destroy($news_id);
		} else {
			$news_model->title = $request->input('news.title');
			$news_model->content = $request->input('news.content');
			$news_model->content_src = $request->input('news.content_src');
			$news_model->is_priority = filter_var( $request->input('news.is_priority'), FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
			$news_model->requires_confirm = filter_var( $request->input('news.requires_confirm'), FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
			$news_model->published = filter_var( $request->input('news.published'), FILTER_VALIDATE_BOOLEAN) ? 1 : 0;
			$news_model->publish_date = strtotime($request->input('news.publish_date')) ? date('Y-m-d H:i:s', strtotime($request->input('news.publish_date'))) : date('Y-m-d');

			$news_model->save();
		}
		
		$data['type'] = $request->input('news.type');

		return response()
			->json($data); 
	}
}
