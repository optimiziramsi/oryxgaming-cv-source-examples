<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerProperties extends Model
{
    protected $fillable = [
    	'partner_id',
	    'properties',
	    'lostpassword_token',
	    'lostpassword_created_at'
    ];

    protected $hidden = [
        'properties', 'lostpassword_token', 'lostpassword_created_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
