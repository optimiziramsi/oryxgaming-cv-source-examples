<?php

namespace App\Nancy;

use App\Nancy\NancyClient;
use Storage;
use Illuminate\Support\Facades\Log;

class Model extends \Jenssegers\Model\Model {

	const COUNT_METHOD = "NancyDbCount";
	const LIST_METHOD = "NancyDbList";
	const INSERT_METHOD = "NancyDbInsert";
	const UPDATE_METHOD = "NancyDbUpdate";
	const DELETE_METHOD = "NancyDbDelete";

	/**
	 * NancyApi model name
	 * @var string
	 */
	protected static $NancyTypeName;
	/**
	 * NancyApi sort by column / columns
	 * @var string
	 */
	protected static $NancySort;
	/**
	 * map between nancyfields and model fillable fields
	 * @var array of key value pairs / "fillable_key" => "nancy_api_field"
	 */
	protected static $NancyFillable_map;
	/**
	 * model fillable fields that we can edit / override when editing
	 * @var array of strings
	 */
	protected static $NancyEditableFields;
	/**
	 * model fields that we have to ignore from editable fields list
	 * @var array of strings
	 */
	protected static $NancyCreateIgnoreFields;
	/**
	 * model fields that are required for creating new record
	 * @var array of strings
	 */
	protected static $NancyCreateRequiredFields;

	protected static $NancyUpdateIgnoreFields;
	protected static $NancyUpdateRequiredFields;

	/**
	 * function for creating new record from model
	 * @return boolean true on success | false on any errors
	 */
	public function create(){
		// check for required fields
		$required_fields = static::$NancyCreateRequiredFields;

		$errors = [];
		if(is_array($required_fields)){
			foreach ($required_fields as $required_field) {
				if(empty( $this->{$required_field} )){
					$errors[] = sprintf("Field \"%s\" is required.", $required_field);
				}
			}
		}

		if( count($errors) > 0 ){
			dd($errors);
			// TODO - do something with errors
			return false;
		}

		$created = self::NancyInsert( $this );
		if($created){
			return $this->onCreated();
		}
		return $created;
	}

	public function update(){
		// check for required fields
		$required_fields = static::$NancyUpdateRequiredFields;

		$errors = [];
		if(is_array($required_fields)){
			foreach ($required_fields as $required_field) {
				if(empty( $this->{$required_field} )){
					$errors[] = sprintf("Field \"%s\" is required.", $required_field);
				}
			}
		}

		if( count($errors) > 0 ){
			dd($errors);
			// TODO - do something with errors
			return false;
		}

		$updated = self::NancyUpdate( $this );
		if($updated){
			return $this->onUpdated();
		}

		return $updated;
	}

	public function delete(){
		return self::NancyDelete( $this );
	}

	/**
	 * Returns count of all objects based on criteria
	 * @param  string $criteria sql like condition
	 * @param  string $fts      search parameter
	 * @return int           count of all objects or -1 on fail
	 */
	public static function count( $criteria = null, $fts = null ){
		return self::NancyGet( self::COUNT_METHOD, $criteria, $fts );
	}

	/**
	 * retrieves a list of models based on criteria
	 * @param  int  $take     number of objects to take
	 * @param  int $skip     start taking objects after index
	 * @param  string  $criteria sql like condition
	 * @param  string  $sort     sort by field / fields
	 * @param  string  $fts      searc parameter
	 * @return array of models            list of models | empty list on fail
	 */
	public static function get( $take, $skip = 0, $criteria = null, $sort = null, $fts = null ){
		return self::NancyGet( self::LIST_METHOD, $criteria, $fts, $take, $skip, $sort );
	}

	/**
	 * use laravel input as ctriteria to find a model then populate it with data from input
	 * @param  laravel input $input array
	 * @return model | null        return updated model on success or null if not found
	 */
	public static function getFromInput($input){
		if( is_array($input) && isset($input['id']) && ! empty($input['id']) ){
			$model = static::firstOrNull(sprintf("Id='%s'", $input['id']));
			if( is_null($model)){
				return null;
			}
			$model->fillFromInput($input);
			return $model;
		}

		return null;
	}

	/**
	 * use laravel input as ctriteria to find a model then populate it with data from input
	 * @param  laravel input $input array
	 * @return model | null        return updated model on success or null if not found
	 */
	public static function newFromInput($input, $init_data = []){
		if( is_array($input) ){
			$model = new static($init_data);
			$model->fillFromInput($input);
			return $model;
		}

		return null;
	}

	/**
	 * returns first model found by the criteria given or null if not found
	 * @param  string $criteria sql like condition
	 * @param  string $sort     sort by field / fields
	 * @param  string $fts      search parameter
	 * @return model | null           model on success | null on not found
	 */
	public static function firstOrNull( $criteria = null, $sort = null, $fts = null ){
		$models = static::get(1, 0, $criteria, $sort, $fts);

		if(count($models) > 0){
			return $models[0];
		}
		return null;
	}

	/*************************************************
			CALLBACKS - START
	*************************************************/
	
	/**
	 * callback when model is successfully initialized from NancyApi data
	 */
	protected function onInitializedFromNancy(){

	}

	/**
	 * callback when model is filled from input data
	 */
	protected function onFillFromInput(){

	}

	/**
	 * callback when model is successfully created with NancyApi
	 * @return boolean tells if callback was successful
	 */
	protected function onCreated(){
		// write custom handler for callbacks in your model
		return true;
	}

	/**
	 * callback when model is successfully updated with NancyApi
	 * @return boolean tells if callback was successful
	 */
	protected function onUpdated(){
		// write custom handler for callbacks in your model
		return true;
	}
	
	/*************************************************
			CALLBACKS - END
	*************************************************/



	/**
	 * when we retrieve model from input data, we first get it from NancyApi, then change / update fields
	 * from input
	 * @param  array  $attributes laravel input funtion array
	 * @return void
	 */
	public function fillFromInput(array $attributes = []){
		$tmp_fillable = $this->fillable;
		$this->fillable = static::$NancyEditableFields;
		$this->fill($attributes);
		$this->fillable = $tmp_fillable;

		$this->onFillFromInput();

		return $this;
	}



	protected static function map_data($map, $data_obj){
    	$data_mapped = [];
    	foreach ($map as $fillable_key => $data_obj_key) {
    		if(property_exists($data_obj, $data_obj_key)){
    			$data_mapped[$fillable_key] = $data_obj->{$data_obj_key};
    		}
    	}
    	return $data_mapped;
    }





    private static function NancyGet( $method, $criteria = null, $fts = null, $take = null, $skip = null, $sort = null ){
		$url = self::NancyUrl($method);
		$data = [];

		$criteria = self::criteria( $criteria );
		if(!is_null($criteria))
			$data['criteria'] = $criteria;

		if(!is_null($fts))
			$data['freeTextCriteria'] = $fts;

		if(!is_null($take))
			$data['take'] = $take;

		if(!is_null($skip))
			$data['skip'] = $skip;

		$sort = self::sort($sort);
		if(!is_null($sort))
			$data['sort'] = $sort;


		$nancy_result = NancyClient::post($url, $data);

		// var_dump( $nancy_result );
		// die();

		switch ($method) {
			case self::COUNT_METHOD:
			
				if($nancy_result->status(200) && is_numeric($nancy_result->content()))
					return (int)$nancy_result->content();
				return 0;

				break;

			case self::LIST_METHOD:

				if($nancy_result->status(200) && $nancy_result->json() && is_array($nancy_result->json())){
					$ret = [];
					foreach ($nancy_result->json() as $data_obj) {
						$model = new static(static::map_data(static::$NancyFillable_map, $data_obj));
						$model->onInitializedFromNancy();
						$ret[] = $model;
					}
					return $ret;
				}

				return [];

				break;
		}

		return null;
	}

	private static function NancyInsert( Model $model ){
		$insert_fields = self::getNancyCreateFields();

		$data = [];
		foreach ($insert_fields as $insert_field) {
			if(isset($model->{$insert_field}) && isset( static::$NancyFillable_map[$insert_field]) ){
				$nancy_field_key = static::$NancyFillable_map[$insert_field];
				$data[ $nancy_field_key ] = $model->{$insert_field};
			}
		}

		$url = self::NancyUrl( self::INSERT_METHOD );

		$nancy_result = NancyClient::post($url, $data);

		// var_dump( $nancy_result );

		// Log::debug("NancyInsert");
		// Log::debug(var_export($nancy_result, true));
		
		$created = false;
		if( $nancy_result->status(200) && ! empty( $nancy_result->json() ) ){
			$model->id = $nancy_result->json();
			$created = true;
		}

		return $created;
	}

	private static function NancyUpdate( Model $model ){
		$update_fields = self::getNancyUpdateFields();

		$data = [];
		foreach ($update_fields as $update_field) {
			if(isset($model->{$update_field}) && isset( static::$NancyFillable_map[$update_field]) ){
				$nancy_field_key = static::$NancyFillable_map[$update_field];
				$data[ $nancy_field_key ] = $model->{$update_field};
			}
		}

		$url = self::NancyUrl( self::UPDATE_METHOD );

		$nancy_result = NancyClient::post($url, $data);
		
		return ($nancy_result->status(200) && true === $nancy_result->json());
	}

	private static function NancyDelete( Model $model ){

		$data = [ 'id' => $model->id ];
		
		$url = self::NancyUrl( self::DELETE_METHOD );

		$nancy_result = NancyClient::post($url, $data);
		
		return ($nancy_result->status(200) && true === $nancy_result->json());
	}

	protected static function NancyUrl($method){
		$url_args = [
			'Nancy',
			'API',
			static::$NancyTypeName,
			$method,
		];
		$url = implode('/', array_map('urlencode', $url_args));
		return $url;
	}

	public static function getNancyFieldsMap( $model_field_key = null ){
		if( ! is_null($model_field_key) ){
			return isset(static::$NancyFillable_map[$model_field_key]) ? static::$NancyFillable_map[$model_field_key] : false;
		}
		return static::$NancyFillable_map;
	}

	public static function getNancyEditableFields(){
		return static::$NancyEditableFields;
	}

	public static function getNancyCreateFields(){
		$editable_fields = static::getNancyEditableFields();
		$ignore_fields = static::$NancyCreateIgnoreFields;
		$required_fields = static::$NancyCreateRequiredFields;

		if(is_array($required_fields)){
			$editable_fields = array_merge( $editable_fields, $required_fields );
		}

		if(is_array($ignore_fields)){
			foreach ($ignore_fields as $ignore_field) {
				while(($key = array_search($ignore_field, $editable_fields)) !== false){
					unset($editable_fields[$key]);
				}
			}
		}

		$editable_fields = array_filter($editable_fields);
		$editable_fields = array_values($editable_fields);

		return $editable_fields;
	}

	public static function getNancyUpdateFields(){
		$editable_fields = static::getNancyEditableFields();
		$required_fields = static::$NancyUpdateRequiredFields;
		$ignore_fields = static::$NancyUpdateIgnoreFields;

		if(is_array($required_fields)){
			$editable_fields = array_merge( $editable_fields, $required_fields );
		}

		if(is_array($ignore_fields)){
			foreach ($ignore_fields as $ignore_field) {
				while(($key = array_search($ignore_field, $editable_fields)) !== false){
					unset($editable_fields[$key]);
				}
			}
		}

		$editable_fields = array_filter($editable_fields);
		$editable_fields = array_values($editable_fields);

		return $editable_fields;
	}

	private static function criteria($criteria = null){
    	$str = static::defaultCriteria();
    	if(!is_null($criteria)){
    		if(null == $str){
    			return $criteria;
    		} else {
    			$str .= ' AND ('.$criteria.')';
    		}
    	}
    	return $str;
    }

    private static function sort($sort){
    	$sort_final = static::$NancySort;
    	if(!is_null($sort)){
    		$sort_final = $sort;
    	}
    	return $sort_final;
    }

    protected static function defaultCriteria(){
    	return null;
    }
}