<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	protected $casts = [
        'is_priority' => 'boolean',
        'requires_confirm' => 'boolean',
        'published' => 'boolean',
    ];

    protected $dates = [
        'publish_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
