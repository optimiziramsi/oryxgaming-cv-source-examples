<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\NancyClient;
use App\Nancy\Helper;

use App\Partner;
use App\DeliveryPackage;
use App\DeliveryService;
use App\DeliveryTracking;
use App\Service;

class Offer extends Model {

	protected static $NancyTypeName = 'XGePonudba';
	protected static $NancySort = 'datumČas descending';
	protected static $NancyFillable_map = [
		"id" => "id",
		// "company_id" => "partnerId",
		// "partner_id" => "partnerKontaktId",
		// "partner_display_name" => "partnerKontaktIme",
		"code" => "oznaka",
		"status_code" => "statusOznaka",
		"status_display_name" => "statusTekst",
		"type_id" => "vrstaId",
		"type_code" => "vrstaOznaka",
		"type_display_name" => "vrstaIme",
		"service_id" => "servisId",
		"service_code" => "servisOznaka",
		"service_display_name" => "servisIme",
		"service_price" => "cenaIzhodnaDejanska",
		"addressee_1" => "naslovnik_1",
		"addressee_2" => "naslovnik_2",
		"street_1" => "naslov_1",
		"street_2" => "naslov_2",
		"post_number" => "pošta",
		"city" => "kraj",
		"contact_name" => "ponudbaIme",
		"contact_phone" => "ponudbaTelefon",
		"contact_email" => "ponudbaEpošta",
		"country_id" => "državaId",
		"country_display_name" => "državaIme",
		"comment" => "komentar",
		"packages_count" => "paketiŠtevilo",
		"packages_weight" => "paketiTeža",
		// "packages" => "paketi",
		// "additional_assurance" => "zavarovanje",
		// "additional_assurance_package_value" => "zavarovanjeVrednost",
		// "additional_assurance_package_description" => "zavarovanjeOpis",
		"updated" => "datumČas",
		// "tracking_status" => "sledenjeStatus",
		// "tracking_updated" => "sledenjeZadnjiDogodek",		
	];
	protected static $NancyEditableFields = [
		"type_id",
		"addressee_1",
		"addressee_2",
		"street_1",
		"street_2",
		"post_number",
		"city",
		"contact_name",
		"contact_phone",
		"country_id",
		"comment",
		"packages_count",
		"packages",
		"additional_assurance",
		"additional_assurance_package_value",
		"additional_assurance_package_description",
	];

	protected static $NancyCreateRequiredFields = [
		// "partner_id",
	];

	protected static $NancyCreateIgnoreFields = [
		"packages_count",
		"packages",
		"service_id",
	];

	protected static $NancyUpdateRequiredFields = [
		"id",
	];

	protected static $NancyUpdateIgnoreFields = [
		"packages_count",
		"packages",
		"service_id",
		"additional_assurance",
		"additional_assurance_package_value",
		"additional_assurance_package_description",
	];

	protected $fillable = [
		"id",
		"company_id",
		"partner_id",
		"partner_display_name",
		"code",
		"status_code",
		"status_display_name",
		"type_id",
		"type_code",
		"type_display_name",
		"service_id",
		"service_code",
		"service_display_name",
		"service_price",
		"addressee_1",
		"addressee_2",
		"street_1",
		"street_2",
		"post_number",
		"city",
		"contact_name",
		"contact_phone",
		"country_id",
		"country_display_name",
		"comment",
		"packages_count",
		"packages_weight",
		"packages",
		"additional_assurance",
		"additional_assurance_package_value",
		"additional_assurance_package_description",
		"updated",
		"tracking_status",
    	"tracking_updated",
    	"tracking_status_display_name",
	];

	protected $hidden = [];

	protected $guarded = [];

	protected $casts = [];

	/**
	 * callback when model initialized from api data
	 */
	protected function onInitializedFromNancy(){
		// var_dump($this->tracking_status);
		$this->tracking_status_display_name = self::tracking_statuses($this->tracking_status);
	}

	protected function onFillFromInput(){
		$package_type = PackageType::firstOrNull(sprintf("Id='%s'", $this->type_id));
		
		if(!$package_type){
			return;
		}

		// create empty packages if package type does not have weight or dimentions
		if(!$package_type->has_dimentions && !$package_type->has_weight){
			// prep blank data array
			$package_blank_data = [];
			$package_editable_fields = DeliveryPackage::getNancyEditableFields();
			foreach ($package_editable_fields as $package_editable_field) {
				$package_blank_data[$package_editable_field] = 0;
			}

			$packages = [];
			$packages_count = (int)$this->packages_count > 0 ? (int)$this->packages_count : 0;
			for ($i=0; $i < $packages_count; $i++) {
				$packages[] = DeliveryPackage::newFromInput( $package_blank_data, ['delivery_id' => $this->id ] );
			}
			$this->packages_count = $packages_count;
			$this->packages = $packages;
		}
	}

	/**
	 * Callback function when we create new delivery
	 * we will use it to save packages data
	 * @return boolean success of callback function
	 */
	protected function onCreated(){
		if( is_array( $this->packages ) ){
			foreach ( $this->packages as $package_data ) {
				$package = DeliveryPackage::newFromInput( $package_data, ['delivery_id' => $this->id ] );
				if( ! $package->create() ){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * callback function where we will handle packages save
	 * @return boolean success or not
	 */
	protected function onUpdated(){
		if( is_array( $this->packages ) ){
			$current_packages = $this->getDeliveryPackages();
			$num_current_packages = count($current_packages);
			foreach ( $this->packages as $p_idx => $package_data ) {
				if(is_object($package_data) && method_exists($package_data, "toArray")){
					$package_data = $package_data->toArray();
				}
				$is_new = ($p_idx >= $num_current_packages);
				$package =
					$is_new
					?
					DeliveryPackage::newFromInput( $package_data, ['delivery_id' => $this->id ] )
					:
					$current_packages[$p_idx]->fillFromInput($package_data)
				;

				if( ! ( $is_new ? $package->create() : $package->update() ) ){
					return false;
				}
			}
			for ( $delete_idx = $p_idx +1; $delete_idx < $num_current_packages; $delete_idx++) { 
				$package = $current_packages[$delete_idx];
				if( ! $package->delete() ){
					return false;
				}
			}
		}
		return true;
	}

	public function fillPackagesData(){
		$this->packages = $this->getDeliveryPackages();
	}

	public function services(){
		$dservices = DeliveryService::services( $this->id );

		/*******************  preverimo, če potrebujemo prikazovati samo najcenejše storitve od istega ponudnika  *******************/
		// TODO - nastavitev beri iz nastavitev
		$show_only_cheapest_from_same_service = true;
		if($show_only_cheapest_from_same_service){
			$checked_ids = [];
			$filtered_services = [];
			foreach ($dservices as $s_idx => $service) {
				// preskočimo, če smo ta id že preverjali
				if(in_array($service->id, $checked_ids))
					continue;

				// nastavimo privzeti service, ki ga bomo obdržali in če najdemo cenejšega, obdržimo tistega
				$keep_service = $service;

				// gremo skozi preostale storitve, če najdemo cenejšega
				foreach ($dservices as $s_idx2 => $service2) {
					// preskočimo, če smo ta ID že preverjali
					if(in_array($service->id, $checked_ids))
						continue;
					
					// iščemo service z enakim IDjem
					if($keep_service->id == $service2->id && floatval($keep_service->price) > 0 && floatval($service2->price) > 0 && floatval($keep_service->price) > floatval($service2->price) ){
						$keep_service = $service2;
						break;
					}
				}
				
				$filtered_services[] = $keep_service;
				$checked_ids[] = $keep_service->id;
			}
			$delivery_services = $filtered_services;
		}

		/*******************  preverimo še, če prikazujemo ceno ali ne  *******************/
		$services = Service::get(100);
		foreach ($delivery_services as $ds_idx => $dservice) {
			foreach ($services as $service) {
				if($service->id == $dservice->id && !$service->show_prices && floatval($dservice->price) > 0){
					$delivery_services[$ds_idx]->price = '';
					$delivery_services[$ds_idx]->price_comment = 'Cena se obračuna po ceniku.';
					break;
				}
			}
		}

		return $delivery_services;
	}

	public function trackings(){
		return DeliveryTracking::tracking( $this->id );
	}

	public function pdfUrl(){
		return route('delivery_pdf', ['delivery_id' => $this->id]);
	}

	public function pdfUrlDownload(){
		return route('delivery_pdf_download', ['delivery_id' => $this->id]);
	}

	public function pdf(){
		$url = self::NancyUrl(static::PDF_METHOD);
        $data = [ 'id' => $this->id ];

        $nancy_result = NancyClient::post($url, $data);

        if($nancy_result->status(200)){
            return $nancy_result->content();
        }

        return null;
	}

	private static $tracking_statuses_cache_obj;
	private static $tracking_statuses_cache_arr;
	public static function tracking_statuses( $get_status ){
		if(!self::$tracking_statuses_cache_obj){
			$data_arr = [
				(object)["code" => "00", "display_name" => "Neznano"],
				(object)["code" => "05", "display_name" => "V pripravi"],
				(object)["code" => "10", "display_name" => "V tranzitu"],
				(object)["code" => "20", "display_name" => "Poteklo"],
				(object)["code" => "30", "display_name" => "Čaka prevzem"],
				(object)["code" => "35", "display_name" => "Prišlo je do zapleta"],
				(object)["code" => "40", "display_name" => "Dostavljeno"],
				(object)["code" => "50", "display_name" => "Vrnjeno"],
			];
			$data_obj =  new \stdClass;
			foreach ($data_arr as $status) {
				$data_obj->{$status->code.""} = $status->display_name;
			}
			self::$tracking_statuses_cache_arr = $data_arr;
			self::$tracking_statuses_cache_obj = $data_obj;
		}

		return property_exists(self::$tracking_statuses_cache_obj, $get_status."") ? self::$tracking_statuses_cache_obj->{$get_status.""} : null;
	}

	public static function statuses(){
		return [
			"S00" => "00 - v pripravi",
			"S10" => "10 - oddano",
			"S20" => "20 - sprejeto",
			"S30" => "30 - GE vhod",
			"S40" => "40 - GE izhod",
			"S50" => "50 - tranzit",
			"S55" => "55 - pickup",
			"S60" => "60 - dostavljeno",
			"S70" => "70 - nedostavljeno",
			"S90" => "90 - prekinjeno",
		];
	}

	private function getDeliveryPackages(){
		$delivery_nancy_field = DeliveryPackage::getNancyFieldsMap('delivery_id');
		if( $delivery_nancy_field && $this->id ){
			$packages = DeliveryPackage::get( 1000, 0, sprintf("%s='%s'", $delivery_nancy_field, $this->id) );
			return $packages;
		} else {
			return [];
		}
	}

	private static $NancyDefaultCritria = null;
	protected static function defaultCriteria(){
		// if(is_null(self::$NancyDefaultCritria)){
		// 	if(Partner::current()){
		// 		self::$NancyDefaultCritria = sprintf("partnerId='%s'", Partner::current()->company_id);
		// 	} else {
		// 		self::$NancyDefaultCritria = "1=0";
		// 	}
		// }
		return self::$NancyDefaultCritria;
	}
}