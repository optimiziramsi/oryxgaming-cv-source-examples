<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class DeliveryService extends Model {

    const SERVICES_METHOD = "NancyIzračunSpisek";
    const SERVICE_SELECT_METHOD = "NancyIzračunIzberi";

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeNaročilo';
    protected static $NancySort = null;
    protected static $NancyFillable_map = [
        "id" => "ServisId",
        "code" => "ServisOznaka",
        "display_name" => "ServisIme",
        "pricelist_id" => "CenikId",
        "route_id" => "LinijaId",
        "price" => "Cena",
        "comment" => "Opomba",
        "available" => "Uspeh",
        "eta" => "PredvidenČas",
        "image" => "Slika",
    ];

	protected $fillable = [
        "id",
        "delivery_id",
        "code",
        "display_name",
        "pricelist_id",
        "route_id",
        "price",
        "price_comment",
        "comment",
        "available",
        "eta",
        "image",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [
        'available' => 'boolean',
    ];

    protected function onInitializedFromNancy(){
        // var_dump($this->tracking_status);
        $this->tracking_status_display_name = self::tracking_statuses($this->type_id);
    }

    public static function services( $delivery_id ){
        $url = self::NancyUrl(static::SERVICES_METHOD);
        $data = [ 'id' => $delivery_id ];

        $nancy_result = NancyClient::post($url, $data);

        if($nancy_result->status(200) && $nancy_result->json() && is_array($nancy_result->json())){
            $ret = [];
            foreach ($nancy_result->json() as $data_obj) {
                $model = new static(static::map_data(static::$NancyFillable_map, $data_obj));
                $model->delivery_id = $delivery_id;
                $ret[] = $model;
            }
            return $ret;
        }

        return [];
    }

    public function select(){
        $url = self::NancyUrl(static::SERVICE_SELECT_METHOD);
        
        $required_fields = ['id', 'pricelist_id', 'route_id'];
        
        $data = [
            // we set delivery id as "id" because it is not "mapped" in services API call
            'id' => $this->delivery_id,
        ];
        foreach ($required_fields as $required_field) {
            if(isset($this->{$required_field}) && isset( static::$NancyFillable_map[$required_field]) ){
                $nancy_field_key = static::$NancyFillable_map[$required_field];
                $data[ $nancy_field_key ] = $this->{$required_field};
            }
        }

        $nancy_result = NancyClient::post($url, $data);

        return ($nancy_result->status(200) && true === $nancy_result->json());
    }
}