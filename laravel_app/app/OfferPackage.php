<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class OfferPackage extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGePonudbaPaket';
    protected static $NancySort = 'zaporednaŠtevilka';
    protected static $NancyFillable_map = [
        "id" => "id",
        "delivery_id" => "naročiloId",
        "sort_id" => "zaporednaŠtevilka",
        "weight" => "teža",
        "depth" => "dolžina",
        "width" => "širina",
        "height" => "višina",
    ];

    protected static $NancyEditableFields = [
        "weight",
        "depth",
        "width",
        "height",
    ];

    protected static $NancyCreateIgnoreFields = [];

    protected static $NancyCreateRequiredFields = [
        "delivery_id",
    ];

    protected static $NancyUpdateIgnoreFields = [
        "delivery_id",
    ];

    protected static $NancyUpdateRequiredFields = [
        "id",
    ];

	protected $fillable = [
        "id",
        "delivery_id",
        "sort_id",
        "weight",
        "depth",
        "width",
        "height",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [];
}