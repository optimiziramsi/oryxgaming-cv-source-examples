<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class Service extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeServis';
    protected static $NancySort = 'Id';
    protected static $NancyFillable_map = [
        "id" => "Id",
        "typeId" => "VrstaId",
        "code" => "Oznaka",
        "display_name" => "OznakaZaStranko",
        "description" => "Opis",
        "volume_convert" => "VolumskiPretvornik",
        "agreable" => "Dogovorni",
        "show_prices" => "PrikazCen",
        "active" => "Aktivno",
    ];

	protected $fillable = [
        "id",
        "typeId",
        "code",
        "display_name",
        "description",
        "volume_convert",
        "agreable",
        "show_prices",
        "active",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [
        "agreable" => "boolean",
        "show_prices" => "boolean",
        "active" => "boolean",
    ];
}