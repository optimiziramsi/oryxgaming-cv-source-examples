<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

use Auth;
use App\User;
use App\PartnerProperties;

class Partner extends Model {

	protected static $NancyTypeName = 'XSkPartnerKontakt';
    protected static $NancySort = "Ime";
    protected static $NancyFillable_map = [
		"id" => "Id",
		"username" => "Oznaka",
		"display_name" => "Ime",
		"email" => "Epošta",
		"password" => "Geslo",
		"company_id" => "PartnerId",
		"company_name" => "PartnerIme",
		"active" => "Aktivno",
    ];

    protected static $NancyEditableFields = [
		"email",
		"password",
    ];


	protected static $NancyUpdateIgnoreFields = [];

	protected static $NancyUpdateRequiredFields = [
		"id",
	];

	protected $fillable = [
        "id",
		"username",
		"display_name",
		"email",
		"password",
		"company_id",
		"company_name",
		"active"
	];

	protected $hidden = ["password"];

    protected $guarded = [];

    protected $casts = ["active" => "boolean"];

    private static $current = null;
    public static function current(){
    	if(is_null(self::$current) && Auth::check() && !empty(Auth::id())){
			$username = Auth::id();
        	$username = strtoupper($username);
			self::$current = Partner::firstOrNull(sprintf("Oznaka='%s'", $username ));
		}
		return self::$current;
    }

    private $is_gex = null;
    public function isAdmin(){
    	if(!is_null($this->is_gex)){
    		return $this->is_gex;
    	}
    	$this->is_gex = (!empty($this->company_id) && $this->company_id == env('ADMIN_PARTNER_ID')) ? true : false;
    	return $this->is_gex;
    }

    
    public function prop($key, $default = null){
    	$properties = $this->getProperties();
        
        if(isset($properties[$key])){
            return $properties[$key];
        }
        return $default;
    }

    public function prop_save($key, $val){
        if(is_array($val)){
            $val = array_filter($val);
            $val = array_values($val);
        }
    	$properties = $this->getProperties();
        $properties[$key] = $val;

        $this->saveProperties($properties);
    }

    private $properties = false;
    private $propertiesModel = false;
    private function getProperties(){
    	if(!empty($this->id)){
            if(!$this->properties){
                $propertiesModel = PartnerProperties::firstOrCreate(['partner_id' => $this->id]);
                $properties = (array)@json_decode($propertiesModel->properties);
                if(!is_array($properties)){
                    $properties = [];
                }
                $this->properties = $properties;
                $this->propertiesModel = $propertiesModel;
            }
            return $this->properties;
    	}
    	return [];
    }

    private function saveProperties($properties){
        if($this->propertiesModel && is_array($this->properties) && is_array($properties)){
            $this->properties = $properties;
            $this->propertiesModel->properties = json_encode($properties);
            $this->propertiesModel->save();
        }
    }

    public function generateLostPasswordToken(){
        if(!empty($this->id)){
            $token = str_random(100);
            $propertiesModel = PartnerProperties::firstOrCreate(['partner_id' => $this->id]);
            $propertiesModel->lostpassword_token = $token;
            $propertiesModel->lostpassword_created_at = time();
            $propertiesModel->save();
            return $token;    
        }
    }
}