<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class DeliveryPackage extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeNaročiloPaket';
    protected static $NancySort = 'ZaporednaŠtevilka';
    protected static $NancyFillable_map = [
        "id" => "Id",
        "delivery_id" => "NaročiloId",
        "sort_id" => "ZaporednaŠtevilka",
        "weight" => "Teža",
        "depth" => "Dolžina",
        "width" => "Širina",
        "height" => "Višina",
    ];

    protected static $NancyEditableFields = [
        "weight",
        "depth",
        "width",
        "height",
    ];

    protected static $NancyCreateIgnoreFields = [];

    protected static $NancyCreateRequiredFields = [
        "delivery_id",
    ];

    protected static $NancyUpdateIgnoreFields = [
        "delivery_id",
    ];

    protected static $NancyUpdateRequiredFields = [
        "id",
    ];

	protected $fillable = [
        "id",
        "delivery_id",
        "sort_id",
        "weight",
        "depth",
        "width",
        "height",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [];
}