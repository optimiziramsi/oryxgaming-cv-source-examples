/*************************************************
		DELIVERY EDIT - START
*************************************************/

function dee_init( ajax_json_url, ajax_addresses_url, ajax_post_url, delivery_id, package_types ){
	window.DeeVue = new Vue({
		el: dee_selector,
		data: {
			'ajax_json_url' : ajax_json_url,
			'ajax_addresses_url' : ajax_addresses_url,
			'ajax_post_url' : ajax_post_url,

			'edit_enabled': undefined,

			'loading': false,

			'tab': 0,

			'search' : '',
			'search_timeout': false,
			'search_loading': false,
			'search_has_results': false,
			'search_result': [],
			'search_result_active': undefined,
			'search_copy_fields': [],

			'ignore_tab_watch': false,

			'delivery_id': delivery_id,
			'delivery': {},
			'delivery_saving': false,
			'packages_count': 0,	// info for number of packages (from input or packages list)

			'package_types': package_types,
			'package_has_dimension': false,
			'package_has_weight': false,

			'services': [],
			'service_saving': false,
			'selected_service': undefined,

			'pdf_viewer_url': undefined,
			'pdf_url_download': '',
			'tracking_urls': [],
			'pdf_loading': false,

			'number_pattern': "[0-9]+([\.,][0-9]+)?",
			'post_number_pattern': '',
		},
		methods: {
			load_delivery: function(){
				this.loading = true;

				$.get(
					this.ajax_json_url,
					this.on_load_delivery,
					'json'
				);
			},
			on_load_delivery: function( data ){
				if(data.hasOwnProperty('edit_enabled')){
					this.edit_enabled = data.edit_enabled;
				}

				if(data.delivery){
					$(data.delivery.packages).each(function(i, v){
						var package = v;
						for(var p_key in package){
							package[p_key] = (package[p_key] + '').split(".").join(",").replace(',00', '');
						}
						data.delivery.packages[i] = package;
					});
					this.delivery = data.delivery;
					this.init_delivery();

					setTimeout(function(){guide('delivery.search_addressee');}, 800);
				}
				this.loading = false;
			},
			search_load_results: function(){
				this.search_loading = true;

				$.get(
					this.ajax_addresses_url,
					{
						search: this.search,
					},
					this.on_search_load_results,
					'json'
				);
			},
			on_search_load_results: function(data){
				if( ! this.search || this.search != data.search ){
					return;
				}

				this.search_loading = false;
				this.search_result = data.addresses;
				this.search_copy_fields = data.copy_fields;
				this.search_has_results = (this.search_result.length > 0);
			},
			select_search_item: function( delta ){
				if( ! this.search_has_results || '' == this.search && this.search_loading ){
					return;
				}

				var curr_address_index = -1;
				for (var i = 0; i < this.search_result.length; i++) {
					if(this.search_result[i] == this.search_result_active){
						curr_address_index = i;
						break;
					}
				}

				curr_address_index += delta;
				if(curr_address_index < 0){
					curr_address_index = this.search_result.length -1;
				}
				if(curr_address_index > this.search_result.length -1){
					curr_address_index = 0;
				}

				this.search_result_active = this.search_result[curr_address_index];
			},
			set_address: function(address){
				this.search = '';
				var self = this;
				$(this.search_copy_fields).each(function(i, v){
					self.delivery[v] = address[v];
				});
				this.reset_search();
				this.update_fields();

				this.check_country();

				setTimeout(function(){
					$("#delivery_addressee_1").focus();
				}, 50);
			},
			save_delivery: function(){
				// check for required fields
				var required_ok = true;
				$("#delivery-edit-vue").find("input.required, textarea.required").each(function(i ,e){
					if('' == $(e).val()){
						warning($(e), "Polje je zahtevano.");
						required_ok = false;
					}
				});
				if(!this.delivery.type_id){
					warning($(".package-type-choices"), "Izberite tip paketa.");
					required_ok = false;
				}
				// check for invalid number fields
				$("#delivery-edit-vue").find("input.invalid[pattern]").each(function(i, e){
					warning($(e), "Dovoljeni znaki so 0-9, decimalna pika ali vejica.");
					required_ok = false;
				});
				// check for invalid email fields
				$("#delivery-edit-vue").find("input.invalid[type='email']").each(function(i,e){
					warning($(e),
						'Polje "E-pošta" zahteva pravilno obliko e poštnega naslova. Primer:<br>' + 
						'"naziv@domena.si",<br>' + 
						'"ime.priimek@domena.si".<br>' + 
						'Prosimo popravite polja, označena z rdečo.'
						);
					required_ok = false;
				});
				// check country selected
				if(!this.delivery.country_id){
					warning($('.country-field'), 'Izberite državo.');
					required_ok = false;
				}

				if(!required_ok){
					return;
				}

				// clean data we send
				var delivery_data = JSON.parse(JSON.stringify(this.delivery));
				// clean empty packages
				var package_fields = ['weight','width','height','depth'];
				for (var i = delivery_data.packages.length - 1; i >= 0; i--) {
					var package = delivery_data.packages[i];
					// fix floats
					for (field_idx in package_fields) {
						var field = package_fields[field_idx];
						delivery_data.packages[i][field] = package[field].split(",").join(".");
					}
					// check for removal
					var remove_package = true;
					for (field_idx in package_fields) {
						var field = package_fields[field_idx];
						if( '' != package[field] && parseFloat(package[field]) > 0 ){
							remove_package = false;
							break;
						}
					}
					if( remove_package ){
						delivery_data.packages.splice(i, 1);
						continue;
					}
				}

				// check for empty packages / packages_count field
				if( !this.package_has_weight && !this.package_has_dimension ){
					var packages_count = parseInt( delivery_data.packages_count );
					packages_count = packages_count || 0;	// check for NaN and set 0
					if(packages_count <= 0){
						alert("Vrednost v polju za število dokumentov mora biti večja od 0.");
						return;
					}
					delivery_data.packages = [];
				}

				this.delivery_saving = true;
				$.post(
					this.ajax_post_url.replace('REPLACE_ME_WITH_DELIVERY_ID', delivery_data.id ? delivery_data.id : 'new'),
					{
						action: 'update_delivery',
						_token: token(),
						delivery: delivery_data,
					},
					this.on_save_delivery,
					"json"
				);
			},
			on_save_delivery: function(data){
				if( data.error && '' != data.error ){
					this.delivery_saving = false;
					alert(data.error);
					return;
				}

				if( data.new_id ){
					this.delivery.id = data.new_id;
					this.delivery_id = data.new_id;
				}

				if( data.new_url ){
					// window.location.href = data.redirect_to;
					replaceUrl(data.new_url);
				}

				this.delivery.service_id = '';
				this.services = data.services;
				this.selected_service = undefined;

				this.tab = 2;
				this.delivery_saving = false;
			},
			next_tab: function(){
				var self = this;
					self.tab += 1;
			},
			prev_tab: function(){
				var self = this;
				self.tab -= 1;
			},
			init_delivery: function(){
				this.check_package_type();

				var self = this;

				this.check_country();

				setTimeout(function(){
					self.tab = 1;
				}, 400);
			},
			check_country: function( skip_update_fields ){
				var self = this;

				if( ! skip_update_fields )
					this.update_fields();

				setTimeout( function(){
					$("select#edit_delivery_country").find("option").prop('selected', false);
					if(self.delivery.country_id){
						$("select#edit_delivery_country").find("option[value='"+self.delivery.country_id+"']").prop('selected', true);
						$("select#edit_delivery_country").material_select();
						$("#edit_delivery_country").closest('.select-wrapper').addClass('valid');
					}
					
				}, 20 );
			},
			check_package_type: function(){
				for (var i = this.package_types.length - 1; i >= 0; i--) {
					if( this.package_types[i].id == this.delivery.type_id ){
						this.package_has_dimension = this.package_types[i].has_dimension;
						this.package_has_weight = this.package_types[i].has_weight;

						if( ! this.package_has_dimension && ! this.package_has_weight ){
							this.delivery.packages = [];
						}
						break;
					}
				}
				this.update_fields();
				this.check_packages();
			},
			reset_packages: function(){
				this.delivery.packages = [];
			},
			check_packages: function(){
				if( ! this.package_has_dimension && ! this.package_has_weight ){
					this.delivery.packages = [];
					return;
				}

				var count_blanks = 0;
				var num_blanks_needed = 0;
				var num_packages = 0;
				if(this.package_has_dimension || this.package_has_weight){
					num_blanks_needed = 3;
				}
				if(!this.delivery.packages){
					this.delivery.packages = [];
				}
				for (var i = 0; i < this.delivery.packages.length; i++) {
					var package = this.delivery.packages[i];
					if( this.is_blank_package(package) ){
						count_blanks += 1;
						package.required = false;
						this.set_blank_package(package);
					} else {
						count_blanks = 0;
						package.required = true;
						num_packages++;
					}

					this.delivery.packages[i] = package;
				}

				this.packages_count = num_packages;

				if( count_blanks < num_blanks_needed ){
					for (var i = (num_blanks_needed - count_blanks) - 1; i >= 0; i--) {
						this.delivery.packages.push({'weight':'', 'depth': '', 'width':'', 'height':''});
						count_blanks++;
					}
				}

				if( count_blanks > num_blanks_needed ){
					for (var i = (count_blanks - num_blanks_needed) - 1; i >= 0; i--) {
						this.delivery.packages.pop();
						count_blanks--;
					}
				}

				if(num_blanks_needed > 0 && this.delivery.packages.length == num_blanks_needed){
					this.delivery.packages[0].required = true;
				}
				
				for (var i = 0; i < this.delivery.packages.length; i++) {
					if(this.delivery.packages[i].required){
						// TODO
					}
				}
			},
			is_blank_package: function(package){
				var package2 = JSON.parse( JSON.stringify(package) );
				var fields = ['weight', 'depth', 'width', 'height'];
				for(var i in fields){
					var field_int_val = parseFloat( package2[fields[i]].split(",").join(".") ) || 0;
					if( field_int_val > 0 ){
						return false;
					}
				}
				return true;
			},
			set_blank_package: function( package ){
				var fields = ['weight', 'depth', 'width', 'height'];
				for(var i in fields){
					package[fields[i]] = '';
				}
			},
			set_service: function( service ){
				if( this.service_saving || !service.available ){
					return;
				}
				this.selected_service = service;
			},
			save_service: function(){
				if(!this.selected_service){
					alert("Izberite storitev, s katero želite poslati paket.");
					return;
				}

				var service_data = {
					'id': this.selected_service.id,
					'pricelist_id': this.selected_service.pricelist_id,
					'route_id': this.selected_service.route_id,
				};

				this.service_saving = true;
				$.post(
					this.ajax_post_url.replace('REPLACE_ME_WITH_DELIVERY_ID', this.delivery.id),
					{
						action: 'update_service',
						_token: token(),
						delivery_id: this.delivery_id,
						service: service_data,
					},
					this.on_save_service,
					"json"
				);

			},
			on_save_service: function(data){
				if( data.error && '' != data.error ){
					this.service_saving = false;
					alert(data.error);
					return;
				}

				this.tab = 3;
				this.service_saving = false;

				this.pdf_loading = true;
				this.pdf_viewer_url = data.pdf_viewer_url;
				this.pdf_url_download = data.pdf_url_download;
				this.tracking_urls = data.tracking_urls;

				guide('delivery.in_history', 800);
				guide('delivery.can_download_pdf', 1200);
			},
			reset_search: function(){
				this.search='';
				$("label[for='edit_delivery_search_address']").removeClass('active');
				this.scroll_to_top();
			},
			scroll_to_top: function(){
				$('html, body').animate({ scrollTop: 0 }, {duration: 400, queue: false, easing: 'easeOutCubic'});
			},
			next_input: function(){
				var $focused = $(":focus");
				if(!$focused.length || 'TEXTAREA2' == $focused.prop("tagName")){
					return;
				}
				var $inputs = $('input, textarea');
				var nextInput = $inputs.get($inputs.index($focused) + 1);
				if (nextInput) {
					nextInput.focus();
					if($(nextInput).is("input[type=radio], input[type=checkbox]")){
						$(nextInput).trigger({
						    type: 'keyup',
						    which: 9
						});
					}
		       }
			},
			update_fields: function(){
				setTimeout(function(){
					if(Materialize.updateTextFields){
						Materialize.updateTextFields();
					}
				}, 60);
			},
		},
		watch: {
			'delivery_saving': function(){
				if(this.delivery_saving){
					this.search = '';
				}
				var self = this;
				setTimeout( function(){
					self.check_country( true );
				}, 40 );
			},
			'delivery.type_id': function(val, prev_val){
				// če smo tip paketa nastavljali prvič (bo PREV_VAL prazen), ne smemo pobrisati paketov, ki so trenutno prisotni.
				// reset naredimo samo v primeru, da je uporabnik sam izbral drug tip pošiljke
				if(prev_val){
					this.delivery.packages = [];
				}
				this.check_package_type();
			},
			search: function(){
				clearTimeout( this.search_timeout );
				if('' != this.search){
					this.search_loading = true;
					this.search_timeout = setTimeout(this.search_load_results, 0.6 * 1000);
				}
			},
			tab: function(){
				var self = this;
				$("#delivery-edit-vue .collapsible").data('disabled', false);
				setTimeout(function(){
					$("#delivery-edit-vue .collapsible-header").eq(self.tab - 1).click();
					self.scroll_to_top();
				}, 20);
				setTimeout(function(){
					$("#delivery-edit-vue .collapsible").data('disabled', true);
				}, 200);
			},
			post_number_pattern: function(){
				this.update_fields();
			},
		},
		mounted: function(){
			var self = this;
			this.$on('edit_delivery_country_changed', function(val, text, post_number_pattern){
				self.post_number_pattern = post_number_pattern;
				self.delivery.country_id = val;
				self.delivery.country_display_name = text;

				self.check_country();
			});

			$("#delivery-edit-vue .collapsible").data('disabled', false);

			this.load_delivery();

			var return_key_listeners = 'input'; // 'input, textarea';
			$(document).on('keydown', return_key_listeners, function(e){
				// enter pressed
				if (e.which == 13) {
					self.next_input();
				}
			});

			$(dee_selector).show();
		},
	});
}

/*************************************************
		DELIVERIES NEW - END
*************************************************/