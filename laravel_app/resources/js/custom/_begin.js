(function($){
	var dee_selector = "#delivery-edit-vue";
	var de_selector = "#deliveries-vue";
	var ad_selector = "#addresses-vue";
	var he_selector = "#help-vue";
	var of_selector = "#offers-vue";

	var _csrf_token = false;

	function init(){
		// date helper
		Date.prototype.yyyymmdd = function() {
			var mm = this.getMonth() + 1; // getMonth() is zero-based
			var dd = this.getDate();

			return [this.getFullYear(),
			(mm>9 ? '' : '0') + mm,
			(dd>9 ? '' : '0') + dd
			].join('-');
		};

		// token updater and global var
		_csrf_token = $('meta[name="csrf-token"]').attr('content');
		setInterval(token_refresh, 3600000);

		vue_init();

		if(1 == $(ad_selector).length){
			ad_init(
				$(ad_selector).attr('data-ajax-url'),
				$(ad_selector).attr('data-update-url'),
				$(ad_selector).attr('data-delivery-url')
			);
		}

		if(1 == $(de_selector).length){
			de_init(
				$(de_selector).attr('data-ajax-url'),
				$(de_selector).attr('data-edit-url'),
				$(de_selector).attr('data-pdf-url-base'),
				$(de_selector).attr('data-pdf-download-url-base'),
				$(de_selector).attr('data-tracking-url-base')
				);
		}

		if(1 == $(dee_selector).length){
			dee_init(
				$(dee_selector).attr('data-ajax-url'),
				$(dee_selector).attr('data-addresses-url'),
				$(dee_selector).attr('data-ajax-post-url'),
				$(dee_selector).attr('data-delivery-id'),
				$(dee_selector).data('package-types')
			);
		}

		if(1 == $('#login-vue').length){
			li_init();
		}

		if(1 == $('#profile-vue').length){
			pr_init(
				$('#profile-vue').attr('data-edit-url'),
				$('#profile-vue').attr('data-email')
			);
		}

		if(1 == $(he_selector).length){
			h_init( $(he_selector).data() );
		}

		if(1 == $(of_selector).length){
			offers_init( $(of_selector).data() );
		}

		if($("#latest-news").length > 0){
			ln_init($("#latest-news").data());
		}

		select_init();
		$('.modal').modal({
			dismissible: false,
		});
		tooltip_init();
		$(".button-collapse").sideNav();

		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 2 // Creates a dropdown of 15 years to control year
		});
	}

	function token(){
		// console.log("T", _csrf_token);
		return _csrf_token;
	}
	function token_refresh(){
		var refresh_url = $('meta[name="refresh-csrf-token-url"]').attr('content');
		$.get(refresh_url).done(function(token){
			_csrf_token = token;
			$('[name="csrf-token"]').attr('content', _csrf_token);
			$("input[name='_token']").val(_csrf_token);
		});
	}

	function select_init(){
		$("select").material_select();
	}
	function tooltip_init(){
		$('.tooltipped').tooltip({delay: 50, position: 'top'});
	}

	function replaceUrl(new_url){
		if ("replaceState" in history)
        	history.replaceState("", document.title, new_url);
	}