<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="{{ url('js/materializecss.js') }}?v={{ env('APP_VERSION') }}"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.1.8/dist/vue.js"></script>
<script type="text/javascript" src="{{ url('js/pdfobject.min.js') }}?v={{ env('APP_VERSION') }}"></script>
<script type="text/javascript" src="{{ url('js/custom.js') }}?v={{ env('APP_VERSION') }}"></script>