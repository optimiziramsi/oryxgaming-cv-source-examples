@extends('layouts.full_width')
@section('content')
<div class="row">
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<h1 class="center-align">Ups, tole pa je neprijetno</h1>
		<p class="center-align">Trenutno zaznavamo težave v komunikaciji s sistemom Global Express.</p>
		<p class="center-align">
			Prosimo poskusite ponovno čez nekaj trenutkov.
		</p>
		<p class="center-align">
			<a href="{{ route('delivery_new') }}" class="btn red">Pojdi Domov</a>
		</p>
	</div>
</div>
@stop