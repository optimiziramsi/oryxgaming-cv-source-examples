@extends('layouts.full_width')
@section('content')
<div id="profile-vue" class="row" data-edit-url="{{ route('profile') }}" data-email="{{ $email }}">
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m6 offset-m3 l4 offset-l4">
		<div class="row">
			<div class="col s12">
				<h3>{{ trans('Email za ponastavitev gesla') }}</h3>
				<p>V primeru, da pozabite geslo za vstop v E-tovorni list, boste lahko sami zahtevali novno geslo.</p>
			</div>
			<div class="input-field col s12">
				<input id="email" type="email" v-model="email" class="validate" :disabled="email_updating">
				<label for="email">{{ trans('Email') }}</label>
			</div>
			<div class="col s12">
				<span class="green-text" v-show="email_updated"><strong>Posodobljeno!</strong></span>
				<button class="btn waves-effect waves-light right" @click="update_email" :disabled="email_updating">{{ trans('Posodobite email.') }}<i class="material-icons left">save</i></button>
				<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="email_updating">
					<div class="spinner-layer spinner-red-only">
						<div class="circle-clipper left"><div class="circle"></div></div>
						<div class="gap-patch"><div class="circle"></div></div>
						<div class="circle-clipper right"><div class="circle"></div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m6 offset-m3 l4 offset-l4">
		<div class="row">
			<div class="col s12">
				<h3>{{ trans('Posodobitev gesla') }}</h3>
			</div>
			<div class="input-field col s12">
				<input id="old_password" type="password" v-model="old_password" class="validate" :class="{ required: password_required }" :disabled="password_updating">
				<label for="old_password">{{ trans('Trenutno geslo')}}</label>
			</div>
			<div class="input-field col s12">
				<input id="new_password" type="password" v-model="new_password" class="validate" :class="{ required: password_required }" :disabled="password_updating">
				<label for="new_password">{{ trans('Novo geslo')}}</label>
			</div>
			<div class="input-field col s12">
				<input id="new_password2" type="password" v-model="new_password2" class="validate" :class="{ required: password_required }" :disabled="password_updating">
				<label for="new_password2">{{ trans('Ponovno vpišite novo geslo')}}</label>
			</div>
			<div class="col s12">
				<span class="green-text" v-show="password_updated"><strong>Posodobljeno!</strong></span>
				<button class="btn waves-effect waves-light right" @click="update_password" :disabled="password_updating">{{ trans('Posodobite geslo') }}<i class="material-icons left">save</i></button>
				<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="password_updating">
					<div class="spinner-layer spinner-red-only">
						<div class="circle-clipper left"><div class="circle"></div></div>
						<div class="gap-patch"><div class="circle"></div></div>
						<div class="circle-clipper right"><div class="circle"></div></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop