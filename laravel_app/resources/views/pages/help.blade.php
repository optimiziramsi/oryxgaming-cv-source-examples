@extends('layouts.full_width')
@section('content')
<div id="help-vue" class="row" data-ajax-url="{{ route('help_ajax') }}" data-ajax-update-url="{{ route('help_ajax_update') }}" data-is-admin="{{ App\Partner::current()->isAdmin() }}" style="display:none;">
	<div class="col s12 hide-on-med-and-up">
		{{-- TODO - selector za prikaz HELP ali NOVICE --}}
	</div>
	<div class="col s12 m6 l6">
		<div class="row">
			<div class="col s12">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
					<h2>Pomoč</h2>
					<p>Če v spodnjih navodilih ne najdete želene informacije, se prosimo obrnite na enega izmed spodnjih kontaktov:</p>
					<p>
						@if (App\Partner::current()->isAdmin())
						<div class="btn-floating red right hide-on-med-and-down" @click="edit_help_new('help')" data-guide="help.add_item" data-guide-position="top"><i class="material-icons">add</i></div>
						@endif
						<a href="tel:+38651332913" class="btn red"><i class="material-icons left">phone</i>041 877 002 [ Miha Puželj ]</a><br><br>
						<a href="mailto:miha.puzelj@global-express.si" class="btn small red"><i class="material-icons left">email</i>miha.puzelj@global-express.si</a>
					</p>
				</div>
			</div>
			<div class="col s12" v-if="isAdmin">
				<p class="right-align">
			      <input type="checkbox" id="with_unpublished_help" v-model="help.with_unpublished" class="filled-in" />
			      <label for="with_unpublished_help" data-guide="help.only_admin_see" data-guide-position="top">Prikaži tudi neobjavljene</label>
			    </p>
			</div>
			<div class="col s12" v-if="help.loading">
				<div class="progress">
					<div class="indeterminate"></div>
				</div>
			</div>
			<ul class="pagination col s12" v-show="help.pages > 1 && !help.loading">
				<li v-bind:class="{ disabled: help.page == 1 }" v-bind:class="{ waves-effect: help.page != 1 }"><a href="#!" @click="prev_page('help')"><i class="material-icons">chevron_left</i></a></li>
				<li v-for="page_index in help.pages" v-bind:class="{ active: help.page == page_index }" v-bind:class="{ waves-effect: help.page != page_index }">
					<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index,'help')">@{{ page_index }}</a>
				</li>
				<li v-bind:class="{ disabled: help.page == help.pages }" v-bind:class="{ waves-effect: help.page != help.pages }"><a href="#!" @click="next_page('help')"><i class="material-icons">chevron_right</i></a></li>
			</ul>
			<div class="col s12" v-for="item in help.items" v-show="!help.loading">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel" :style="{ opacity: (!item.published || is_draft_calc(item.publish_date)) ? 0.7 : 1, }">
					<a href="#delete" @click.prevent="delete_item(item)" class="btn red right tooltipped hide-on-med-and-down" data-tooltip="izbriši objavo" v-if="isAdmin"><i class="material-icons">delete</i></a>
					<a href="#edit" @click.prevent="edit_help(item)" class="btn red right tooltipped hide-on-med-and-down" data-tooltip="uredi objavo" v-if="isAdmin"><i class="material-icons">edit</i></a>
					<h4 v-if="!item.published">[osnutek]</h4>
					<div v-html="item.content"></div>
				</div>
			</div>
			<ul class="pagination col s12" v-show="help.pages > 1 && !help.loading">
				<li v-bind:class="{ disabled: help.page == 1 }" v-bind:class="{ waves-effect: help.page != 1 }"><a href="#!" @click="prev_page('help')"><i class="material-icons">chevron_left</i></a></li>
				<li v-for="page_index in help.pages" v-bind:class="{ active: help.page == page_index }" v-bind:class="{ waves-effect: help.page != page_index }">
					<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index,'help')">@{{ page_index }}</a>
				</li>
				<li v-bind:class="{ disabled: help.page == help.pages }" v-bind:class="{ waves-effect: help.page != help.pages }"><a href="#!" @click="next_page('help')"><i class="material-icons">chevron_right</i></a></li>
			</ul>
			<div class="col s12" v-if="0 == help.items.length">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
					<p>Ni vnosov</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col s12 m6 l6">
		<div class="row">
			<div class="col s12">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
					@if (App\Partner::current()->isAdmin())
					<div class="btn-floating red right hide-on-med-and-down" @click="edit_help_new('news')" data-guide="help.add_item" data-guide-position="top"><i class="material-icons">add</i></div>
					@endif
					<h2>Novice</h2>
				</div>
			</div>
			<div class="col s12" v-if="isAdmin">
				<p class="right-align">
			      <input type="checkbox" id="with_unpublished_news" v-model="news.with_unpublished" class="filled-in"/>
			      <label for="with_unpublished_news" data-guide="help.only_admin_see" data-guide-position="top">Prikaži tudi neobjavljene</label>
			    </p>
			</div>
			<div class="col s12" v-if="news.loading">
				<div class="progress">
					<div class="indeterminate"></div>
				</div>
			</div>
			<ul class="pagination col s12" v-show="news.pages > 1 && !news.loading">
				<li v-bind:class="{ disabled: news.page == 1 }" v-bind:class="{ waves-effect: news.page != 1 }"><a href="#!" @click="prev_page('news')"><i class="material-icons">chevron_left</i></a></li>
				<li v-for="page_index in news.pages" v-bind:class="{ active: news.page == page_index }" v-bind:class="{ waves-effect: news.page != page_index }">
					<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index,'news')">@{{ page_index }}</a>
				</li>
				<li v-bind:class="{ disabled: news.page == news.pages }" v-bind:class="{ waves-effect: news.page != news.pages }"><a href="#!" @click="next_page('news')"><i class="material-icons">chevron_right</i></a></li>
			</ul>
			<div class="col s12" v-for="item in news.items" v-show="!news.loading">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel" :style="{ opacity: (!item.published || is_draft_calc(item.publish_date)) ? 0.7 : 1, }">
					<a href="#delete" @click.prevent="delete_item(item)" class="btn red right tooltipped hide-on-med-and-down" data-tooltip="izbriši objavo" v-if="isAdmin"><i class="material-icons">delete</i></a>
					<a href="#edit" @click.prevent="edit_help(item)" class="btn red right tooltipped hide-on-med-and-down" data-tooltip="uredi objavo" v-if="isAdmin"><i class="material-icons">edit</i></a>
					<h4 v-if="!item.published">[osnutek]</h4>
					<div v-html="item.content"></div>
				</div>
			</div>
			<ul class="pagination col s12" v-show="news.pages > 1 && !news.loading">
				<li v-bind:class="{ disabled: news.page == 1 }" v-bind:class="{ waves-effect: news.page != 1 }"><a href="#!" @click="prev_page('news')"><i class="material-icons">chevron_left</i></a></li>
				<li v-for="page_index in news.pages" v-bind:class="{ active: news.page == page_index }" v-bind:class="{ waves-effect: news.page != page_index }">
					<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index,'news')">@{{ page_index }}</a>
				</li>
				<li v-bind:class="{ disabled: news.page == news.pages }" v-bind:class="{ waves-effect: news.page != news.pages }"><a href="#!" @click="next_page('news')"><i class="material-icons">chevron_right</i></a></li>
			</ul>
			<div class="col s12" v-if="0 == news.items.length">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
					<p>Ni vnosov</p>
				</div>
			</div>
		</div>
	</div>
	@if(App\Partner::current()->isAdmin())
	<div id="edit_help_modal" class="modal modal-fixed-footer" data-dismissible="false">
		<div class="modal-content" v-show="!edit.saving">
			<div class="row section">
				<div class="col s6">
					<h3>Urejanje</h3>
					<div class="row">
						<div class="col s12">
							<div class="switch">
								<label>
									Kot osnutek
									<input type="checkbox" v-model="edit.published">
									<span class="lever"></span>
									Objavljeno
								</label>
							</div>
						</div>
						<div class="col s12" v-if="'news' == edit.type">
							<div class="switch">
								<label>
									Navadna novica
									<input type="checkbox" v-model="edit.is_priority">
									<span class="lever"></span>
									Prioritetna novica
								</label>
							</div>
						</div>
						<div class="col s12" v-if="'news' == edit.type">
							<div class="switch">
								<label>
									Ne zahtevaj potrditve
									<input type="checkbox" v-model="edit.requires_confirm">
									<span class="lever"></span>
									Zahtevaj potrditev
								</label>
							</div>
						</div>
						<div class="input-field col s12" style="margin-top: 2rem;">
							<input id="edit_publish_date" type="text" v-model="edit.publish_date" pattern="\d{4}-\d{2}-\d{2}" placeholder="yyyy-mm-dd" class="validate">
							<label for="edit_publish_date">Datum objave [zapiši v obliki yyyy-mm-dd | za 1. marec 2017 vpiši "2017-03-01"]</label>
						</div>
					</div>
					<h4>Naslov</h4>
					<div class="row">
						<div class="input-field col s12">
							<input id="edit_title" type="text" v-model="edit.title">
							<label for="edit_title">Naslov</label>
						</div>
					</div>
					<h4>Vsebina</h4>
					<div class="row" v-for="section, section_idx in edit.content_src">
						<div class="input-field col s11" v-if="section.type=='text'">
							<h4>Tekst</h4>
							<textarea class="materialize-textarea" v-model="section.content"></textarea>
						</div>
						<div class="file-field input-field col s11" v-if="section.type=='image'">
							<h4>Slika</h4>
							<div class="btn red">
								<span><i class="material-icons left">insert_photo</i>Brskaj</span>
								<input type="file" @change="edit_image_selected($event, section_idx)">
							</div>
							<div class="right" v-if="'' != section.content">
								<img :src="section.content ? section.content : undefined" style="max-height: 100px;">
							</div>
						</div>
						<div class="input-field col s11" v-if="section.type=='video'">
							<h4>YouTube video</h4>
							<input type="text" v-model="section.content" placeholder="youtube video url">
						</div>
						<div class="col s1">
							<a class="btn-floating btn-small waves-effect waves-light grey" @click="edit_sort(section_idx, -1)"><i class="material-icons">arrow_drop_up</i></a><br>
							<a class="btn-floating btn-small waves-effect waves-light grey" @click="edit_remove(section_idx)"><i class="material-icons">delete</i></a><br>
							<a class="btn-floating btn-small waves-effect waves-light grey" @click="edit_sort(section_idx, 1)"><i class="material-icons">arrow_drop_down</i></a>
						</div>
					</div>
					<div class="row">
						<div class="col s12 right-align">
							<a class="waves-effect waves-light btn red" @click.prevent="edit_add('text')"><i class="material-icons left">add</i>Tekst</a>
							<a class="waves-effect waves-light btn red" @click.prevent="edit_add('image')"><i class="material-icons left">add</i>Slika</a>
							<a class="waves-effect waves-light btn red" @click.prevent="edit_add('video')"><i class="material-icons left">add</i>YT Video</a>
						</div>
					</div>
				</div>
				<div class="col s6">
					<h3>Predogled vsebine</h3>
					<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
						<div id="edit_help_preview">
							<h3>@{{edit.title?edit.title:'[ naslov je prazen ]'}} <small v-if="edit.type=='news' && edit.publish_date" class="right">Objavljeno: @{{ edit.publish_date | humanDate2 }}</small></h3>
							<div v-for="section in edit.content_src">
								<pre class="p" v-if="section.type == 'text'">@{{section.content }}</pre>
								<div class="video-container" v-if="section.type == 'video'">
									<iframe width="853" height="480" :src="section.content ? youtubeEmberUrl(section.content) : undefined" frameborder="0" allowfullscreen></iframe>
								</div>
								<p v-if="section.type == 'image' && '' != section.content">
									<img :src="section.content" style="width: 100%;">
								</p>
							</div>
						</div>
						<pre class="p" v-if="!edit.content_src || 0 == edit.content_src.length">[ Vsebina je prazna ]</pre>
						<div style="overflow: hidden;">
							<a href="#" class="btn green right" v-if="edit.requires_confirm">Sprejemam</a>
							<a href="#" class="btn-flat right" v-if="edit.requires_confirm">Opomni me kasneje</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-content" v-show="edit.saving">
			<h4>Shranjujem, prosimo počakajte ...</h4>
		</div>
		<div class="modal-footer" v-show="!edit.saving">
			<a href="#save" class="modal-action waves-effect waves-green btn green" style="margin-left: 5px;" @click.stop.prevent="edit_save">Shrani</a>
			<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="edit_cancel">Prekliči</a>
		</div>
		<div class="modal-footer" v-show="edit.saving">
			<div class="progress">
				<div class="indeterminate"></div>
			</div>
		</div>
	</div>
	@endif
</div>
@stop