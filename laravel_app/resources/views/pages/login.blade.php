@extends('layouts.full_width')
@section('content')
<div id="login-vue" class="row">
	@if(!empty($errors->first()))
	<div class="card-panel red lighten-1 z-depth-1 col s12 m6 offset-m3 l4 offset-l4">
		<p class="white-text">{{ $errors->first() }}</p>
	</div>
	@endif
	<form class="card-panel grey lighten-5 z-depth-1 col s12 m6 offset-m3 l4 offset-l4" action="{{ asset('/login') }}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="request_login" value="yes">
		<div class="row">
			<div class="col s12">
				<h3>{{ trans('Prijava') }}</h3>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="username" type="text" name="username" v-model="username">
				<label for="username">{{ trans('Uporabniško ime')}}</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" type="password" name="password" v-model="password">
				<label for="password">{{ trans('Geslo') }}</label>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<button class="btn waves-effect waves-light" type="submit" name="action">{{ trans('Prijavite se') }}<i class="material-icons right">send</i></button>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<a href="{{ route('forgotpassword') }}">{{ trans('Ste pozabili svoje geslo?') }}</a>
			</div>
		</div>
	</form>
</div>
@stop