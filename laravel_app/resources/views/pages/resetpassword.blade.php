@extends('layouts.full_width')
@section('content')
<div class="row">
	@if (session('success'))
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel green white-text z-depth-5">
			<p>Uspešno ste si nastavili novo geslo.</p>
			<p>
				<a href="{{ route('login') }}" class="btn red">Vpiši se v E-tovorni</a>
			</p>
		</div>
	</div>
	@elseif(isset($token_error) && 'token_not_found' == $token_error)
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel grey lighten-4 z-depth-5">
			<h3>Opozorilo</h3>
			<p>Povezava za ponastavitev gesla ni veljavna.</p>
			<p>Če želite ponastaviti geslo, ponovno zahtevajte ponastavitev.</p>
			<p><a href="{{ route('forgotpassword') }}">{{ trans('Ponastavi svoje geslo') }}</a></p>
		</div>
	</div>
	@elseif(isset($token_error) && 'not_valid_timespan' == $token_error)
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel grey lighten-4 z-depth-5">
			<h3>Veljavnost povezave je potekla</h3>
			<p>Povezava za ponastavitev gesla je zaradi varnosti veljavna 30 min.</p>
			<p>Če želite ponastaviti geslo, ponovno zahtevajte ponastavitev.</p>
			<p><a href="{{ route('forgotpassword') }}">{{ trans('Ponastavi svoje geslo') }}</a></p>
		</div>
	</div>
	@elseif(isset($token_error) && 'partner_not_found' == $token_error)
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel grey lighten-4 z-depth-5">
			<h3>Opozorilo</h3>
			<p>Prišlo je do neznane napake pri pridobivanju vaših podatkov. Prosimo osvežite stran ali poskusite ponovno kasneje.</p>
			<p>Če se vam ta napaka ponavlja, prosimo kontaktirajte Global Express</p>
			<p>
				<a href="tel:+38651332913" class="btn red"><i class="material-icons left">phone</i>041 877 002 [ Miha Puželj ]</a><br><br>
				<a href="mailto:miha.puzelj@global-express.si" class="btn small red"><i class="material-icons left">email</i>miha.puzelj@global-express.si</a>
			</p>
		</div>
	</div>
	@elseif(isset($token_error) && ! empty($token_error))
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel grey lighten-4 z-depth-5">
			<h3>Napaka</h3>
			<p>Prišlo je do neznane napake.</p>
			<p>Če želite ponastaviti geslo, ponovno zahtevajte ponastavitev.</p>
			<p><a href="{{ route('forgotpassword') }}">{{ trans('Ponastavi svoje geslo') }}</a></p>
		</div>
	</div>
	@else
	<form class="col s12 m6 offset-m3 l4 offset-l4" action="" method="post">
		{{ csrf_field() }}
		@if (session('error'))
		<div class="row card-panel red lighten-1 z-depth-5 white-text">
			<div class="col s12">
				<h3>Opozorilo</h3>
				<p>{{ session('error') }}</p>
			</div>
		</div>
		@endif
		<div class="row card-panel grey lighten-5 z-depth-1">
			<div class="col s12">
				<h3>{{ trans('Nastavi novo geslo') }}</h3>
				<p>Pozdravljeni {{$partner->display_name}},<br>prosimo nastavite si novo geslo.</p>
			</div>
			<div class="input-field col s12">
				<input id="new_password" name="new_password" type="password" class="validate required">
				<label for="new_password">{{ trans('Novo geslo')}}</label>
			</div>
			<div class="input-field col s12">
				<input id="new_password2" name="new_password2" type="password" class="validate required">
				<label for="new_password2">{{ trans('Ponovno vpišite novo geslo')}}</label>
			</div>
			<div class="col s12">
				<button class="btn waves-effect waves-light right">{{ trans('Posodobi geslo') }}<i class="material-icons left">save</i></button>
			</div>
		</div>
	</form>
	@endif
</div>
@stop