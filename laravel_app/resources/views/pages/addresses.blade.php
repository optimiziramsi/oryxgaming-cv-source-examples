@extends('layouts.full_width')
@section('content')
<div id="addresses-vue" data-ajax-url="{{ url('/addresses/json') }}" data-update-url="{{ url('/addresses') }}" data-delivery-url="{{ url('/deliveries/from_address/{address_id}') }}" style="display:none;">
	<div class="row">
		<div class="input-field col s12 l9" data-guide="addresses.search" data-guide-position="bottom">
			<input id="search_address" type="search" v-model="search">
			<label for="search_address"><i class="material-icons">search</i> išči naslovnika</label>
			<i class="material-icons" @click="reset_search">close</i>
		</div>
		<div class="input-field col s12 l3" data-guide="addresses.filter" data-guide-position="bottom">
			<select onchange="AdVue.$emit('select_active_changed', $(this).val());">
				<option value="">Aktivni in neaktivni naslovniki</option>
				<option value="1">Samo aktivni naslovniki</option>
				<option value="0">Samo neaktivni naslovniki</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="progress" v-show="loading">
				<div class="indeterminate"></div>
			</div>
		</div>
		<ul class="pagination" v-show="pages > 1 && !loading">
			<li v-bind:class="{ disabled: page == 1 }" v-bind:class="{ waves-effect: page != 1 }"><a href="#!" @click="prev_page"><i class="material-icons">chevron_left</i></a></li>
			<li v-for="page_index in pages" v-bind:class="{ active: page == page_index }" v-bind:class="{ waves-effect: page != page_index }">
				<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index)">@{{ page_index }}</a>
			</li>
			<li v-bind:class="{ disabled: page == pages }" v-bind:class="{ waves-effect: page != pages }"><a href="#!" @click="next_page"><i class="material-icons">chevron_right</i></a></li>
		</ul>
		<div class="col s12" v-show="!loading">
			<div class="col s12 hide-on-med-and-down">
				<div class="card-panel grey lighten-5 z-depth-1 address-card-panel address-title-card">
					<div class="row">
						<div class="col s3">
							<span class="black-text left clickable" @click="sort('addressee_1')">Naslovnik</span>
							<i class="material-icons left" v-show="sort_by == 'addressee_1' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'addressee_1' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s3">
							<span class="black-text left clickable" @click="sort('street_1')">Naslov</span>
							<i class="material-icons left" v-show="sort_by == 'street_1' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'street_1' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s3">
							<span class="black-text left clickable" @click="sort('contact_name')">Kontakt</span>
							<i class="material-icons left" v-show="sort_by == 'contact_name' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'contact_name' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s1 center-align">
							<span class="black-text clickable" @click="sort('active')">Aktiven</span>
							<i class="material-icons left" v-show="sort_by == 'active' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'active' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s2 center-align">
							<span class="black-text">Akcije</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12" v-for="address in addresses">
				<div class="card-panel grey lighten-5 z-depth-1 hoverable address-card-panel">
					<div class="row">
						<div class="col s12 l3">
							<span class="black-text">
								<span class="grey-text hide-on-large-only">Naslovnik:<br></span>
								@{{ address.addressee_1 }} <br v-show="address.addressee_2">
								@{{ address.addressee_2 }}
							</span>
						</div>
						<div class="col s12 l3">
							<span class="black-text">
								<span class="grey-text hide-on-large-only">Naslov:<br></span>
								@{{ address.street_1 }} <br v-show="address.naslovUlica_2 && address.street_2">
								@{{ address.street_2 }} <br v-show="address.naslovPosta || address.city">
								@{{ address.post_number }} @{{ address.city }} <br v-show="address.country_display_name">
								@{{ address.country_display_name }}
							</span>
						</div>
						<div class="col s12 l3">
							<span class="black-text">
								<span class="grey-text hide-on-large-only">Kontakt:<br></span>
								<i class="tiny material-icons" v-show="address.naslovKontaktIme">perm_identity</i> @{{ address.contact_name }}<br v-show="address.contact_phone">
								<a :href="'tel:' + address.contact_phone" v-show="address.contact_phone"><i class="tiny material-icons">call</i> @{{ address.contact_phone }}</a><br v-show="address.contact_email">
								<a :href="'mailto:' + address.contact_email" v-show="address.contact_email"><i class="tiny material-icons">email</i> @{{ address.contact_email }}</a>
							</span>
						</div>
						<div class="col s12 l1 center-align">
							<span class="black-text">
								<span class="grey-text hide-on-large-only">Aktiven:</span>
								<i class="material-icons red-text" v-if="!address.active" style="font-size: 3rem;">close</i>
								<i class="material-icons green-text" v-if="address.active" style="font-size: 3rem;">done</i>
							</span>
						</div>
						<div class="col s12 l2 center-align">
							<span class="black-text">
								<a href="#edit" @click.stop.prevent="edit_address(address)" class="btn red tooltipped" data-tooltip="Uredite naslovnika"><i class="material-icons">edit</i></a>
								<?php /*<a href="#history" @click.stop.prevent="show_history" class="btn red tooltipped" data-tooltip="Ogled zgodovine paketov"><i class="material-icons">assessment</i></a> */ ?>
								<a href="#new_delivery" @click.stop.prevent="send_package(address)" class="btn red tooltipped" data-tooltip="Pošljite paket"><i class="material-icons">note_add</i></a>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12" v-show="addresses.length == 0 && !loading">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s12">
							<span class="black-text">
								Seznam je prazen
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ul class="pagination" v-show="pages > 1 && !loading">
			<li v-bind:class="{ disabled: page == 1 }" v-bind:class="{ waves-effect: page != 1 }"><a href="#!" @click="prev_page"><i class="material-icons">chevron_left</i></a></li>
			<li v-for="page_index in pages" v-bind:class="{ active: page == page_index }" v-bind:class="{ waves-effect: page != page_index }">
				<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index)">@{{ page_index }}</a>
			</li>
			<li v-bind:class="{ disabled: page == pages }" v-bind:class="{ waves-effect: page != pages }"><a href="#!" @click="next_page"><i class="material-icons">chevron_right</i></a></li>
		</ul>
	</div>
	<div id="edit_address_modal" class="modal modal-fixed-footer" data-dismissible="false">
		<div class="modal-content" v-show="!edit.saving">
			<div class="row">
				<div class="col s12 m12 l6">
					<h4>Naslovnik</h4>
					<div class="row">
						<div class="input-field col s12">
							<input v-model="edit.address.addressee_1" id="edit.address.addressee_1" type="text" class="validate">
							<label for="edit.address.addressee_1">Uradni naziv naslovnika</label>
						</div>
						<div class="input-field col s12">
							<input v-model="edit.address.addressee_2" id="edit.address.addressee_2" type="text" class="validate">
							<label for="edit.address.addressee_2">(neobvezno) podrobnosti o naslovniku</label>
						</div>
						<div class="input-field col s12">
							<input v-model="edit.address.street_1" id="edit.address.street_1" type="text" class="validate">
							<label for="edit.address.street_1">Ulica</label>
						</div>
						<div class="input-field col s12">
							<input v-model="edit.address.street_2" id="edit.address.street_2" type="text" class="validate">
							<label for="edit.address.street_2">(neobvezno) Poslovna enota / nadstropje</label>
						</div>
						<div class="input-field col s4 m4 l2">
							<input v-model="edit.address.post_number" id="edit.address.post_number" type="text" class="validate">
							<label for="edit.address.post_number">Poštna št.</label>
						</div>
						<div class="input-field col s8 m8 l4">
							<input v-model="edit.address.city" id="edit.address.city" type="text" class="validate">
							<label for="edit.address.city">Kraj</label>
						</div>
						<div class="input-field col s12 m12 l6">
							<select id="edit_address_country" onchange="AdVue.$emit('edit_address_country_changed', $(this).val(), $(this).find('option:selected').text());">
								@foreach($countries as $country)
								<option value="{{ $country->id }}">{{ $country->display_name }}</option>
								@endforeach
							</select>
							<label>Država</label>
						</div>
					</div>
				</div>
				<div class="col s12 m12 l6">
					<h4>Kontaktni podatki</h4>
					<div class="row">
						<div class="input-field col s12">
							<input v-model="edit.address.contact_name" id="edit.address.contact_name" type="text" class="validate">
							<label for="edit.address.contact_name">Ime</label>
						</div>
						<div class="input-field col s12 m6 l6">
							<input v-model="edit.address.contact_phone" id="edit.address.contact_phone" type="text" class="validate">
							<label for="edit.address.contact_phone">Telefon</label>
						</div>
					</div>
					<h4>Aktiven</h4>
					<div class="row">
						<div class="input-field col s12">
							<input type="checkbox" id="edit.address.active" v-model="edit.address.active"/>
      						<label for="edit.address.active">Ta naslovnik je aktiven</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-content" v-show="edit.saving">
			<h4>Shranjujem, prosimo počakajte ...</h4>
		</div>
		<div class="modal-footer" v-show="!edit.saving">
			<a href="#save" class="modal-action waves-effect waves-green btn green" style="margin-left: 5px;" @click.stop.prevent="save_address">Shrani</a>
			<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="cancel_edit_address">Prekliči</a>
		</div>
		<div class="modal-footer" v-show="edit.saving">
			<div class="progress">
				<div class="indeterminate"></div>
			</div>
		</div>
	</div>
</div>
@stop