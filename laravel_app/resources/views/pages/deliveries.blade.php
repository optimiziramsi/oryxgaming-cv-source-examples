@extends('layouts.full_width')
@section('content')
<div id="deliveries-vue" data-ajax-url="{{ url('/deliveries/json') }}" data-edit-url="{{ url('/deliveries') }}" data-pdf-url-base="{{ route('delivery_pdf_viewer', ['delivery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']) }}" data-pdf-download-url-base="{{ route('delivery_pdf_download', ['delivery_id' => 'REPLACE_ME_WITH_DELIVERY_ID']) }}" data-tracking-url-base="{{ $tracking_base_url }}" style="display:none;">
	<div class="row">
		<div class="col s12 z-depth-2 hoverable" style="padding-top: 1rem;">
			<div class="row">
				<div class="col s12 m6 l6">
					<section class="input-field" data-guide="deliveries.search" data-guide-position="bottom">
						<input id="search_delivery" type="search" v-model="search">
						<label for="search_delivery"><i class="material-icons">search</i> išči paket</label>
						<i class="material-icons" @click="reset_search">close</i>
					</section>	
				</div>
				<div class="col s12 m6 l2">
					<section class="input-field">
						<select id="deliveries_period_select">
							<option value="">Vsa obdobja</option>
							<option value="current_month">Trenutni mesec</option>
							<option value="previous_month">Pretekli mesec</option>
						</select>
					</section>
				</div>
				<div class="col s12 m6 l2">
					<section class="input-field">
						<select id="deliveries_status_select" multiple>
							<option value="" disabled>Vsi statusi</option>
							@foreach(App\Delivery::statuses() as $value => $display_name)
							<option value="{{ $value }}">{{ $display_name }}</option>
							@endforeach
						</select>
					</section>
				</div>
				<div class="col s12 m6 l2">
					<section class="input-field" data-guide="deliveries.filter1" data-guide-position="bottom">
						<select id="deliveries_partner_contact_filter" multiple>
							<option value="" disabled>Vsi uporabniki</option>
							@foreach(App\Partner::get(20, 0, sprintf("partnerId='%s'", App\Partner::current()->company_id)) as $partner)
							<option value="{{ $partner->id }}" {{ $partner->id == App\Partner::current()->id ? 'selected' : '' }}>{{ $partner->display_name }}</option>
							@endforeach
						</select>
					</section>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="progress" v-show="loading">
				<div class="indeterminate"></div>
			</div>
		</div>
		<ul class="pagination" v-show="pages > 1 && !loading">
			<li v-bind:class="{ disabled: page == 1 }" v-bind:class="{ waves-effect: page != 1 }"><a href="#!" @click="prev_page"><i class="material-icons">chevron_left</i></a></li>
			<li v-for="page_index in pages" v-bind:class="{ active: page == page_index }" v-bind:class="{ waves-effect: page != page_index }">
				<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index)">@{{ page_index }}</a>
			</li>
			<li v-bind:class="{ disabled: page == pages }" v-bind:class="{ waves-effect: page != pages }"><a href="#!" @click="next_page"><i class="material-icons">chevron_right</i></a></li>
		</ul>
		<div class="col s12" v-show="!loading">
			<div class="col s12">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel delivery-title-card hide-on-med-and-down">
					<div class="row">
						<div class="col s12 l1">
							<span class="black-text left clickable" @click="sort('code')">Oznaka</span>
							<i class="material-icons left" v-show="sort_by == 'code' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'code' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s12 l2">
							<span class="black-text left clickable" @click="sort('status_code')">Status</span>
							<i class="material-icons left" v-show="sort_by == 'status_code' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'status_code' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s12 l1">
							<span class="black-text left clickable" @click="sort('updated')">
								<i class="material-icons right" v-show="sort_by == 'updated' && sort_type == 'desc'">arrow_drop_down</i>
								<i class="material-icons right" v-show="sort_by == 'updated' && sort_type == 'asc'">arrow_drop_up</i>
								Zadnja Sprememba
							</span>
						</div>
						<div class="col s12 l3">
							<span class="black-text left clickable" @click="sort('addressee_1')">Naslov</span>
							<i class="material-icons left" v-show="sort_by == 'addressee_1' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'addressee_1' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s12 l2">
							<span class="black-text left clickable" @click="sort('contact_name')">Kontakt</span>
							<i class="material-icons left" v-show="sort_by == 'contact_name' && sort_type == 'desc'">arrow_drop_up</i>
							<i class="material-icons left" v-show="sort_by == 'contact_name' && sort_type == 'asc'">arrow_drop_down</i>
						</div>
						<div class="col s12 l1">
							<span class="black-text">Paketi in storitev</span>
						</div>
						<div class="col s12 l2 center-align">
							<span class="black-text">Akcije</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12" v-for="delivery in deliveries">
				<div class="card-panel grey lighten-5 z-depth-1 hoverable delivery-card-panel">
					<div class="row">
						<div class="col s12 l1">
							<span class="grey-text hide-on-large-only">Oznaka:</span>
							<span class="black-text">@{{delivery.code}}</span>
						</div>
						<div class="col s12 l2">
							<span class="grey-text hide-on-large-only">Status:</span>
							<span class="black-text">@{{delivery.status_display_name}}</span>
						</div>
						<div class="col s12 l1">
							<span class="grey-text hide-on-large-only">Zadnja Sprememba:</span>
							<span class="black-text">@{{delivery.updated | humanDate}}</span>
						</div>
						<div class="col s12 l3">
							<span class="grey-text hide-on-large-only">Naslov:<br></span>
							<span class="black-text">
								@{{delivery.addressee_1}}, <br v-show="delivery.naslovNaslovnik_1 && delivery.addressee_2">
								@{{delivery.addressee_2}}<br v-show="delivery.street_1">
								@{{delivery.street_1}}<br v-show="delivery.street_2">
								@{{delivery.street_2}}<br v-show="delivery.post_number || delivery.city || delivery.country_display_name">
								@{{delivery.post_number}} @{{delivery.city}}, @{{delivery.country_display_name}}
							</span>
						</div>
						<div class="col s12 l2">
							<span class="grey-text hide-on-large-only">Kontakt:<br></span>
							<span class="black-text">
								@{{ delivery.contact_name }}<br v-show="delivery.contact_phone">
								<a :href="'tel:' + delivery.contact_phone" v-show="delivery.contact_phone"><i class="tiny material-icons">call</i> @{{ delivery.contact_phone }}</a>
							</span>
						</div>
						<div class="col s12 l1">
							<span class="grey-text hide-on-large-only">Paketi in storitev:<br></span>
							<span class="black-text">
								@{{delivery.packages_count | humanUnit }}<br v-show="delivery.service_display_name">
								@{{delivery.service_display_name }}<br v-show="delivery.service_price">
								@{{delivery.service_price | humanPrice }}
								{{-- <br v-show="delivery.tracking_status_display_name">
								<span class="black-text" v-show="delivery.tracking_status_display_name">[ @{{delivery.tracking_status_display_name}} ]</span> --}}
							</span>
						</div>
						
						<div class="col s12 l2 center-align">
							<span class="black-text">
								<a :href="edit_url + '/' + delivery.id" class="btn red tooltipped" v-show="delivery.status_code === null || delivery.status_code[1] == '0'" data-tooltip="Uredite naročilo"><i class="material-icons">edit</i></a>
								<a href="#" class="btn red tooltipped" @click.stop.prevent="print(delivery.id)" v-show="delivery.status_code !== null && delivery.status_code[1] != '0'" data-tooltip="Natisnite PDF"><i class="material-icons">print</i></a>
								<a href="#" class="btn red tooltipped" @click.stop.prevent="track(delivery.id)" v-show="delivery.status_code !== null && delivery.status_code[1] != '0'" data-tooltip="Sledite pošiljki"><i class="material-icons"><i class="material-icons">track_changes</i></i></a>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col s12" v-show="deliveries.length == 0">
				<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s12">
							<span class="black-text">
								Seznam je prazen
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ul class="pagination" v-show="pages > 1 && !loading">
			<li v-bind:class="{ disabled: page == 1 }" v-bind:class="{ waves-effect: page != 1 }"><a href="#!" @click="prev_page"><i class="material-icons">chevron_left</i></a></li>
			<li v-for="page_index in pages" v-bind:class="{ active: page == page_index }" v-bind:class="{ waves-effect: page != page_index }">
				<a v-bind:href="'#' + page_index" @click.stop.prevent="open_page(page_index)">@{{ page_index }}</a>
			</li>
			<li v-bind:class="{ disabled: page == pages }" v-bind:class="{ waves-effect: page != pages }"><a href="#!" @click="next_page"><i class="material-icons">chevron_right</i></a></li>
		</ul>
	</div>
	<div id="pdf_modal" class="modal modal-fixed-footer" data-dismissible="false">
		<div class="modal-content">
			<div class="row">
				<div class="col s12" v-show="pdf_loading">
					<div class="progress">
						<div class="indeterminate"></div>
					</div>
				</div>
				<div class="col s12">
					<a class="btn red right" :href="pdf_download_url"><i class="material-icons left">file_download</i>Prenesi PDF</a>
				</div>
				{{-- <div id="pdf_viewer"></div> --}}
				<iframe class="col s12" style="min-height: 400px; padding: 0px;" :src="pdf_url" v-on:load="pdf_loading = false;"></iframe>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="cancel_pdf_model">Zapri</a>
		</div>
	</div>
	<div id="track_modal" class="modal modal-fixed-footer" data-dismissible="true">
		<div class="modal-content" v-show="this.tracking_loading">
			<div class="col s12">
				<div class="progress">
					<div class="indeterminate"></div>
				</div>
			</div>
		</div>
		<div class="modal-content" v-show="!this.tracking_loading">
			<div class="row">
				<div class="col s12" v-show="tracking_list.length > 0">
					<div class="card-panel grey lighten-5 z-depth-1">
						<h3>Informacije o paketu:</h3>
						<p>Kurir: <strong>@{{ tracking_courier_display_name ? tracking_courier_display_name : '/' }}</strong></p>
						<p>AWB paketa: <strong>@{{ tracking_awb ? tracking_awb : '/' }}</strong></p>
						<h3>Povezave za slednje paketu. Povezavo lahko posredujete stranki, ki pričakuje paket.</h3>
						<ul>
							<li v-for="tracking_url in tracking_urls">V jeziku @{{ tracking_url.language }}: <strong>@{{ tracking_url.url }}</strong> <a :href="tracking_url.url" target="_blank">prikaži</a></li>
						</ul>
					</div>
				</div>
				<div class="col s12" v-show="tracking_list.length > 0">
					<div class="card-panel grey lighten-5 z-depth-1">
						<div class="row valign-wrapper">
							<div class="col s3">
								<span class="black-text">Datum</span>
							</div>
							<div class="col s3">
								<span class="black-text">Tip</span>
							</div>
							<div class="col s6">
								<span class="black-text">Opis</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12" v-for="tracking in tracking_list">
					<div class="card-panel grey lighten-5 z-depth-1">
						<div class="row valign-wrapper">
							<div class="col s3">
								<span class="black-text">@{{ tracking.updated | humanDate }}</span>
							</div>
							<div class="col s3">
								<span class="black-text">@{{ tracking.location }}</span>
							</div>
							<div class="col s6">
								<span class="black-text">@{{ tracking.description }}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col s12" v-show="0 == tracking_list.length">
					<div class="card-panel grey lighten-5 z-depth-1">
						<div class="row valign-wrapper">
							<div class="col s12">
								<span class="black-text">Za ta paket (še) ni podatkov za sledenje.</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="close_track">Zapri</a>
		</div>
	</div>
</div>
@stop