<!DOCTYPE html>
<html>
	<head>
		@include('includes.head')
	</head>
	@if(isset($no_header_menu) && true == $no_header_menu)
	<body>
		<nav>
			@include('includes.header')
		</nav>
	@else
	<body {{ !Auth::guest() ? 'data-guide-viewed-url='.route('profile').'' : '' }} {{ !Auth::guest() && \App\Partner::current() ? 'data-guide-viewed='.json_encode(\App\Partner::current()->prop('viewed_guides', [])).'' : '' }}>
		<nav>
			@include('includes.header')
		</nav>
	@endif

		<main class="section">
			@yield('main')
		</main>

		<footer class="page-footer red lighten-1">
			@include('includes.footer')
		</footer>

		@include('includes.footer_scripts')
	</body>
</html>