<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('login/forgotpassword', 'ForgotPassword@forgotpassword')->name('forgotpassword');
Route::post('login/forgotpassword', 'ForgotPassword@forgotpassword_submit');
Route::get('login/resetpassword/{token}', 'ForgotPassword@resetpassword')->name('resetpassword');
Route::post('login/resetpassword/{token}', 'ForgotPassword@resetpassword_submit');

/*******************  testings  *******************/
// Route::get('/testings/addresses', 'TestingsController@addresses');
// Route::get('/testings/countries', 'TestingsController@countries');
// Route::get('/testings/deliveries', 'TestingsController@deliveries');
// Route::get('/testings/offers', 'TestingsController@offers');
// Route::get('/testings/deliveries/new', 'TestingsController@delivery_create');
// Route::get('/testings/deliveries/{delivery_id}/services', 'TestingsController@delivery_services');
// Route::get('/testings/deliveries/{delivery_id}/tracking', 'TestingsController@delivery_tracking');
// Route::get('/testings/packages', 'TestingsController@packages');
// Route::get('/testings/packages/new/{delivery_id}', 'TestingsController@packages_new');
// Route::get('/testings/partners', 'TestingsController@partners');
// Route::get('/testings/partners/update', 'TestingsController@partners_update');

/*******************  CSRF REFRESH  *******************/
Route::get('/refresh-csrf', function(){
	return csrf_token();
})->name('refresh-csrf');

/*******************  home page  *******************/
// Route::get('', 'HomeController@index')->name('home');
Route::get('/', 'DeliveriesController@edit')->name('delivery_new');

/*******************  deliveries  *******************/
Route::get('/deliveries', 'DeliveriesController@index')->name('deliveries');
Route::get('/deliveries/json', 'DeliveriesController@json');
Route::get('/deliveries/from_address/{address_id}', 'DeliveriesController@from_address')->name('delivery_from_address');

Route::get('/deliveries/addresses_json', 'DeliveriesController@addresses_json')->name('deliveries_addresses_json');
Route::get('/deliveries/{delivery_id}', 'DeliveriesController@edit')->name('delivery_edit');
Route::get('/deliveries/{delivery_id}/json', 'DeliveriesController@delivery_json')->name('delivery_json');
Route::get('/deliveries/{delivery_id}/pdf', 'DeliveriesController@delivery_pdf')->name('delivery_pdf');
Route::get('/deliveries/{delivery_id}/pdf/download', 'DeliveriesController@delivery_pdf_download')->name('delivery_pdf_download');
Route::get('/deliveries/{delivery_id}/pdf_viewer', 'DeliveriesController@delivery_pdf_viewer')->name('delivery_pdf_viewer');
Route::get('/deliveries/{delivery_id}/tracking', 'DeliveriesController@delivery_tracking')->name('delivery_tracking');
Route::post('/deliveries/{delivery_id}', 'DeliveriesController@update')->name('delivery_post');

/*******************  addresses  *******************/
Route::get('addresses', 'AddressesController@index')->name('addresses');
Route::get('addresses/json', 'AddressesController@json')->name('addresses_json');
Route::post('addresses/{address_id}', 'AddressesController@update');

/*******************  help  *******************/
Route::get('/help', 'HelpController@help_index')->name('help');
Route::get('/help/json', 'HelpController@help_ajax')->name('help_ajax');
Route::post('/help', 'HelpController@help_ajax_update')->name('help_ajax_update');

/*******************  profile  *******************/
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile', 'ProfileController@update');

/*******************  tracking  *******************/
Route::get('/tracking/{tracking_id}/{lang}', 'TrackingController@index')->name('tracking');