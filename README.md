# NAMEN
Koda je namenjena izključno v namen prikaza znanja in se ne sme uporabiti v nobenem drugem primeru, razen v primeru pridobljenega soglasja.

# KODA
Priložena koda je moje delo. Ostala koda je zaradi lažjega pregleda, odstranjena.

# PROJEKT - ionic framework app (angular)
Primer kode, kjer imam dovolj časa za spoštovanje programerskih standardov.

Disclaimers:

* opisi funkcionalnosti so namenoma v Slovenščini
* Večina dokumentacije je v "interface" class-ih, mapa "app/directives/roulette-game/roulette-game/interfaces/..."
* projekt je bil opuščen

# PROJEKT - laravel app (php, vuejs)
Primer "slabe kode", kjer sem spoštoval le minimalne standarde programiranja.

Disclaimers:

* projekt bom 100% vzdrževal samo jaz, zato ni potrebe po čemerkoli več
* Namenoma ne podpiram večjezičnosti, zato je vsebina kar v kodi / ne gre preko prevodov.
* Zunanji API ima naslove klicev in polj v Slovenščini (I know, do't ask, it was above my control). Za primere klicev, glej "nancy_api_docs.md"